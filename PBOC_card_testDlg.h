// PBOC_card_testDlg.h : header file
//

#pragma once

#include "reader/pcsc/PcScCtrl.h" 
#include "reader/pcsc/Helper.h"
#include "reader/q5/serialport.h"
#include "reader/q5/com_helper.h"
#include "reader/q5/Mpos_command.h"
#include "afxwin.h"
#include "pboc\pboc.h"
#include "IssuerDlg.h"
#include "PinDlg.h"
#include "SetTimeDlg.h"


#define PBOC_STATE_PROCESS 1017
#define SECOND_TIMER 1018
#define PBOC_AUTO_OUT_IN 1019

// CPBOC_card_testDlg dialog
class CPBOC_card_testDlg : public CDialog
{
// Construction
public:
    CPBOC_card_testDlg(CWnd* pParent = NULL);    // standard constructor

    ~CPBOC_card_testDlg();
// Dialog Data
    enum { IDD = IDD_PBOC_CARD_TEST_DIALOG };

// Implementation
    CIssuerDlg *Issuer;
    CPinDlg Pin;

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support



public:
    HICON m_hIcon;
    CPcScCtrl m_SCReader;
    CSerialPort serial_port;
    BYTE mode;
protected:
    //transaction setting
    BYTE amount_9f02[6];
    DWORD tsc_9f41;
    BYTE tsc_9f41_bcd[4];
    
    //file setting
    BYTE file_aid_num;
    BYTE file_ca_num;
    BYTE file_aid_bltlv[SECTOR_AID_NUM+EXTENDED_AID_NUM][256];
    BYTE file_ca_bltlv[SECTOR_CA_NUM+EXTENDED_CA_NUM][512];
    BYTE file_term_num;
    BYTE file_term_bltlv[30][30];
    BYTE file_term_special_num;
    BYTE file_term_special_bltlv[30][30];
    BYTE file_dict_num;
    PBOC_DICTIONARY file_dict[50];

    BYTE file_test_num;
    char file_test_bltlvs[255][5][512]; //NAME, ARC, IAD, AC, SCRIPT

    //storage setting    
    BYTE aid_num;
    BYTE ca_num;
    
    #ifdef PBOC_EXTENDED_STORAGE
    BYTE aid_bltlv[SECTOR_AID_NUM+EXTENDED_AID_NUM][256];
    BYTE ca_bltlv[SECTOR_CA_NUM+EXTENDED_CA_NUM][512];
    #else
    BYTE aid_bltlv[SECTOR_AID_NUM][256];
    BYTE ca_bltlv[SECTOR_CA_NUM][512];
    #endif
    
public:
    //Host data
    BOOL network_ok;
    BYTE arclv[512];  //normally is 2
    BYTE authcode[6];
    BYTE issauthdata[16];
    BYTE issauthlen;
    BYTE script[512];
    DWORD script_len;

    //flow control
    int flow_state;
    int second;
    BYTE online_pin[9];
    
    // Generated message map functions
    virtual BOOL OnInitDialog();
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    DECLARE_MESSAGE_MAP()

public:
    static BYTE RunPBOC(BYTE start_nstop);
    static BYTE RunSwipe(BYTE start_nstop);

    DWORD GetEditValue(CEdit &edit, BYTE *buf);
    BOOL ReadFileSetting();

//HAL begin    
    BOOL CheckStorageSetting();
    BOOL ReadStorageSetting();
    void OverWriteStorageSetting();
    BOOL WriteStorageSetting(BYTE aid[16][256], BYTE aidnum);
    BOOL CheckConsistence();
    BOOL CheckReader();
    BOOL CheckCard();
    void AdjustTime();
	void InitRandom();
//HAL end

    void AssmTLV(CEdit &edit, DWORD tag, BYTE taglen, BYTE tlv[1024], DWORD &tlvlen);
    void AssmTLV(BYTE *value, BYTE vallen, DWORD tag, BYTE taglen, BYTE tlv[1024], DWORD &tlvlen);
	void AssmTLV(BYTE *tlvalue, BYTE tlvaluelen, BYTE tlv[1024], DWORD &tlvidx);
//PBOC begin
    void InitPBOC();
    void FlowPBOC();
    void PBOC2nd(BYTE *ARC/*2bytes*/, BYTE *AUTH_CODE/*6bytes*/, BYTE *ISSUER_AUTH_DATA, BYTE IAD_LEN, BYTE *SCRIPT, DWORD SCRIPT_LEN);
    void PBOCEnd();
//PBOC end    

//Swipe begin
    void FlowSwipe();
//Swipe end

    CString GenerateTLVString(DWORD tag, BYTE *lv);
    void UpdateEditValue(int id, BYTE *lv);
    void UpdateEditValue(int id, BYTE *l2v, BOOL isAppend); //two byte hex
    CString SearchDict(uint32_t tag, CString default=0);


    void InitUI();    
    void InitAIDUI();
    void InitCAUI();
    void UpdateTermUI();
    void UpdateCAUI();
    void UpdateAIDUI();

    CMyListCtrl myListAID;
    CMyListCtrl myListCA;
    CEdit m_edit_9f02;
    CEdit m_edit_9c;
    CEdit m_edit_9f09_appver;

    afx_msg void OnBnClickedAtr();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnBnClickedButtonEditAid();
    afx_msg void OnBnClickedRadioS();
    CComboBox m_TermFix;
    afx_msg void OnEnUpdateEdit9f02Amount();
    afx_msg void OnEnChangeEdit9f02Amount();
    afx_msg void OnBnClickedButtonEditWaitin();
    
    void Go2State(int state);
    void StateProcess();
    void ReaderOut();
    void ReaderIn();
	CColorStatic m_pboc_result;
    CString version;

	afx_msg void OnBnClickedButtonLockTime();
	CButton m_check_isforce_online;
	afx_msg void OnBnClickedCheckForceOnline();
	CButton m_check_no_amount;
	afx_msg void OnBnClickedCheckNoAmount();
	afx_msg void OnBnClickedButtonSwipe();
};
