#ifndef __DES3_HH__
#define __DES3_HH__
#include "cryptoPboc.h"
#define USE_3DES

#define DES3_KEY_LEN    24
#define DES3_IV_LEN        8
#define DES_KEY_LEN        8

#define MAXBLOCKSIZE    24
#define	CRYPT_INVALID_KEYSIZE	-21
#define	CRYPT_INVALID_ROUNDS	-22

typedef struct {
    uint32 ek[3][32], dk[3][32];
} psDes3Key_t;

typedef struct {
    int32                blocklen;
    unsigned char        IV[8];
    psDes3Key_t            key;
} des3_CBC;

typedef union {
#ifdef USE_RC2
    rc2_CBC        rc2;
#endif
#ifdef USE_ARC4
    psRc4Key_t    arc4;
#endif
#ifdef USE_3DES
    des3_CBC    des3;
#endif
#ifdef USE_AES
    aes_CBC        aes;
#endif
} psCipherContext_t;


int32 psDes3Init(psCipherContext_t *ctx, unsigned char *IV,
                 unsigned char *key, uint32 keylen);
int32 psDes3Decrypt(psCipherContext_t *ctx, unsigned char *ct,
                    unsigned char *pt, uint32 len);
int32 psDes3Encrypt(psCipherContext_t *ctx, unsigned char *pt,
                    unsigned char *ct, uint32 len);
/*
	Block Mode DES3
*/
int32 psDes3InitKey(const unsigned char *key, uint32 keylen,
                    psDes3Key_t *skey);
void psDes3EncryptBlock(const unsigned char *pt, unsigned char *ct,
                        psDes3Key_t *skey);
void psDes3DecryptBlock(const unsigned char *ct, unsigned char *pt,
                        psDes3Key_t *skey);



#endif

