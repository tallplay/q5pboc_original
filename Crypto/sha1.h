#ifndef ___SHA1_HH__
#define ___SHA1_HH__

#include "cryptoPboc.h"
#define USE_SHA1


#ifdef USE_SHA1
#define SHA1_HASH_SIZE 20
struct sha1_state {
#ifdef HAVE_NATIVE_INT64
    uint64        length;
#else
    uint32        lengthHi;
    uint32        lengthLo;
#endif /* HAVE_NATIVE_INT64 */
    uint32        state[5], curlen;
    unsigned char    buf[64];
};
#endif /* USE_SHA1 */

typedef union {
#ifdef USE_SHA1
    struct sha1_state    sha1;
#endif /* USE_SHA1 */

#ifdef USE_MD5
    struct md5_state    md5;
#endif /* USE_MD5 */
} psDigestContext_t;


#define STORE32H(x, y) { \
(y)[0] = (unsigned char)(((x)>>24)&255); \
(y)[1] = (unsigned char)(((x)>>16)&255); \
(y)[2] = (unsigned char)(((x)>>8)&255); \
(y)[3] = (unsigned char)((x)&255); \
}

#define LOAD32H(x, y) { \
x = ((unsigned long)((y)[0] & 255)<<24) | \
((unsigned long)((y)[1] & 255)<<16) | \
((unsigned long)((y)[2] & 255)<<8)  | \
((unsigned long)((y)[3] & 255)); \
}

#define ROL(x, y) \
	( (((unsigned long)(x)<<(unsigned long)((y)&31)) | \
	(((unsigned long)(x)&0xFFFFFFFFUL)>>(unsigned long)(32-((y)&31)))) & \
	0xFFFFFFFFUL)
#define ROR(x, y) \
	( ((((unsigned long)(x)&0xFFFFFFFFUL)>>(unsigned long)((y)&31)) | \
	((unsigned long)(x)<<(unsigned long)(32-((y)&31)))) & 0xFFFFFFFFUL)



void psSha1Init(psDigestContext_t * md);
void psSha1Update(psDigestContext_t * md, const unsigned char *buf, uint32 len);
int32 psSha1Final(psDigestContext_t * md, unsigned char *hash);
#endif
