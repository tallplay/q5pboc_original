/*
 *	osdep.h
 *	Operating System and Hardware Abstraction Layer
 *	Release $Name: MATRIXSSL-3-4-2-OPEN $
 */
/*
 *	Copyright (c) 2013 INSIDE Secure Corporation
 *	Copyright (c) PeerSec Networks, 2002-2011
 *	All Rights Reserved
 *
 *	The latest version of this code is available at http://www.matrixssl.org
 *
 *	This software is open source; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This General Public License does NOT permit incorporating this software
 *	into proprietary programs.  If you are unable to comply with the GPL, a
 *	commercial license for this software may be purchased from INSIDE at
 *	http://www.insidesecure.com/eng/Company/Locations
 *
 *	This program is distributed in WITHOUT ANY WARRANTY; without even the
 *	implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *	See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *	http://www.gnu.org/copyleft/gpl.html
 */
/******************************************************************************/

#ifndef _h_PS_PLATFORM
#define _h_PS_PLATFORM

#ifdef  __cplusplus
extern "C"{
#endif

#ifndef min
#define min(a,b)    (((a) < (b)) ? (a) : (b))
#endif /* min */

#ifdef WIN32
#include <windows.h>
typedef signed long int32;
typedef unsigned long uint32;
typedef signed short int16;
typedef unsigned short uint16;
#ifdef HAVE_NATIVE_INT64
typedef unsigned long long	uint64;
typedef signed long long	int64;
#endif
#else
#include <stdint.h>
typedef int32_t int32;
typedef uint32_t uint32;
typedef int16_t int16;
typedef uint16_t uint16;
#ifdef HAVE_NATIVE_INT64
typedef int64_t int64;
typedef uint64_t uint64;
#endif
#endif /* WIN32 */

/******************************************************************************/
/*
	Hardware Abstraction Layer
*/
#define halAlert()

#define PSPUBLIC extern

/******************************************************************************/
PSPUBLIC void _psTrace(char *msg);
PSPUBLIC void _psTraceInt(char *msg, int32 val);
PSPUBLIC void _psTraceStr(char *msg, char *val);
PSPUBLIC void _psTracePtr(char *message, void *value);
PSPUBLIC void psTraceBytes(char *tag, unsigned char *p, int l);

PSPUBLIC void _psError(char *msg);
PSPUBLIC void _psErrorInt(char *msg, int32 val);
PSPUBLIC void _psErrorStr(char *msg, char *val);

//tallplay add begin
#define psTraceInfo(x)
#define psTraceStrInfo(x, y)
#define psTraceIntInfo(x, y)
#define psTraceCrypto(x)
#define psTraceStrCrypto(x, y)
#define psTraceIntCrypto(x, y)
#define psTracePtrCrypto(x, y)

//tallplay add end

/******************************************************************************/
/*
	Core trace
*/
#ifndef USE_CORE_TRACE
#define psTraceCore(x)
#define psTraceStrCore(x, y)
#define psTraceIntCore(x, y)
#define psTracePtrCore(x, y)
#else
#define psTraceCore(x) _psTrace(x)
#define psTraceStrCore(x, y) _psTraceStr(x, y)
#define psTraceIntCore(x, y) _psTraceInt(x, y)
#define psTracePtrCore(x, y) _psTracePtr(x, y)
#endif
/* USE_CORE_TRACE */

/******************************************************************************/
/*
	HALT_ON_PS_ERROR define at compile-time determines whether to halt on
	psAssert and psError calls
*/
#define psAssert(C)

#define psError(a)

#define psErrorStr(a,b)

#define psErrorInt(a,b)


#include <string.h> /* memset, memcpy */

#define MATRIX_NO_POOL        (void *)0x0

/******************************************************************************/
/*
    Native memory routines
*/
#include <stdlib.h>         /* malloc, free, etc... */

#define MAX_MEMORY_USAGE    0
#define psOpenMalloc()        0
#define psCloseMalloc()
#define psDefineHeap(A, B)
#define psAddPoolCache(A, B)
#define psMalloc(A, B)        malloc(B)
#include <stdio.h>
/*
inline void * psMalloc(void *pool, uint32_t size)
{
    void *a;
    a = malloc(size);
    printf("alloc %d in %x \n", size, a);
    VCOM_TX_CHECK();
    return a;
}*/

#define psCalloc(A, B, C)    calloc(B, C)
#define psMallocNoPool        malloc
#define psRealloc        //    realloc
#define psFree                free
/*
inline void psFree(void *ptr)
{
    printf("free %x\n", ptr);
    free(ptr);
    VCOM_TX_CHECK();
}*/
#define psMemset            memset
#define psMemcpy            memcpy

typedef int32 psPool_t;



/******************************************************************************/
/*
	Universal return codes
*/
#define PS_SUCCESS			0
#define PS_FAILURE			-1	

/*	NOTE: Failure return codes MUST be < 0 */
/*	NOTE: The range for core error codes should be between -2 and -29 */
#define PS_ARG_FAIL			-6	/* Failure due to bad function param */
#define PS_PLATFORM_FAIL	-7	/* Failure as a result of system call error */
#define PS_MEM_FAIL			-8	/* Failure to allocate requested memory */	
#define PS_LIMIT_FAIL		-9	/* Failure on sanity/limit tests */	
#define PS_UNSUPPORTED_FAIL	-10 /* Unimplemented feature error */	
#define PS_DISABLED_FEATURE_FAIL -11 /* Incorrect #define toggle for feature */
#define PS_PROTOCOL_FAIL	-12 /* A protocol error occurred */
#define PS_TIMEOUT_FAIL		-13 /* A timeout occurred and MAY be an error */
#define PS_INTERRUPT_FAIL	-14 /* An interrupt occurred and MAY be an error */
#define PS_PENDING			-15 /* In process. Not necessarily an error */ 
#define PS_EAGAIN			-16 /* Try again later. Not necessarily an error */ 

#define	PS_TRUE		1
#define	PS_FALSE 	0	



//small ram
#define PS_EXPTMOD_WINSIZE        3

//speed
//#define PS_EXPTMOD_WINSIZE        5


unsigned char CryptoRSAEncrypt(unsigned char *in, unsigned char *exp, unsigned char *mod, uint32 inlen, uint32 elen, uint32 mlen, unsigned char *out);
void CryptoSha1(unsigned char op, const unsigned char *buf, uint32 len, unsigned char *hash);

void Crypto3DESEnc(const unsigned char *key, const unsigned char *pt, unsigned char *ct);
void Crypto3DESDec(const unsigned char *key, const unsigned char *ct, unsigned char *pt);

void pstm_tallplay_test();

#ifdef  __cplusplus
}
#endif

#endif /* _h_PS_PLATFORM */

