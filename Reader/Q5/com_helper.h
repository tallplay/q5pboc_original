#ifndef COM_HELPER_H
#define COM_HELPER_H

#include <windows.h>

#ifdef _cplusplus
extern "C" {
#endif

int FindComPort(char *pName, unsigned short *pVid, unsigned short *pPid);

USHORT h4tous(char *hex);
#ifdef _cplusplus
};
#endif

#endif
