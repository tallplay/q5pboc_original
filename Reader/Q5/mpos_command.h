#ifndef __MPOS_COMMAND_HH
#define __MPOS_COMMAND_HH
#ifdef  __cplusplus
extern "C"{
#endif

#define BYPASS_UINT32_ERROR
#include "stdiint.h"
#define OUT
#define IN

//response code
#define RES_OP_OK 0x3030
#define RES_OP_NOT_SUPPORT 0x3031
#define RES_OP_PARM_ERR 0x3032
#define RES_OP_VAR_LEN_ERR 0x3033
#define RES_OP_FRAME_ERR 0x3034
#define RES_OP_LRC_ERR 0x3035
#define RES_OP_UNKNOWN_ERR 0x3036
#define RES_OP_TIMEOUT 0x3037
#define RES_OP_RETURN_CUR 0x3038

//info code
#define INFO_OP_PBOC_SELECT 1
#define INFO_OP_PBOC_READ 2
#define INFO_OP_PBOC_DA 3
#define INFO_OP_PBOC_HANDLE_LIMIT 4
#define INFO_OP_NO_AID 5
#define INFO_OP_NOT_FOUND 6
#define INFO_OP_EXIST 7
#define INFO_OP_FULL 8
#define INFO_OP_PBOC_TERM_RISK_MANAGER 9
#define INFO_OP_PBOC_TERM_BEHAVIOR_ANA 10
#define INFO_OP_PBOC_CARD_BEHAVIOR_ANA 11

//command code
#define CMD_OP_OPEN_DEVICE 0xD121
#define CMD_OP_READ_TRACKS 0xD122
#define CMD_OP_READ_DEV_INFO 0xF101
#define CMD_OP_GET_RANDOM 0xF102
#define CMD_OP_PBOC_SET_AID 0x1C02
#define CMD_OP_PBOC_SET_CA 0x1C01
#define CMD_OP_PBOC_EXECUTE 0x1C05
#define CMD_OP_PBOC_2ND 0x1C06
#define CMD_OP_PBOC_END 0x1C07
#define CMD_OP_SET_TIME 0x1D04
#define CMD_OP_GET_TIME 0x1D05
#define CMD_OP_ISP 0x1D09

#define CMD_OP_VENDOR_ERASE_FLASH 0xF89F
#define CMD_OP_VENDOR_WRITE_KEY 0xF8AF
#define CMD_OP_VENDOR_READ_UID      0xF85F
#define CMD_OP_VENDOR_WRITE_FLASH   0xF86F
#define CMD_OP_VENDOR_READ_FLASH    0xF87F
#define CMD_OP_VENDOR_PBOC_L1_TEST  0xF88F
#define CMD_OP_VENDOR_READ_TRACKS   0xF84F
#define CMD_OP_VENDOR_SET_BATTERY37 0xF8BF
#define CMD_OP_VENDOR_OPEN_SCR      0xF8CF
#define CMD_OP_VENDOR_CHECK_SCR     0xF8DF
#define CMD_OP_VENDOR_PBOC_TERM     0xF8EF

#define CMD_STATE_STX0 0
#define CMD_STATE_STX1 1
#define CMD_STATE_LEN0 2
#define CMD_STATE_LEN1 3
#define CMD_STATE_INDI 4 //0x2F, 0x3F, 0x4F
#define CMD_STATE_COP0 5
#define CMD_STATE_COP1 6
#define CMD_STATE_SERL 7
#define CMD_STATE_DATA 8
#define CMD_STATE_ETX  9
#define CMD_STATE_LRC  10
#define CMD_STATE_PREPARE_EXECUTE  11
#define CMD_STATE_EXECUTING  12
#define CMD_STATE_RESP_OP0 13
#define CMD_STATE_RESP_OP1 14

#define MPOS_SHARE_SECREAT {20, 14, 03, 22, 20, 14, 03, 22, 20, 14, 03, 22, 20, 14, 03, 22} 
#define MPOS_SIGNATURE "KOL2B168"

void MposRegisterReceiver(uint32_t (*rbytes)(uint32_t len, uint8_t *buf));
void MposRegisterSender(uint8_t (*sbyte)(uint8_t byte), uint32_t (*sbytes)(uint32_t len, uint8_t *buf));
uint16_t MposSendCmd(uint8_t *dat, uint16_t len, uint16_t op, uint8_t serial, OUT uint16_t &response_code, OUT uint8_t *odat, OUT uint16_t olen, IN uint8_t is_timeout=1, void (*info)(uint16_t len, uint8_t *dat) = 0);
uint8_t MPosProcessData(uint8_t dat);
void Mpos_DeriveSessionKey(uint8_t *uid, uint8_t *random_prive, uint8_t *session);
void MposStopPendingCmd();
void Mpos_3DESEnc(uint8_t *key, uint8_t *pt, uint8_t *ct);
void Mpos_3DESDec(uint8_t *key, uint8_t *ct, uint8_t *pt);


#ifdef  __cplusplus
};
#endif

#endif
