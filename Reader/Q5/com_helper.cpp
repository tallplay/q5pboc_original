#include "stdafx.h"
#include "com_helper.h"
#include "autoheapalloc.h"

#include <stdio.h>
#include <stdlib.h>
#include <tchar.h> 

#include <setupapi.h>

#define COM_DEBUG //OutputDebugStringA

int ishex(char c);
USHORT h4tous(char *hex) ;
BOOL IsNumeric(LPCSTR pszString, BOOL bIgnoreColon);
BOOL IsNumeric(LPCWSTR pszString, BOOL bIgnoreColon);
BOOL RegQueryValueString(HKEY kKey, LPCTSTR lpValueName, LPTSTR &pszValue);
BOOL QueryRegistryPortName(HKEY hDeviceKey, int &nPort);

void strupper(char *s) {
    while (*s) {
        if ((*s >= 'a' ) && (*s <= 'z')) *s -= ('a'-'A');
        s++;
    }
}

int
FindComPort(char *pName, unsigned short *pVid, unsigned short *pPid) {

	//First need to convert the name "Ports" to a GUID using SetupDiClassGuidsFromName
	DWORD dwGuids = 0;
	SetupDiClassGuidsFromName(_T("Ports"), NULL, 0, &dwGuids);
	if (dwGuids == 0) {
		return -1;
	}

	//Allocate the needed memory
	CAutoHeapAlloc guids;
	if (!guids.Allocate(dwGuids * sizeof(GUID))){		
		return -1;
	}

	//Call the function again
	GUID* pGuids = static_cast<GUID*>(guids.m_pData);
	if (!SetupDiClassGuidsFromName(_T("Ports"), pGuids, dwGuids, &dwGuids)) {
		return -1;
	}

	//Now create a "device information set" which is required to enumerate all the ports
	HDEVINFO hDevInfoSet = SetupDiGetClassDevs(pGuids, NULL, NULL, DIGCF_PRESENT);
	if (hDevInfoSet == INVALID_HANDLE_VALUE) {
		return -1;
	}

    BOOL                bMoreItems      = TRUE;
    int                 nIndex          = 0;
    SP_DEVINFO_DATA     devInfo;

    int                 iComFound       = -1;

    while (bMoreItems) {
        //Enumerate the current device
        devInfo.cbSize = sizeof(SP_DEVINFO_DATA);
        bMoreItems = SetupDiEnumDeviceInfo(hDevInfoSet, nIndex, &devInfo);

        if (bMoreItems) {
            //Did we find a serial port for this device
            BOOL        bAdded          = FALSE;
            int         nPort           = -1;

            //Get the registry key which stores the ports settings
            HKEY        hDeviceKey      = SetupDiOpenDevRegKey(hDevInfoSet,
                                                               &devInfo,
                                                               DICS_FLAG_GLOBAL,
                                                               0, DIREG_DEV,
                                                               KEY_QUERY_VALUE);
            if (hDeviceKey != INVALID_HANDLE_VALUE) {
                if (QueryRegistryPortName(hDeviceKey, nPort)) {
                    bAdded = TRUE;

                    char        buf[MAX_PATH];
                    sprintf(buf, "Com%d found.\n", nPort);
                    COM_DEBUG(buf);
                }

                //Close the key now that we are finished with it
                RegCloseKey(hDeviceKey);
            }

            //If the port was a serial port, then also try to get its friendly name
            if (bAdded) {
                char    szFriendlyName[1024];
                char    szHardwareName[1024];
                unsigned short vid = 0x0000;
                unsigned short pid = 0x0000;

                DWORD   dwSize;
                DWORD   dwType;              

                // Try to query the FriendlyName
                memset(szFriendlyName, 0x00, sizeof(szFriendlyName));
                dwSize = sizeof(szFriendlyName);
                dwType = 0;
                if (SetupDiGetDeviceRegistryProperty(hDevInfoSet, &devInfo,
                                                     SPDRP_DEVICEDESC,
                                                     &dwType,
                                                     reinterpret_cast<PBYTE>(szFriendlyName),
                                                     dwSize, &dwSize) &&
                    (dwType == REG_SZ)) {
                    // Prolific USB-to-Serial Comm Port
					#ifdef UNICODE
						wcstombs(szFriendlyName, (const wchar_t*)szFriendlyName, wcslen((const wchar_t*)szFriendlyName));
                    #endif
                    COM_DEBUG(szFriendlyName);
                    COM_DEBUG("\r\n");
                } 

                // Try to query the Hardware id.
                memset(szHardwareName, 0x00, sizeof(szHardwareName));
                dwSize = sizeof(szHardwareName);
                dwType = 0;
                if (SetupDiGetDeviceRegistryProperty(hDevInfoSet, &devInfo,
                                                     SPDRP_HARDWAREID,
                                                     &dwType,
                                                     reinterpret_cast<PBYTE>(szHardwareName),
                                                     dwSize, &dwSize) &&
                    (dwType == REG_MULTI_SZ)) {
                    // USB\Vid_067b&Pid_2303&Rev_0300
					#ifdef UNICODE
						wcstombs(szHardwareName, (const wchar_t*)szHardwareName, wcslen((const wchar_t*)szHardwareName));
                    #endif
					strupper(szHardwareName);
                    COM_DEBUG(szHardwareName);
                    COM_DEBUG("\r\n");

                    char        *p;
                    p = strstr(szHardwareName, "VID_");
                    if (p) {
                        vid = h4tous(p + 4);
                    }
                    p = strstr(szHardwareName, "PID_");
                    if (p) {
                        pid = h4tous(p + 4);
                    }
                }

                // If name is assigned. find com port name first.
                if (pName) {
                    if (strlen(pName)) {
                        if (memcmp(pName, szFriendlyName,
                                   strlen(szFriendlyName)) == 0) {
                            if ((*pVid != 0x0000) && (*pPid != 0x0000)) {
                                if ((*pVid == vid) && (*pPid == pid)) {
                                    iComFound = nPort;
                                    goto END_FOUND;
                                }
                            } else {
                                *pVid = vid;
                                *pPid = pid;
                                iComFound = nPort;
                                goto END_FOUND;
                            }
                        }
                    } else {
                        if ((*pVid != 0x0000) && (*pPid != 0x0000)) {
                            if ((*pVid == vid) && (*pPid == pid)) {
								memcpy(pName, szFriendlyName, strlen(szFriendlyName));
                                iComFound = nPort;
                                goto END_FOUND;
                            }
                        } else {
							memcpy(pName, szFriendlyName, strlen(szFriendlyName));
                            *pVid = vid;
                            *pPid = pid;
                            iComFound = nPort;
                            goto END_FOUND;
                        }
                    }

                    // If vid and pid are assigned, try to match vid and pid.
                } else if ((*pVid != 0x0000) && (*pPid != 0x0000)) {
                    if ((*pVid == vid) && (*pPid == pid)) {
						if (pName) {
							memcpy(pName, szFriendlyName, strlen(szFriendlyName));
						}
                        iComFound = nPort;
                        goto END_FOUND;
                    }

                    // None of them are assigned, return the first found com port
                } else {
					if (pName) {
						memcpy(pName, szFriendlyName, strlen(szFriendlyName));
					}
                    *pVid = vid;
                    *pPid = pid;
                    iComFound = nPort;
                    goto END_FOUND;
                }
            }
        }        
        ++nIndex;
    }

    END_FOUND:

    //Free up the "device information set" now that we are finished with it
    SetupDiDestroyDeviceInfoList(hDevInfoSet);

    return iComFound;
}

BOOL
IsNumeric(LPCSTR pszString, BOOL bIgnoreColon) {
    size_t      nLen    = strlen(pszString);
    if (nLen == 0) {
        return FALSE;
    }

    //What will be the return value from this function (assume the best)
    BOOL        bNumeric        = TRUE;

    for (size_t i = 0; i < nLen && bNumeric; i++) {
        bNumeric = (isdigit(static_cast<int>(pszString[i])) != 0);
        if (bIgnoreColon && (pszString[i] == ':')) {
            bNumeric = TRUE;
        }
    }

    return bNumeric;
}

BOOL
IsNumeric(LPCWSTR pszString, BOOL bIgnoreColon) {
    size_t      nLen    = wcslen(pszString);
    if (nLen == 0) {
        return FALSE;
    }

    //What will be the return value from this function (assume the best)
    BOOL        bNumeric        = TRUE;

    for (size_t i = 0; i < nLen && bNumeric; i++) {
        bNumeric = (iswdigit(pszString[i]) != 0);
        if (bIgnoreColon && (pszString[i] == L':')) {
            bNumeric = TRUE;
        }
    }

    return bNumeric;
}

BOOL
RegQueryValueString(HKEY kKey, LPCTSTR lpValueName, LPTSTR &pszValue) {
    //Initialize the output parameter
    pszValue = NULL;

    //First query for the size of the registry value 
    DWORD       dwType          = 0;
    DWORD       dwDataSize      = 0;
    LONG        nError          = RegQueryValueEx(kKey, lpValueName, NULL,
                                                  &dwType, NULL, &dwDataSize);
    if (nError != ERROR_SUCCESS) {
        SetLastError(nError);
        return FALSE;
    }

    //Ensure the value is a string
    if (dwType != REG_SZ) {
        SetLastError(ERROR_INVALID_DATA);
        return FALSE;
    }

    //Allocate enough bytes for the return value
    //+sizeof(TCHAR) is to allow us to NULL terminate the data if it is not null terminated in the registry

    DWORD       dwAllocatedSize = dwDataSize + sizeof(TCHAR); 
    pszValue = reinterpret_cast<LPTSTR>(LocalAlloc(LMEM_FIXED, dwAllocatedSize)); 
    if (pszValue == NULL) {
        return FALSE;
    }

    //Recall RegQueryValueEx to return the data
    pszValue[0] = _T('\0');
    DWORD       dwReturnedSize  = dwAllocatedSize;
    nError = RegQueryValueEx(kKey, lpValueName, NULL, &dwType,
                             reinterpret_cast<LPBYTE>(pszValue),
                             &dwReturnedSize);
    if (nError != ERROR_SUCCESS) {
        LocalFree(pszValue);
        pszValue = NULL;
        SetLastError(nError);
        return FALSE;
    }

    //Handle the case where the data just returned is the same size as the allocated size. This could occur where the data
    //has been updated in the registry with a non null terminator between the two calls to ReqQueryValueEx above. Rather than
    //return a potentially non-null terminated block of data, just fail the method call
    if (dwReturnedSize >= dwAllocatedSize) {
        SetLastError(ERROR_INVALID_DATA);
        return FALSE;
    }

    //NULL terminate the data if it was not returned NULL terminated because it is not stored null terminated in the registry
    if (pszValue[dwReturnedSize / sizeof(TCHAR) - 1] != _T('\0')) {
        pszValue[dwReturnedSize / sizeof(TCHAR)] = _T('\0');
    }

    return TRUE;
}

BOOL
QueryRegistryPortName(HKEY hDeviceKey, int &nPort) {
    //What will be the return value from the method (assume the worst)
    BOOL        bAdded          = FALSE;

    //Read in the name of the port
    LPTSTR      pszPortName     = NULL;
    if (RegQueryValueString(hDeviceKey, _T("PortName"), pszPortName)) {
        //If it looks like "COMX" then
        //add it to the array which will be returned
        size_t  nLen    = _tcslen(pszPortName);
        if (nLen > 3) {
            if ((_tcsnicmp(pszPortName, _T("COM"), 3) == 0) &&
                IsNumeric((pszPortName + 3),
                                                                         FALSE)) {
                //Work out the port number
                nPort = _ttoi(pszPortName + 3);
                bAdded = TRUE;
            }
        }
        LocalFree(pszPortName);
    }
    return bAdded;
}


int
ishex(char c) {
    if (isdigit(c)) {
        return 1;
    }
    if ((c >= 'a') && (c < 'g')) {
        return 1;
    }
    if ((c >= 'A') && (c < 'G')) {
        return 1;
    }
    return 0;
}

USHORT
h4tous(char *hex) {
    int         i       = 0;
    USHORT      j       = 0;
    for (i = 0; i < 4; i++) {
        if (ishex(hex[i])) {
            j *= 16;
            if (isdigit(hex[i])) {
                j += hex[i] - 48;
            } else {
                if ((hex[i] >= 'a') && (hex[i] <= 'f')) {
                    j += hex[i] - 'a' + 10;
                } else {
                    j += hex[i] - 'A' + 10;
                }
            }
        } else {
            return 0;
        }
    }
    return j;
}

