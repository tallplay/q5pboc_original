#include "Mpos_Command.h"
#include <afx.h>
#include <stdlib.h>
#include "..\..\Crypto\CryptoPBoc.h"
static uint8_t dat_state = CMD_STATE_STX0;
static uint8_t dat_type = 0;
static uint8_t dat_lrc = 0;
static uint16_t dat_len = 0;
static uint16_t dat_op = 0;
static uint8_t *dat_dat = 0;
static uint8_t *dat_ptr = 0;
static uint16_t resp_op = 0;

uint8_t (*PROTOCOL_TX_SEND_BYTE)(uint8_t byte) = 0;
uint32_t (*PROTOCOL_TX_SEND)(uint32_t len, uint8_t *buf) = 0;
uint32_t (*PROTOCOL_RX_RECV)(uint32_t len, uint8_t *buf) = 0;

void MposRegisterSender(uint8_t (*sbyte)(uint8_t byte), uint32_t (*sbytes)(uint32_t len, uint8_t *buf))
{
    PROTOCOL_TX_SEND = sbytes;
	PROTOCOL_TX_SEND_BYTE = sbyte;
}

void MposRegisterReceiver(uint32_t (*rbytes)(uint32_t len, uint8_t *buf))
{
    PROTOCOL_RX_RECV = rbytes;
}

uint8_t SendHeader(uint8_t ind, uint16_t len, uint16_t op, uint8_t serial)
{
    uint8_t header[10] = {0x4C, 0x4B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    uint8_t lrc, i;
    
    //bcd length
    len +=4;
    header[3] = (len % 10);
    len = len / 10;
    header[3] |= ((len % 10) << 4);
    len = len / 10;
    header[2] = (len % 10) & 0xf;
    len = len / 10;
    header[2] |= ((len % 10) << 4);

    header[4] = ind;
    header[5] = op >> 8;
    header[6] = op & 0xff;
    header[7] = serial;

    lrc = 0;
    for(i = 2; i < 8; i++)
    {
        lrc ^= header[i];
    }

    PROTOCOL_TX_SEND(8, header);
    
    return lrc;
}

uint8_t SendCmdHeader(uint16_t len, uint16_t op, uint8_t serial)
{    
    return SendHeader(0x2f, len, op, serial);
}


uint8_t SendCmdData(uint8_t *dat, uint16_t len)
{
    uint8_t lrc;
    uint16_t i;

    PROTOCOL_TX_SEND(len, dat);

    lrc = 0;
    for(i = 0; i < len; i++)
        lrc ^= dat[i];

    return lrc;
}

void SendCmdFooter(uint8_t LRC)
{
    
    PROTOCOL_TX_SEND_BYTE(0x03);
    PROTOCOL_TX_SEND_BYTE(LRC);
}

static void SetDatState(uint8_t state)
{
    dat_state = state;
   // printf("state = %d\n", dat_state);
    if(state == CMD_STATE_STX0)
    {
        if(dat_dat)
        {
            free(dat_dat);
            dat_dat = 0;
        }
    }
}

static uint8_t command_stop = 0;

void MposStopPendingCmd()
{
   command_stop = 1;
}

uint16_t MposSendCmd(uint8_t *dat, uint16_t len, uint16_t op, uint8_t serial, OUT uint16_t &response_code, OUT uint8_t *odat, OUT uint16_t olen, IN uint8_t is_timeout, void (*info)(uint16_t len, uint8_t *dat))
{
    uint8_t lrc;
	uint8_t rbuf[512];
	uint32_t rlen;
	uint32_t timeout = 1000000;

	
	if(is_timeout == 1)
    	timeout = 1000000;
	else
	{
		timeout = is_timeout * 100;
	}

    lrc = SendCmdHeader(len, op, serial);

    lrc ^= SendCmdData(dat, len);

    SendCmdFooter(lrc ^ 0x03);

	while(--timeout)
	{
		MSG msg;
	    if(!is_timeout)
    	{
    	    timeout = 300000;
    	}

	    if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))        
	    {        
	            if (msg.message == WM_QUIT)        
	                   break ;        
	            TranslateMessage (&msg) ;        
	            DispatchMessage (&msg) ;        
	    }        
	    else        		
		{
		    if(rlen = PROTOCOL_RX_RECV(512, rbuf))
	    	{
	    	    for(unsigned int i = 0; i < rlen; i++)
		    	{
		    	    if(MPosProcessData(rbuf[i]) == CMD_STATE_PREPARE_EXECUTE)
			    	{
			    	    if(dat_type == 0x4F) //resp
		    	    	{
							ASSERT(op == dat_op);
							ASSERT(dat_len <= olen);
							
							if(op == dat_op)
							{
				    	        response_code = resp_op;
							    memcpy(odat, dat_dat, dat_len);
							    SetDatState(CMD_STATE_STX0);
    							return dat_len;
							}
						    SetDatState(CMD_STATE_STX0);

						}
						else
						{
                            TRACE(TEXT("inform\n"));
							if(info)
							{
							    info(dat_len, dat_dat);
							}
    						SetDatState(CMD_STATE_STX0);
						}
			    	}
		    	}
	    	}
		}

		if(command_stop == 1)
			break;
	}

    response_code = RES_OP_TIMEOUT;
	return 0;
}


uint8_t MPosProcessData(uint8_t dat)
{
    switch(dat_state)
    {
        case CMD_STATE_STX0:
            if(dat == 0x4C)
            {
                SetDatState(CMD_STATE_STX1);
                dat_lrc = 0;
            }
			else
			{
		        TRACE(TEXT("%c"), dat);
			}
            break;
        case CMD_STATE_STX1:
            if(dat == 0x4B)
                SetDatState(CMD_STATE_LEN0);
            else
                SetDatState(CMD_STATE_STX0);
            break;
        case CMD_STATE_LEN0:
            dat_len = (dat & 0xf) * 100;
            SetDatState(CMD_STATE_LEN1);
            dat_lrc = dat;
            break;
        case CMD_STATE_LEN1:
            dat_len += (((dat >> 4) & 0xf) * 10 + (dat & 0xf));
            SetDatState(CMD_STATE_INDI);
            dat_lrc ^= dat;
            if(dat_len >= 6)
                dat_len -= 6;
            else
                SetDatState(CMD_STATE_STX0);
            break;
        case CMD_STATE_INDI:
                {
            if(dat == 0x3F || dat == 0x4F)
            {
	            dat_type = dat;
                if(dat_dat)
                {
                    free(dat_dat);
                }
				if(dat_len)
				{
                    dat_dat = (uint8_t*) malloc(dat_len);
                    dat_ptr = dat_dat;
				}
				else
				{
					dat_ptr = dat_dat = 0;
				}
                SetDatState(CMD_STATE_COP0);
                dat_lrc ^= dat;
            }
            else
                SetDatState(CMD_STATE_STX0);
            }
            break;
        case CMD_STATE_COP0:
            dat_op = (dat << 8);
            SetDatState(CMD_STATE_COP1);
            dat_lrc ^= dat;
            break;
        case CMD_STATE_COP1:
            dat_op += dat;
            SetDatState(CMD_STATE_SERL);
            dat_lrc ^= dat;
            break;
        case CMD_STATE_SERL:
                SetDatState(CMD_STATE_RESP_OP0);
            dat_lrc ^= dat;
            break;
	    case CMD_STATE_RESP_OP0:
            resp_op = (dat << 8);
            SetDatState(CMD_STATE_RESP_OP1);
            dat_lrc ^= dat;
			break;
	    case CMD_STATE_RESP_OP1:
			resp_op += dat;
	        if(dat_len)
                SetDatState(CMD_STATE_DATA);
            else
                SetDatState(CMD_STATE_ETX);

            dat_lrc ^= dat;
			break;
        case CMD_STATE_DATA:
            *dat_ptr++ = dat;
            dat_len--;
            dat_lrc ^= dat;
            if(!dat_len)
                SetDatState(CMD_STATE_ETX);                             
            break;
        case CMD_STATE_ETX:
            dat_lrc ^= dat;
            if(dat == 0x03)
                SetDatState(CMD_STATE_LRC);
            else
                SetDatState(CMD_STATE_STX0);
            break;
        case CMD_STATE_LRC:
            if(dat_lrc == dat)
            {
                SetDatState(CMD_STATE_PREPARE_EXECUTE);
                dat_len = dat_ptr - dat_dat;
            }
            else
            {
                SetDatState(CMD_STATE_STX0);
            }
            break;
    }

	return dat_state;
}


const uint8_t mpos_share_secreat[] = MPOS_SHARE_SECREAT;

//share_secret(UID) ==> TAK, TAK(session_key) = random_prive

void Mpos_DeriveSessionKey(uint8_t *uid, uint8_t *random_prive, uint8_t *session)
{
    uint8_t tak[16];

	Crypto3DESEnc((uint8_t*)mpos_share_secreat, uid, tak);
    Crypto3DESEnc((uint8_t*)mpos_share_secreat, uid+8, tak+8);

	Crypto3DESDec(tak, random_prive, session);
	memcpy(session+8, session, 8);
}

void Mpos_3DESEnc(uint8_t *key, uint8_t *pt, uint8_t *ct)
{
    Crypto3DESEnc(key, pt, ct);
}

void Mpos_3DESDec(uint8_t *key, uint8_t *ct, uint8_t *pt)
{
    Crypto3DESDec(key, ct, pt);
}


