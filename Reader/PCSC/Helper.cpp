// Helper.cpp: implementation of the Helper class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#if 0 //tallplay, need to include your resource file
//#include "agsm2.h"
#else
#define IDS_STRING_FREE 0x100
#define IDS_STRING_READ 0x101
#define IDS_STRING_DONT_KNOW  0x102
#define IDS_STRING_TO_BE_READ  0x103
//#define IDS_STRING_DONT_KNOW  0x104
#define IDS_STRING_SENT  0x105
#define IDS_STRING_TO_BE_SEND  0x107

#endif
#include "Helper.h"
#include "malloc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define TOHEX(a, b)	{*b++ = hextable[a >> 4];*b++ = hextable[a&0xf];}
#define BCDTOASCII(a,b) {*b++ = bcdtable[a&0xf]; *b++ = bcdtable[a >> 4];}

char Helper::hextable[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
char Helper::bcdtable[16] = {'0','1','2','3','4','5','6','7','8','9','*','#','a','b','c','\0'};
static WCHAR gTable_AlphabetToUnicode[] = {
		64,		//0 COMMERCIAL AT @ 64 
		163,	//1 POUND SIGN �� 163 
		36,		//2 DOLLAR SIGN $ 36 
		165,	//3 YEN SIGN �� 165 
		232,	//4 LATIN SMALL LETTER E WITH GRAVE �� 232 
		233,	//5 LATIN SMALL LETTER E WITH ACUTE �� 233 
		249,	//6 LATIN SMALL LETTER U WITH GRAVE �� 249 
		236,	//7 LATIN SMALL LETTER I WITH GRAVE �� 236 
		242,	//8 LATIN SMALL LETTER O WITH GRAVE �� 242 
		199,	//9 LATIN CAPITAL LETTER C WITH CEDILLA ? 199 
		10,		//10 LINE FEED  10 
		216,	//11 LATIN CAPITAL LETTER O WITH STROKE ? 216 
		248,	//12 LATIN SMALL LETTER O WITH STROKE ? 248 
		13,		//13 CARRIAGE RETURN  13 
		197,	//14 LATIN CAPITAL LETTER A WITH RING ABOVE ? 197 
		229,	//15 LATIN SMALL LETTER A WITH RING ABOVE ? 229 
		916,	//16 GREEK CAPITAL LETTER DELTA ��  
		95,		//17 LOW LINE _ 95 
		934,	//18 GREEK CAPITAL LETTER PHI ��  
		915,	//19 GREEK CAPITAL LETTER GAMMA ��  
		923,	//20 GREEK CAPITAL LETTER LAMBDA ��  
		937,	//21 GREEK CAPITAL LETTER OMEGA ��  
		928,	//22 GREEK CAPITAL LETTER PI ��  
		968,	//23 GREEK CAPITAL LETTER PSI ��  
		931,  	//24 GREEK CAPITAL LETTER SIGMA ��
		952, 	//25 GREEK CAPITAL LETTER THETA ��
		926, 	//26 GREEK CAPITAL LETTER XI �� 
		27,		//27 ESCAPE TO EXTENSION TABLE
		198, 	//28 LATIN CAPITAL LETTER AE ? 198
		230,	//29 LATIN SMALL LETTER AE ? 230 
		223,	//30 LATIN SMALL LETTER SHARP S (German) ? 223 
		201,	//31 LATIN CAPITAL LETTER E WITH ACUTE �� 201 
		32,		//32 SPACE  32 
		33,		//33 EXCLAMATION MARK ! 33 
		34,		//34 QUOTATION MARK " 34 
		35,		//35 NUMBER SIGN # 35 
		36,		//36 CURRENCY SIGN �� 164 (ISO-8859-1) 
		37,		//37 PERCENT SIGN % 37 
		38,		//38 AMPERSAND & 38 
		39,		//39 APOSTROPHE ' 39 
		40,		//40 LEFT PARENTHESIS ( 40 
		41,		//41 RIGHT PARENTHESIS ) 41 
		42,		//42 ASTERISK * 42 
		43,		//43 PLUS SIGN + 43 
		44,		//44 COMMA , 44 
		45,		//45 HYPHEN-MINUS - 45 
		46,		//46 FULL STOP . 46 
		47,		//47 SOLIDUS (SLASH) / 47 
		48,		//48 DIGIT ZERO 0 48 
		49,		//49 DIGIT ONE 1 49 
		50,		//50 DIGIT TWO 2 50 
		51,		//51 DIGIT THREE 3 51 
		52,		//52 DIGIT FOUR 4 52 
		53,		//53 DIGIT FIVE 5 53 
		54,		//54 DIGIT SIX 6 54 
		55,		//55 DIGIT SEVEN 7 55 
		56,		//56 DIGIT EIGHT 8 56 
		57,		//57 DIGIT NINE 9 57 
		58,		//58 COLON : 58 
		59,		//59 SEMICOLON ; 59 
		60,		//60 LESS-THAN SIGN < 60 
		61,		//61 EQUALS SIGN = 61 
		62,		//62 GREATER-THAN SIGN > 62 
		63,		//63 QUESTION MARK ? 63 
		64,		//64 INVERTED EXCLAMATION MARK ? 161 
		65,		//65 LATIN CAPITAL LETTER A A 65 
		66,		//66 LATIN CAPITAL LETTER B B 66 
		67,		//67 LATIN CAPITAL LETTER C C 67 
		68,		//68 LATIN CAPITAL LETTER D D 68 
		69,		//69 LATIN CAPITAL LETTER E E 69 
		70,		//70 LATIN CAPITAL LETTER F F 70 
		71,		//71 LATIN CAPITAL LETTER G G 71 
		72,		//72 LATIN CAPITAL LETTER H H 72 
		73,		//73 LATIN CAPITAL LETTER I I 73 
		74,		//74 LATIN CAPITAL LETTER J J 74 
		75,		//75 LATIN CAPITAL LETTER K K 75 
		76,		//76 LATIN CAPITAL LETTER L L 76 
		77,		//77 LATIN CAPITAL LETTER M M 77 
		78,		//78 LATIN CAPITAL LETTER N N 78 
		79,		//79 LATIN CAPITAL LETTER O O 79 
		80,		//80 LATIN CAPITAL LETTER P P 80 
		81,		//81 LATIN CAPITAL LETTER Q Q 81 
		82,		//82 LATIN CAPITAL LETTER R R 82 
		83,		//83 LATIN CAPITAL LETTER S S 83 
		84,		//84 LATIN CAPITAL LETTER T T 84 
		85,		//85 LATIN CAPITAL LETTER U U 85 
		86,		//86 LATIN CAPITAL LETTER V V 86 
		87,		//87 LATIN CAPITAL LETTER W W 87 
		88,		//88 LATIN CAPITAL LETTER X X 88 
		89,		//89 LATIN CAPITAL LETTER Y Y 89 
		90,		//90 LATIN CAPITAL LETTER Z Z 90 
		91,		//91 LATIN CAPITAL LETTER A WITH DIAERESIS ? 196 
		92,		//92 LATIN CAPITAL LETTER O WITH DIAERESIS ? 214 
		93,		//93 LATIN CAPITAL LETTER N WITH TILDE ? 209 
		94,		//94 LATIN CAPITAL LETTER U WITH DIAERESIS �� 220 
		95,		//95 SECTION SIGN �� 167 
		96,		//96 INVERTED QUESTION MARK ? 191 
		97,		//97 LATIN SMALL LETTER A a 97 
		98,		//98 LATIN SMALL LETTER B b 98 
		99,		//99 LATIN SMALL LETTER C c 99 
		100,	//100 LATIN SMALL LETTER D d 100 
		101,	//101 LATIN SMALL LETTER E e 101 
		102,	//102 LATIN SMALL LETTER F f 102 
		103,	//103 LATIN SMALL LETTER G g 103 
		104,	//104 LATIN SMALL LETTER H h 104 
		105,	//105 LATIN SMALL LETTER I i 105 
		106,	//106 LATIN SMALL LETTER J j 106 
		107,	//107 LATIN SMALL LETTER K k 107 
		108,	//108 LATIN SMALL LETTER L l 108 
		109,	//109 LATIN SMALL LETTER M m 109 
		110,	//110 LATIN SMALL LETTER N n 110 
		111,	//111 LATIN SMALL LETTER O o 111 
		112,	//112 LATIN SMALL LETTER P p 112 
		113,	//113 LATIN SMALL LETTER Q q 113 
		114,	//114 LATIN SMALL LETTER R r 114 
		115,	//115 LATIN SMALL LETTER S s 115 
		116,	//116 LATIN SMALL LETTER T t 116 
		117,	//117 LATIN SMALL LETTER U u 117 
		118,	//118 LATIN SMALL LETTER V v 118 
		119,	//119 LATIN SMALL LETTER W w 119 
		120,	//120 LATIN SMALL LETTER X x 120 
		121,	//121 LATIN SMALL LETTER Y y 121 
		122,	//122 LATIN SMALL LETTER Z z 122 
		123,	//123 LATIN SMALL LETTER A WITH DIAERESIS ? 228 
		124,	//124 LATIN SMALL LETTER O WITH DIAERESIS ? 246 
		125,	//125 LATIN SMALL LETTER N WITH TILDE ? 241 
		126,	//126 LATIN SMALL LETTER U WITH DIAERESIS �� 252 
		127,	//127 LATIN SMALL LETTER A WITH GRAVE �� 224 
};
//template< class KEY, class ARG_KEY, class VALUE, class ARG_VALUE >class CMap : public CObject
typedef CMap<WCHAR,WCHAR,UCHAR,UCHAR &> CMapWCharToUChar;

static CMapWCharToUChar gMap_WCharTo7BitAlphabet;


/*
EXTENSION TABLE 
0x1B0A 27 10 FORM FEED  12 
0x1B14 27 20 CIRCUMFLEX ACCENT ^ 94 
0x1B28 27 40 LEFT CURLY BRACKET { 123 
0x1B29 27 41 RIGHT CURLY BRACKET } 125 
0x1B2F 27 47 REVERSE SOLIDUS (BACKSLASH) \ 92 
0x1B3C 27 60 LEFT SQUARE BRACKET [ 91 
0x1B3D 27 61 TILDE ~ 126 
0x1B3E 27 62 RIGHT SQUARE BRACKET ] 93 
0x1B40 27 64 VERTICAL BAR | 124 
0x1B65 27 101 EURO SIGN � 164 (ISO 
*/
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//Helper::Helper()
//{

//}

//Helper::~Helper()
//{

//}

UCHAR Helper::WCharTo7BitAlphabet(WCHAR w)
{
	if(gMap_WCharTo7BitAlphabet.IsEmpty())
	{
		for(int i=0; i<128; i++)
			gMap_WCharTo7BitAlphabet[gTable_AlphabetToUnicode[i]] = i;
	}
	return gMap_WCharTo7BitAlphabet[w];
}

LPSTR Helper::BinToHex(LPBYTE p, int len)
{
	LPSTR str = (LPSTR)malloc(len*3+1);
	LPSTR basestr = str;
	for(int i=0; i<len; i++)
	{
		TOHEX(p[i], str);
		*str++ = ' ';
	}
	*str = '\0';
	return basestr;
}

LPBYTE Helper::HexToBin(LPSTR s, int &len)
{
	LPBYTE pByte = (LPBYTE)malloc(len>>1);
	LPBYTE basepByte = pByte;
	int i=0;
	BYTE b;
	while(*s)
	{
		if(*s <= '9' && *s >= '0')
		{
			b = *s++ - '0';
			*pByte	= (*pByte << 4) | (b & 0xf);
			i++;
		}else if(*s <= 'f' && *s >= 'a'){
			b = *s++ - 'a' + 10 ;
			*pByte	= (*pByte << 4) | (b & 0xf);
			i++;
		}else if(*s <= 'F' && *s >= 'A'){
			b = *s++ - 'A' + 10;
			*pByte	= (*pByte << 4) | (b & 0xf);
			i++;
		}else 
			*s++;
		if(i==2)
		{
			pByte++;
			i = 0;
		}
	}
	if(i)
		pByte++;
	len = pByte - basepByte;

	if(len)
		return basepByte;
	else{
		free(basepByte);
		return NULL;
	}
}

CString Helper::BcdToAscii(LPBYTE p, int len)
{
	CString cstr;
	LPSTR str = (LPSTR)_alloca(len*2+1);
	LPSTR basestr = str;
	for(int i=0; i<len; i++)
	{
		BCDTOASCII(p[i], str);
		if(*(str-1) == '\0')
			break;
	}
	*str = '\0';
	cstr = basestr;
	return cstr;

}

CString Helper::PhoneNumToAscii(LPBYTE p, int len)
{
	CString cstr;
	LPSTR str = (LPSTR)_alloca(len*2+1);
	LPSTR basestr = str;
	if(*p == 0xff)
		return cstr = _T("");
	if(*p++ & 0x50)
		*str++ = '+';
	for(int i=0; i<len; i++)
	{
		BCDTOASCII(p[i], str);
		if(*(str-1) == '\0')
			break;
	}
	*str = '\0';
	cstr = basestr;
	return cstr;

}

inline TCHAR Ascii2Bcd(TCHAR ch)
{
	if(ch > '0' && ch < '9')
		ch -= '0';
	else if(ch == '*')
		ch = 10;
	else if(ch == '#')
		ch = 11;
	return ch;
}

CString Helper::Gsm11SCTStoCStr(LPBYTE p, int len)
{
	//e.g 50 70 90 71 81 12 23 to 05-07-09 17:18:12
	CString cstr;
	LPSTR str = (LPSTR)_alloca(32);
	LPSTR basestr = str;

	BCDTOASCII(*p, str);
	p++;
	*str++ = '-';
	BCDTOASCII(*p, str);
	p++;
	*str++ = '-';
	BCDTOASCII(*p, str);
	p++;
	*str++ = ' ';
	BCDTOASCII(*p, str);
	p++;
	*str++ = ':';
	BCDTOASCII(*p, str);
	p++;
	*str++ = ':';
	BCDTOASCII(*p, str);
	*str = '\0';
	cstr = basestr;
	return cstr;
}

int Helper::CStrToGsm11SCTS(CString Date,LPBYTE pResult, int len)
{
	TCHAR chL,chH;
	int ret;
	int a,b,c,d,e,f;
	LPCTSTR lp = Date;

	ret = _stscanf(lp,_T("%d-%d-%d %d:%d:%d"),&a,&b,&c,&d,&e,&f);
	if(ret != 6)
	{
		CTime time(CTime::GetCurrentTime());
		Date = time.Format(_T("%y-%m-%d %H:%M:%S"));
		TRACE("Current Time %s\n",Date);
	}

	for(int i=0; i<Date.GetLength();)
	{
		if((Date.GetAt(i) == '-') || (Date.GetAt(i) == ':') || (Date.GetAt(i) == ' '))
			i++;
		chL = Ascii2Bcd(Date.GetAt(i));
		i++;
		chH = Ascii2Bcd(Date.GetAt(i));
		i++;
		*pResult++ = ((chH & 0xf) << 4) | (chL&0xf);
	}
	*pResult = 0x23; //time zone
	return len;
}

int Helper::CStrToGsm11Enc7bit(CString str,LPBYTE pRecData,int len)
{
	int strlen = str.GetLength();
	unsigned char *src = (unsigned char *)str.LockBuffer();
	unsigned char *dst = pRecData;

	unsigned char next7bit;
	unsigned char shift_buf;
	char shift_n;
	int i;
	unsigned char tmp;

	shift_buf = *src++;
	i=1;
	do{
		if( (i > strlen) || ((dst-pRecData) >= len) )
			break;
		shift_n = (i&7);
		next7bit = *src++;
		tmp = next7bit<<(8-shift_n);
		shift_buf |= tmp;
		*dst++ = shift_buf;
		i++;
		if(shift_n == 7)
		{
			shift_buf = *src++;
			i++;
		}else
			shift_buf = (next7bit >> shift_n);
	}while(1);
	str.UnlockBuffer();
	return (dst - pRecData);
}

CString Helper::Gsm11Enc7bitToCStr(LPBYTE pData,int len)
{
	char *str = (char *)_alloca(len*8/7 + 1);
	char *base_str = str;
	ULONG shift_buf;
	int  remanent_bits,consume_len;

	shift_buf = *pData++;
	remanent_bits = 8;
	consume_len = 1;

	do{
		*str++ = (char)(shift_buf & 0x7f);
		shift_buf >>= 7;
		remanent_bits -= 7;
		if(remanent_bits < 7)
		{
			if(consume_len >= len)
				break;
			shift_buf |= (*pData++ << remanent_bits); 
			remanent_bits += 8;
			consume_len++;
		}
	}while(TRUE);
	*str = 0;

	int str_len = str-base_str;
	WCHAR *wstr = (WCHAR *)_alloca(sizeof(WCHAR)*str_len+1);
    int i;
	for(i=0; i<str_len; i++)
	{
		wstr[i] = gTable_AlphabetToUnicode[base_str[i]];
	}
	wstr[i] = 0;

	CString cstr =  (LPWSTR)wstr;
	return cstr;
}

CString Helper::Gsm11UCS2ToCStr(LPBYTE pData, int len)
{
	CString cstr;
    int i;
	char *str = (char *)_alloca(len+2);
	for(i=0; i<len; i+=2)
	{
		str[i] = pData[i+1];
		str[i+1] = pData[i];
	}
	str[i] = str[i+1] = 0;
	cstr = (LPWSTR)str;
	return cstr;
}

CString Helper::Gsm11EncStrToCStr(LPBYTE pData,int len)
{
		CString cstr;
		/*
		The MFC conversion macros use _alloca to allocate
		space from the program stack for the converted string.
		The space is automatically deallocated 
		when the procedure call has completed. 
		*/
		//char *str = (char *)malloc(len+2);
		char *str = (char *)_alloca(len+2);
		memset(str,0,len+1);
		if(pData[0] == 0x80) //UNICODE encoding
			memcpy(str,pData+1,len-1);
		else				 //ANSI encoding
			memcpy(str,pData,len);
		//make zero end string
        int j;
		for(j=0; j<len; j++)
		{
			if(str[j] == (char)0xff)
			{
				str[j] = 0;
				str[j+1] = 0;
				break;
			}
		}
		if(pData[0] == 0x80)
		{
			char tmp;
			unsigned k;
			for(k=0; k<(unsigned)j; k+=2)
			{
				tmp = str[k];
				str[k] = str[k+1];
				str[k+1] = tmp;
			}
			cstr = (LPWSTR)str;
		}
		else
			cstr = (LPCSTR)str;
		return cstr;
}

int Helper::CStrToGsm11EncStr(CString name,LPBYTE pRecData,int len)
{
	USES_CONVERSION;
	int i = 0;
	int nlen = name.GetLength();
	int ret = 0;
	if(nlen > 0)
	{
		LPWSTR uniName = T2W(name.GetBuffer());// T2W(name);
		int ulen = lstrlenW(uniName);
		pRecData[0] = 0x80;//unicode flag
		for(i=1; i<(int)(ulen*sizeof(WCHAR)+1) && i<len; i+=2)
		{
			pRecData[i] = *uniName>>8;
			pRecData[i+1] = *uniName&0xff;
			uniName++;
			ret += 2;
		}
	}
	for(; i<len; i++)
		pRecData[i] = 0xff;
	return ret;
}
////////////////////////////////////////////////////////////////////////
//Table 10.5.118/GSM 04.08: Called party BCD number
//+-------------------------------------------------------+
//. Type of number (octet 3) (Note 1) .
//. .
//. Bits .
//. 7 6 5 .
//. 0 0 0 unknown (Note 2) .
//. 0 0 1 international number (Note 3, Note 5) .
//. 0 1 0 national number (Note 3) .
//. 0 1 1 network specific number (Note 4) .
//. 1 0 0 dedicated access, short code .
//. 1 0 1 reserved .
//. 1 1 0 reserved .
//. 1 1 1 reserved for extension
//Table 10.5.118/GSM 04.08: Called party BCD number (continued)
//+---------------------------------------------------------+
//. Numbering plan identification (octet 3) .
//. .
//. Number plan (applies for type of number = 000, .
//. 001, 010 and 100) .
//. Bits .
//. 4 3 2 1 .
//. 0 0 0 0 unknown .
//. 0 0 0 1 ISDN/telephony numbering plan .
//. (Rec. E.164/E.163) .
//. 0 0 1 1 data numbering plan (Recommendation X.121) .
//. 0 1 0 0 telex numbering plan (Recommendation F.69) .
//. 1 0 0 0 national numbering plan .
//. 1 0 0 1 private numbering plan
//. 1 0 1 1 reserved for CTS (see GSM 04.56) .
//. 1 1 1 1 reserved for extension .
//. .
//. All other values are reserved. .
//+---------------------------------------------------------+

/*
GSM 03.40 page 36
Type-of-number:
Bits 6 5 4
0 0 0 Unknown 1)
0 0 1 International number 2)
0 1 0 National number 3)
0 1 1 Network specific number 4)
1 0 0 Subscriber number 5)
1 0 1 Alphanumeric, (coded according to GSM TS 03.38 7-bit default alphabet)
1 1 0 Abbreviated number
1 1 1 Reserved for extension
The maximum length of the full address field (Address-Length, Type-of-Address and Address-Value) is 12 octets.
*/
bool strContaionNotNumber(CString str)
{
	int len = str.GetLength();
	TCHAR c;
	for(int i=0; i<len; i++)
	{
		c = str.GetAt(i);
		if( (c > '9') || (c < '0') )
			return true;
	}
	return false;
}
int Helper::CStrToCalledPartyBcd(CString phone,LPBYTE pRecData,int len)
{
	int i=0;
	int j=0;
	int bcd_len;
	if(phone.GetLength())
	{
		if(phone.Find(_T("+")) == 0)
		{
			bcd_len = phone.GetLength()/2;
			*pRecData++ = bcd_len+1;
			*pRecData++ = 0x91;
			i = 1;
		}else if(strContaionNotNumber(phone))
		{//for Alphanumeric Address type
			//The maximum length of the full address field (Address-Length, Type-of-Address and Address-Value) is 12 octets.
			int len = CStrToGsm11Enc7bit(phone, pRecData+2,10);
			/*The Address-Length field is an integer representation of the number of useful semi-octets within the Address-Value field,
			i.e. excludes any semi octet containing only fill bits.
			04.08 page 35
			*/
			*pRecData++ = len*2; //
			*pRecData++ = 0xd0; //Alphanumeric
			return len+2;
		}else{
			bcd_len = (phone.GetLength()+1)/2;
			*pRecData++ = bcd_len+1;
			*pRecData++ = 0x81;
			i = 0;
		}
		j = 2;
		int max_len;
		if(len == -1)
		{
			len = bcd_len + 2;
			max_len = len;
		}else
			max_len = len - 2;
		for(; i<phone.GetLength() && j<max_len; i+=2)
		{
			TCHAR chL = Ascii2Bcd(phone.GetAt(i));
			TCHAR chH;
			if(i+1 < phone.GetLength())
				chH = Ascii2Bcd(phone.GetAt(i+1));
			else
				chH = 0xf;
			*pRecData++ = ((chH & 0xf) << 4) | (chL&0xf);
			j++;
			if(i+3 == phone.GetLength())
			{
				TCHAR chL = Ascii2Bcd(phone.GetAt(i+2));
				TCHAR chH = 0xf;
				*pRecData++ = ((chH & 0xf) << 4) | (chL&0xf);
				j++;
				i++;
			}
		}
	}
	for(; j<len; j++)
		*pRecData++ =0xff;
	return len;
}
typedef CMap<CString,LPCTSTR,UCHAR,UCHAR &> CMapCStringToUChar;
static CMapCStringToUChar gMap_SmsStatusToUChar;
static	LPCTSTR SmsStatus[] = {
	(LPCTSTR)IDS_STRING_FREE,
	(LPCTSTR)IDS_STRING_READ,
	(LPCTSTR)IDS_STRING_DONT_KNOW,
	(LPCTSTR)IDS_STRING_TO_BE_READ,
	(LPCTSTR)IDS_STRING_DONT_KNOW,
	(LPCTSTR)IDS_STRING_SENT,
	(LPCTSTR)IDS_STRING_DONT_KNOW,
	(LPCTSTR)IDS_STRING_TO_BE_SEND
};

CString Helper::UCharToSmsStatus(UCHAR s)
{
	return SmsStatus[s&0x7];
}

BOOL Helper::SmsStatusToUChar(LPCTSTR Status, UCHAR &rValue)
{
	if(gMap_SmsStatusToUChar.IsEmpty())
	{
		for(int i=0; i<sizeof(SmsStatus)/sizeof(SmsStatus[0]); i++)
		{
			CString str(SmsStatus[i]);
			gMap_SmsStatusToUChar[str] = i;
		}
	}
	return gMap_SmsStatusToUChar.Lookup(Status,rValue);
}

int Helper::ComposeSubmitSms(LPBYTE pResult, int len,CString Status,CString Da,CString Content)
{
	UCHAR rValue = 0;
	SmsStatusToUChar(Status,rValue);
	*pResult++ = rValue;
	/*
	Length of SMSC information. Here the length is 0, 
	which means that the SMSC stored in the phone should be used.
	Note: This octet is optional. On some phones this octet should be omitted! 
	(Using the SMSC stored in phone is thus implicit) 
	*/
	*pResult++ = 0;
	/*
	SMS-SUBMIT PDU
	A first octet of "11" (hex) has the following meaning: 
	Bit no   7       6      5      4      3     2      1      0 
	Name TP-RP TP-UDHI TP-SRR TP-VPF TP-VPF TP-RD TP-MTI TP-MTI 
	Value    0       0      0      1      0     0      0      1 
	*/
	*pResult++ = 0x11; 
	/*
	TP-Message-Reference. 
	The "00" value here lets the phone set the message reference number itself. 
	*/
	*pResult++ = 0;

	int da_len = CStrToCalledPartyBcd(Da,pResult,-1);
	/*
	Address-Length. Length of the sender number 
	*/
	if(Da.IsEmpty())
		*pResult = 0;
	else if(pResult[1] == 0xd0)// for Alphanumeric address type
	{
		//do nothing
	}else
		*pResult = Da.GetLength() - ((Da.GetAt(0) == '+') ? 1 : 0);
	pResult += da_len;
	/*
	Protocol identifier 0 for text sms
	*/
	*pResult++ = 0;
	/*
	Data coding scheme 0x08 for UCS2
	*/
	*pResult++ = 0x08;
	/*
	VP Value Validity period value
	0-143
	 (VP + 1) x 5 minutes (i.e 5 minutes intervals up to 12 hours)
	144-167
	 12 hours + ((VP-143) x 30 minutes)
	168-196
	 (VP-166) x 1 day
	197-255
	 (VP - 192) x 1 week
	*/
	*pResult++ = 0xff; //max value
	int ud_offset = 4+da_len+1+1+1;
	Content.Remove(_T('\r'));
	int ret = CStrToGsm11EncStr(Content,pResult,len-ud_offset);
	*pResult = ret;
	return len;
}

int Helper::ComposeDeliverSms(LPBYTE pResult, int len,CString Status,CString Oa,CString Content,LPCTSTR Date)
{
	UCHAR rValue = 0;
	SmsStatusToUChar(Status,rValue);
	*pResult++ = rValue;
	CString SMSC = _T("+8613800100500");
	int smsc_len = CStrToCalledPartyBcd(SMSC,pResult,-1);
	pResult += smsc_len;
	*pResult++ = 0x04;
	int Oa_len = CStrToCalledPartyBcd(Oa,pResult,-1);
	if(Oa.IsEmpty())
		*pResult = 0;
	else if(pResult[1] == 0xd0)// for Alphanumeric address type
	{
		//do nothing
	}
	else
		*pResult = Oa.GetLength() - ((Oa.GetAt(0) == '+') ? 1 : 0);
	pResult  += Oa_len;
	*pResult++ = 0;
	*pResult++ = 0x08;
	CStrToGsm11SCTS(Date,pResult,-1);
	pResult += 7;
	int ud_offset = 1+smsc_len+1+Oa_len+1+1+7;
	Content.Remove(_T('\r'));
	int ret = CStrToGsm11EncStr(Content,pResult,len-ud_offset);
	*pResult = ret;
	return len;
}

BOOL Helper::ComposeSms(LPBYTE pResult, int len,CString Status,CString Oa,CString Content,CString Date)
{
	CString haveRead(LPCSTR(IDS_STRING_READ));
	CString tobeSend(LPCSTR(IDS_STRING_TO_BE_SEND));
	CString haveFree(LPCSTR(IDS_STRING_FREE));
	if(Status.IsEmpty() && Oa.IsEmpty() && Content.IsEmpty())
	{
		for(int i=0; i<len; i++)
			pResult[i] = 0xff;
		pResult[0] = 0;
		return TRUE;
	}
	if(Status == _T(""))
		Status = haveRead;

	if(Status == tobeSend)
	{
		ComposeSubmitSms(pResult, len,Status,Oa,Content);
		return TRUE;
	}else if(Status == haveFree)
	{
		pResult[0] = 0;
		return TRUE;
	}

	UCHAR rValue = 0;
	if(!SmsStatusToUChar(Status,rValue))
		return FALSE;

	ComposeDeliverSms(pResult,len,Status,Oa,Content,Date);
	return TRUE;
}

int Helper::ShowUserMsg(CString caption, CString msg, UINT uType)
{
	return MessageBox( NULL, msg, caption, uType | MB_ICONINFORMATION );
}

void Helper::ShowErrorMsg(const TCHAR *format ...)
{
    TCHAR msg[200];

     va_list args;
     va_start (args, format);
     _vstprintf (msg, format, args);
     va_end (args);
	 
	MessageBox( NULL, (LPCTSTR)msg, _T("Error"), MB_OK | MB_ICONINFORMATION );
}

void Helper::ShowLastError( DWORD dwMessageId)
{

	LPVOID lpMsgBuf;
	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dwMessageId ? dwMessageId : GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL 
		);
		// Process any inserts in lpMsgBuf.
		// ...
		// Display the string.
		CString str((LPCTSTR)lpMsgBuf);

	MessageBox( NULL, (LPCTSTR)lpMsgBuf, _T("Error"), MB_OK | MB_ICONINFORMATION );
		// Free the buffer.
	LocalFree( lpMsgBuf );
}

void Helper::SetWindowOpacity(CWnd  *pWnd,int percent)
{
		SetWindowLong(pWnd->GetSafeHwnd(),GWL_EXSTYLE,
				GetWindowLong(pWnd->GetSafeHwnd(),GWL_EXSTYLE)|0x80000/*WS_EX_LAYERED*/);
		HINSTANCE hInst = LoadLibrary(_T("User32.DLL")); 
		if(hInst) 
		{ 
			typedef BOOL (WINAPI *MYFUNC)(HWND,COLORREF,BYTE,DWORD); 
			MYFUNC fun = NULL;
			//ȡ��SetLayeredWindowAttributes����ָ�� 
			fun=(MYFUNC)GetProcAddress(hInst, "SetLayeredWindowAttributes");
			if(fun)fun(pWnd->GetSafeHwnd(),0,(255 * percent)/100,2/*LWA_ALPHA*/); 
			FreeLibrary(hInst); 
		} 
}

