// Helper.h: interface for the Helper class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HELPER_H__49B1C473_C0B2_42A6_AF65_B24D5CE11314__INCLUDED_)
#define AFX_HELPER_H__49B1C473_C0B2_42A6_AF65_B24D5CE11314__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning(disable:4786)// identifier was truncated to '255' characters in the browser information (VC6)

namespace Helper
{
    int CStrToGsm11Enc7bit(CString str,LPBYTE pRecData,int len);
    CString Gsm11EncStrToCStr(LPBYTE p, int len);
    CString BcdToAscii(LPBYTE p,int len);
    CString PhoneNumToAscii(LPBYTE p,int len);
    LPBYTE HexToBin(LPSTR p, int &len);
    LPSTR BinToHex(LPBYTE p, int len);
    int CStrToGsm11EncStr(CString name,LPBYTE pRecData,int len);
    int CStrToCalledPartyBcd(CString phone,LPBYTE pRecData,int len);
    CString Gsm11SCTStoCStr(LPBYTE p, int len);
    int CStrToGsm11SCTS(CString Date,LPBYTE p, int len);
    CString	Gsm11Enc7bitToCStr(LPBYTE pData,int len);
    UCHAR WCharTo7BitAlphabet(WCHAR w);
    CString Gsm11UCS2ToCStr(LPBYTE pData, int len);
    CString UCharToSmsStatus(UCHAR s);
    BOOL SmsStatusToUChar(LPCTSTR Status, UCHAR &rValue);
    int ComposeSubmitSms(LPBYTE pResult, int len,CString Status,CString Da,CString Content);
    int ComposeDeliverSms(LPBYTE pResult, int len,CString Status,CString Oa,CString Content,LPCTSTR Date);
    BOOL ComposeSms(LPBYTE pResult, int len,CString Status,CString Oa,CString Content,CString Date);

    void ShowLastError( DWORD dwMessageId = 0);
    void ShowErrorMsg(const TCHAR *format,  ...);
    int ShowUserMsg(CString caption, CString msg, UINT uType = MB_OK);
    void SetWindowOpacity(CWnd  *pWnd,int percent);
    extern char hextable[16];
    extern char bcdtable[16];
#define INT2STR(x) #x
}

#endif // !defined(AFX_HELPER_H__49B1C473_C0B2_42A6_AF65_B24D5CE11314__INCLUDED_)
