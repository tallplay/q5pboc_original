// PcScCtrl.h: interface for the CPcScCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PCSCCTRL_H__FFD62D79_494C_4D1D_B292_B50AF2408C04__INCLUDED_)
#define AFX_PCSCCTRL_H__FFD62D79_494C_4D1D_B292_B50AF2408C04__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <winscard.h>

#define IDS_STRING_ABSENT 0x100
#define IDS_STRING_PRESENT 0x101
#define IDS_STRING_SWALLOWED 0x102
#define IDS_STRING_POWERD 0x103
#define IDS_STRING_NEGOTIABLE 0x104
#define IDS_STRING_UNDEFINED 0x105
#define IDS_STRING_PROTOCOL 0x106
#define IDS_STRING_UNKNOWN 0x107
#define IDS_STRING_T0 0x108
#define IDS_STRING_T1 0x109
#define IDS_STRING_RAW 0x10a

#define IOCTL_USCR_GET_CONFIG_DESCRIPTOR  	SCARD_CTL_CODE( 0x800 )
#define IOCTL_USCR_RESET_DEVICE           	SCARD_CTL_CODE( 0x801 )
#define IOCTL_USCR_RESET_PIPE  				SCARD_CTL_CODE( 0x802 )
#define IOCTL_USCR_CCID						SCARD_CTL_CODE( 0x803 )

#define PC_to_RDR_RunBuiltInFunctions		0x4f
#define PC_to_RDR_IccPowerOn				0x62
#define PC_to_RDR_IccPowerOff 				0x63
#define PC_to_RDR_GetSlotStatus 			0x65
#define PC_to_RDR_XfrBlock 					0x6F
#define PC_to_RDR_GetResponse 				0x70
#define PC_to_RDR_GetParameters 			0x6C
#define PC_to_RDR_ResetParameters 			0x6D
#define PC_to_RDR_SetParameters 			0x61

typedef struct _tagSW1SW2 {
    BYTE MASK[2];
    BYTE SW[2];
    TCHAR *text;
} SW1SW2;

const SW1SW2 C_response[]=
{
    //Response to commands which are correctly executed
    { { 0xff, 0xff }, { 0x90, 0x00 } , _T("Ok") },
    { { 0xff, 0x00 }, { 0x9f, 0x00 } , _T("%d response bytes available") },
    //Memory management
    { { 0xff, 0x00 }, { 0x92, 0x00 } , _T("Update successful but after using an internal retry routin %d times") },
    { { 0xff, 0xff }, { 0x92, 0x40 } , _T("memory problem") },
    //Referencing management
    { { 0xff, 0xff }, { 0x94, 0x00 } , _T("no EF selected") },
    { { 0xff, 0xff }, { 0x94, 0x02 } , _T("out of range(invalid address)") },
    { { 0xff, 0xff }, { 0x94, 0x04 } , _T("file ID or pattern not found") },
    { { 0xff, 0xff }, { 0x94, 0x08 } , _T("file is inconsistent with the command") },
    //Security management
    { { 0xff, 0xff }, { 0x98, 0x02 } , _T("no CHV initialised") },
    { { 0xff, 0xff }, { 0x98, 0x04 } , _T("access condition not fulfilled,authentication failed at least one attempt left") },
    { { 0xff, 0xff }, { 0x98, 0x08 } , _T("in contradiction with CHV status") },
    { { 0xff, 0xff }, { 0x98, 0x10 } , _T("in contradiction with invalidation stats") },
    { { 0xff, 0xff }, { 0x98, 0x40 } , _T("unsuccessful CHV verification,no attempt left. CHV blocked") },
    { { 0xff, 0xff }, { 0x98, 0x50 } , _T("increase cannot be performed,Max value reached") },

    { { 0xff, 0x00 }, { 0x61, 0x00 } , _T("%d response bytes available") },

    { { 0xff, 0xff }, { 0x62, 0x81 } , _T("returned data may be corrupt") },
    { { 0xff, 0xff }, { 0x62, 0x82 } , _T("EOF reached prematurely") },
    { { 0xff, 0xff }, { 0x62, 0x83 } , _T("selected file invalid") },
    { { 0xff, 0xff }, { 0x62, 0x84 } , _T("FCI not formated") },
    { { 0xff, 0x00 }, { 0x62, 0x00 } , _T("nvmem unchanged") },

    { { 0xff, 0x00 }, { 0x63, 0x00 } , _T("nvmem changed") },
    { { 0xff, 0x00 }, { 0x63, 0x81 } , _T("file filled up by last write") },
    { { 0xff, 0xf0 }, { 0x63, 0xc0 } , _T("Counter=%1.1X") },

    { { 0xff, 0xff }, { 0x64, 0x00 } , _T("nvmem unchanged") },
    { { 0xff, 0x00 }, { 0x64, 0x00 } , _T("nvmem unchanged - RFU") },

    { { 0xff, 0xff }, { 0x65, 0x00 } , _T("nvmem changed") },
    { { 0xff, 0xff }, { 0x65, 0x81 } , _T("nvmem changed - memory failure") },
    { { 0xff, 0x00 }, { 0x65, 0x00 } , _T("nvmem changed - unknown?") },

    { { 0xff, 0x00 }, { 0x66, 0x00 } , _T("security related %d") },

    { { 0xff, 0xff }, { 0x67, 0x00 } , _T("wrong length") },
    { { 0xff, 0x00 }, { 0x67, 0x00 } , _T("wrong length - %d expected") },

    { { 0xff, 0xff }, { 0x68, 0x81 } , _T("wrong cla - logical channel not supported") },
    { { 0xff, 0xff }, { 0x68, 0x82 } , _T("wrong cla - secure messaging not supported") },
    { { 0xff, 0x00 }, { 0x68, 0x00 } , _T("cla not supported") },

    { { 0xff, 0xff }, { 0x69, 0x81 } , _T("command incompatible with file structure") },
    { { 0xff, 0xff }, { 0x69, 0x82 } , _T("security status not satisfied") },
    { { 0xff, 0xff }, { 0x69, 0x83 } , _T("authentication method blocked") },
    { { 0xff, 0xff }, { 0x69, 0x84 } , _T("referenced data invalid") },
    { { 0xff, 0xff }, { 0x69, 0x85 } , _T("conditions of use not satisfied") },
    { { 0xff, 0xff }, { 0x69, 0x86 } , _T("command not allowed - no current EF") },
    { { 0xff, 0xff }, { 0x69, 0x87 } , _T("expected SM data objects missing") },
    { { 0xff, 0xff }, { 0x69, 0x88 } , _T("SM data objects incorrect") },
    { { 0xff, 0x00 }, { 0x69, 0x00 } , _T("command not allowed") },

    { { 0xff, 0xff }, { 0x6a, 0x80 } , _T("P1-P2: incorrect parameters in data field") },
    { { 0xff, 0xff }, { 0x6a, 0x81 } , _T("P1-P2: function not supported") },
    { { 0xff, 0xff }, { 0x6a, 0x82 } , _T("P1-P2: file not found") },
    { { 0xff, 0xff }, { 0x6a, 0x83 } , _T("P1-P2: record not found") },
    { { 0xff, 0xff }, { 0x6a, 0x84 } , _T("P1-P2: not enough memory space in file") },
    { { 0xff, 0xff }, { 0x6a, 0x85 } , _T("P1-P2: Lc inconsistent with TLV") },
    { { 0xff, 0xff }, { 0x6a, 0x86 } , _T("P1-P2 incorrect") },
    { { 0xff, 0xff }, { 0x6a, 0x87 } , _T("P1-P2 inconsistent with Lc") },
    { { 0xff, 0xff }, { 0x6a, 0x88 } , _T("Referenced data not found") },
    { { 0xff, 0x00 }, { 0x6a, 0x00 } , _T("P1-P2 invalid") },

    { { 0xff, 0x00 }, { 0x6b, 0x00 } , _T("P1-P2 invalid") },

    { { 0xff, 0x00 }, { 0x6c, 0x00 } , _T("wrong length -  %d expected") },

    { { 0xff, 0x00 }, { 0x6d, 0x00 } , _T("INS code not supported or invalid") },

    { { 0xff, 0x00 }, { 0x6e, 0x00 } , _T("CLA %02X not supported") },

    { { 0xff, 0x00 }, { 0x6f, 0x00 } , _T("no precise diagnosis") },

    { { 0x00, 0x00 }, { 0x00, 0x00 } , _T("Unknown response") }
};

typedef struct _tagBerTlv {
    BYTE ProactiveSIM;
    BYTE SMSPPDownload;
    BYTE CellBroadcatDownload;
    BYTE MenuSelection;
    BYTE CallControl;
    BYTE MOShortMsg;
    BYTE EventDownload;
    BYTE TimeExpiration;
} BER_TLV_TAG;

enum SIMPLE_TLV_TAG {
    CommandDetails =                     								    0x01,
    DeviceIdentity =                    									0x02,
    Result =                            									0x03,
    Duration =                          									0x04,
    AlphaIdentifier =                   									0x05,
    Address =                           									0x06,
    CapabilityConfigurationParameters = 									0x07,
    Subaddress =                        									0x08,
    SSstring =                          									0x09,
    USSDstring =                        									0x0A,
    SMSTPDU =                           									0x0B,
    CellBroadcastPage =                 									0x0C,
    TextString =                        									0x0D,
    Tone =                              									0x0E,
    Item =                              									0x0F,
    ItemIdentifier =                    									0x10,
    ResponseLength =                    									0x11,
    FileList =                          									0x12,
    LocationInformation =               									0x13,
    IMEI =                              									0x14,
    HelpRequest =                       									0x15,
    NetworkMeasurementResults =         									0x16,
    DefaultText =                       									0x17,
    ItemsNextActionIndicator =          									0x18,
    EventList =                         									0x19,
    Cause =                             									0x1A,
    LocationStatus =                    									0x1B,
    TransactionIdentifier =             									0x1C,
    BCCHChannelList =                   									0x1D,
    IconIdentifier =                    									0x1E,
    ItemIconIdentifierList =            									0x1F,
    CardReaderStatus =                 									    0x20,
    CardATR =                           									0x21,
    C_APDU =                            									0x22,
    R_APDU =                            									0x23,
    TimerIdentifier =                   									0x24,
    TimerValue =                        									0x25,
    Date_Time =                         									0x26,
    CallControlRequestedAction =        									0x27,
    ATCommand =                         									0x28,
    ATResponse =                        									0x29,
    BCRepeatIndicator =                 									0x2A,
    ImmediateResponse =                 									0x2B,
    DTMFstring =                        									0x2C,
    Language =                          									0x2D,
    TimingAdvance    =                  									0x2E,
    BrowserIdentity =                  									    0x30,
    URL         =                       									0x31,
    Bearer   =                          									0x32,
    ProvisioningReferenceFile =         									0x33,
    BrowserTerminationCause =           									0x34,
    BearerDescription  =                									0x35,
    ChannelData =                       									0x36,
    ChannelDataLength  =                									0x37,
    ChannelStatus  =                    									0x38,
    BufferSize  =                       									0x39,
    CardReaderIdentifier     =          									0x3A,
    SIMMEInterfaceTransportLevel   =    									0x3C,
    NetworkAccessName =               									    0x47,
};



































enum _proactiveCommand {
    REFRESH                              			=0x01,
    MORE_TIME										=0x02,
    POLL_INTERVAL									=0x03,
    POLLING_OFF										=0x04,
    SET_UP_EVENT_LIST								=0x05,
    SET_UP_CALL										=0x10,
    SEND_SS										    =0x11,
    SEND_USSD										=0x12,
    SEND_SHORT_MESSAGE								=0x13,
    SEND_DTMF										=0x14,
    LAUNCH_BROWSER									=0x15,
    PLAY_TONE										=0x20,
    DISPLAY_TEXT									=0x21,
    GET_INKEY										=0x22,
    GET_INPUT										=0x23,
    SELECT_ITEM										=0x24,
    SET_UP_MENU										=0x25,
    PROVIDE_LOCAL_INFORMATION						=0x26,
    TIMER_MANAGEMENT								=0x27,
    SET_UP_IDLE_MODEL_TEXT							=0x28,
    PERFORM_CARD_APDU								=0x30,
    POWER_ON_CARD									=0x31,
    POWER_OFF_CARD									=0x32,
    GET_READER_STATUS								=0x33,
    RUN_AT_COMMAND									=0x34,
    LANGUAGE_NOTIFICATION							=0x35,
    OPEN_CHANNEL									=0x40,
    CLOSE_CHANNEL									=0x41,
    RECEIVE_DATA									=0x42,
    SEND_DATA										=0x43,
    GET_CHANNEL_STATUS								=0x44,
    TAXEIA_SHOW = 0xff
};

typedef struct _tagIso7816Cmd {
    BYTE Klass;
    BYTE Select;
    BYTE Status;
    BYTE ReadBinary;
    BYTE UpdateBinary;
    BYTE ReadRecord;
    BYTE UpdateRecord;
    BYTE Seek;
    BYTE Increase;
    BYTE VerifyChv;
    BYTE ChangeChv;
    BYTE DisableChv;
    BYTE EnableChv;
    BYTE UnblockChv;
    BYTE Invalidate;
    BYTE Rehabilitate;
    BYTE RunGsmAlgorithm;
    BYTE Sleep;
    BYTE GetResponse;
    BYTE TerminalProfile;
    BYTE Envelope;
    BYTE Fetch;
    BYTE TerminalResponse;
    BYTE GPO;
} ISO7816_CMD;

typedef struct _tagCmdApdu {
    BYTE klass;
    BYTE inc;
    BYTE p1;
    BYTE p2;
    BYTE p3;
    BYTE data[256];
} CMDAPDU;

typedef struct _tagResApdu {
    LPBYTE	data;
    DWORD	len;
} RESAPDU;

enum ReadRecMode {
    RRM_NextRecord=2,
    RRM_PreviousRecord,
    RRM_Absolute
};

enum StructOfFile {
    EF_TRANSPARENT,
    EF_LINERFIXED,
    EF_RFU,
    EF_CYCLIC
};

class CPcScCtrl
{
public:
    LONG DoAPDU(LPBYTE cmdAPDU,DWORD  len,LPBYTE resAPDU,DWORD &reslen);
    LONG DoAPDU_p(LPBYTE cmdAPDU,DWORD  len,LPBYTE resAPDU,DWORD *reslen);
    LONG DoAPDUWithFetch(LPBYTE cmdAPDU,DWORD  len,LPBYTE resAPDU,DWORD &reslen);
    LONG ListReaders();
    CPcScCtrl();
    virtual ~CPcScCtrl();
public:
    void SetCLA(UCHAR cla);
    LONG DisconnectCurCard();
    LONG ConnectCurCard();
    LONG SCardSetAttrib(DWORD dwAttrId,LPCBYTE pbAttr,DWORD cbAttrLen);
    LONG SCardGetAttrib(DWORD dwAttrId, LPBYTE pbAttr,LPDWORD pcbAttrLen);
    LONG SCardStatus(LPTSTR  szReaderName,LPDWORD pcchReaderLen,LPDWORD pdwState,LPDWORD pdwProtocol,LPBYTE pbAtr,LPDWORD pcbAtrLen);
    LONG SCardControl(DWORD dwControlCode, LPCVOID lpInBuffer,  DWORD nInBufferSize,   LPVOID lpOutBuffer,   DWORD nOutBufferSize,  LPDWORD lpBytesReturned  );
    DWORD GetCardState(BYTE *atr, DWORD *len);
    LONG GPO(UCHAR PDOL[], UCHAR pdolen, RESAPDU &resApdu, UCHAR P1=0, UCHAR P2=0);
    LONG Select(ULONG id,RESAPDU &resApdu,UCHAR P1=0, UCHAR P2=0);
    CString FormatErrMsg(BYTE errseq[2]);
    CString FormatErrMsg(LONG errcode);
    LONG GetResponse(RESAPDU &resApdu);
    LONG RunGsmAlgorithm(LPBYTE data,UCHAR len,RESAPDU &resApdu);
    LONG Rehabilitate();
    LONG Invalidate();
    LONG EnableChv(UCHAR ChvNo,LPBYTE data,UCHAR len=8);
    LONG DisableChv(UCHAR ChvNo,LPBYTE data,UCHAR len=8);
    LONG UnblockChv(UCHAR ChvNo,LPBYTE data,UCHAR len=16);
    LONG ChangeChv(UCHAR ChvNo,LPBYTE data,UCHAR len=16);
    LONG VerifyChv(UCHAR ChvNo, LPBYTE data, UCHAR len);
    LONG Increase(LPBYTE data, UCHAR len,RESAPDU &resApdu);
    LONG Seek(UCHAR mode, LPBYTE patten, UCHAR len,RESAPDU &resApdu);
    LONG UpdateRecord(UCHAR RecNo, UCHAR mode, LPBYTE Rec, UCHAR len);
    LONG ReadRecord(UCHAR RecNo, UCHAR mode, RESAPDU &resApdu);
    LONG UpdateBinary(UCHAR OffHi,UCHAR OffLo,LPBYTE data,UCHAR len);
    LONG ReadBinary(UCHAR OffHi, UCHAR OffLo, RESAPDU &resApdu);
    LONG Status(RESAPDU &resApdu);
    LONG Select(UCHAR id[],RESAPDU &resApdu,UCHAR P1=0, UCHAR P2=0);
    LONG Select(UCHAR aid[], UCHAR aidlen, RESAPDU &resApdu, UCHAR P1=0, UCHAR P2=0);
    LONG TerminalProfile(LPBYTE data, UCHAR len, RESAPDU &resApdu);
    LONG TerminalResponse(LPBYTE data, UCHAR len, RESAPDU &resApdu);
    LONG Envelope(LPBYTE data, UCHAR len, RESAPDU &resApdu);

    INT GetCurReader();
    LONG SetCurReader(INT n);
    DWORD TrackingCard(DWORD dwCurrentState = SCARD_STATE_UNAWARE);
    BOOL m_bGetRes;
    CStringArray m_asReaderNames;

private:
    ULONG m_LastError;
    LONG DoChv(UCHAR cmd,UCHAR ChvNo,LPBYTE data,UCHAR len);
    LONG SCListReaders (IN SCARDCONTEXT hContext, IN LPCTSTR lpmszReaderGroups,OUT LPTSTR * lplpmszReaderNames);
    LONG SCFree (IN LPVOID lpMemory);
    LPVOID SCMalloc (IN DWORD dwSize);
    SCARDCONTEXT hContext;
    SCARDHANDLE hCard;
    INT m_nCurReader;
    DWORD dwActiveProtocol;

protected:
    LONG EstablishContext();
};

#endif // !defined(AFX_PCSCCTRL_H__FFD62D79_494C_4D1D_B292_B50AF2408C04__INCLUDED_)
