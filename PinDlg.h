#pragma once


// CPinDlg dialog
#define SECOND_DOWN_COUNT 1008
class CPinDlg : public CDialog
{
    DECLARE_DYNAMIC(CPinDlg)

public:
    CPinDlg(CWnd* pParent = NULL);   // standard constructor
    virtual ~CPinDlg();

// Dialog Data
    enum { IDD = IDD_DIALOG_PIN_KAD };

    int second;
    int UpdateUI();
    unsigned char *your_pin;
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    virtual BOOL OnInitDialog();
    void ResetPin(unsigned char pin[8]);
    afx_msg void OnBnClickedButton1();
    afx_msg void OnBnClickedButton2();
    afx_msg void OnBnClickedButton3();
    afx_msg void OnBnClickedButton4();
    afx_msg void OnBnClickedButton5();
    afx_msg void OnBnClickedButton6();
    afx_msg void OnBnClickedButton7();
    afx_msg void OnBnClickedButton8();
    afx_msg void OnBnClickedButton9();
    afx_msg void OnBnClickedButtonOk();
    afx_msg void OnBnClickedButtonCancel();
	afx_msg void OnBnClickedButton0();
};
