#include "pboc.h"
#include "../crypto/cryptopboc.h"

uint8_t pboc_buf[256];
uint8_t pboc_buflen;
//uint8_t aip = 0;

uint32_t (*pDoAPDU)(uint8_t* cmdAPDU, uint32_t len, uint8_t* resAPDU, uint32_t *reslen) = 0;

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
#include <windows.h>
void TRACE(char *format, ...)
{
    char buffer[96];

    va_list args;

    va_start (args, format);
    vsnprintf(buffer, 96, format, args);
    va_end (args); 

    OutputDebugStringA(buffer);
}
#else
#include <stdio.h>
#define TRACE //printf 
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
//APDU commands
uint32_t Select(uint8_t aid[], uint8_t aidlen, uint8_t P1, uint8_t P2, uint8_t *resAPDU, uint32_t *resLen)
{
    uint8_t apdu[30];

    apdu[0] = 0x00;
    apdu[1] = 0xA4;
    apdu[2] = P1;
    apdu[3] = P2;
    apdu[4] = aidlen;
    memcpy(&apdu[5] , aid, aidlen);

    return pDoAPDU(apdu,5+aidlen, resAPDU, resLen);
}

uint32_t GPO(uint8_t PDOL[], uint8_t pdolen, uint8_t *resAPDU, uint32_t *resLen)
{
    uint8_t *apdu; 
    uint32_t ret;

    if(pdolen > 0x80)
    {
        apdu = (uint8_t*) malloc(pdolen + 7); //2CA.109.00
        apdu[4] = pdolen+2;
        apdu[6] = 0x81;
        memcpy(apdu+7, PDOL, pdolen++);
    }
    else
    {
        apdu = (uint8_t*) malloc(pdolen + 6); //2CA.109.00
        apdu[4] = pdolen+1;
        if(pdolen)
            memcpy(apdu+6, PDOL, pdolen);

    }
    
    apdu[0] = 0x80;
    apdu[1] = 0xa8;
    apdu[2] = 0;
    apdu[3] = 0;
    apdu[5] = 0x83;
    ret = pDoAPDU((uint8_t*)apdu,5+pdolen+1,resAPDU,resLen);
    free(apdu);
    return ret;
}

uint32_t ReadRecord(uint8_t RecNo, uint8_t mode, uint8_t *resAPDU, uint32_t *resLen)
{
    uint8_t apdu[30];

    apdu[0] = 0x00;
    apdu[1] = 0xb2;
    apdu[2] = RecNo;
    apdu[3] = mode;
    apdu[4] = (uint8_t)(*resLen/*-2*/);
    return pDoAPDU((uint8_t*)apdu,5,resAPDU,resLen);
}

uint32_t GetData(uint16_t tag, uint8_t *resAPDU, uint32_t *resLen)
{
    uint8_t apdu[30];

    apdu[0] = 0x80;
    apdu[1] = 0xca;
    apdu[2] = tag >> 8;
    apdu[3] = tag & 0xff;
    apdu[4] = 0x00;
    return pDoAPDU((uint8_t*)apdu,5,resAPDU,resLen);
}

uint32_t TransparantAPDU(uint8_t *apdu, uint32_t len, uint8_t *resAPDU, uint32_t *resLen)
{
    return pDoAPDU((uint8_t*)apdu,len,resAPDU,resLen);
}

uint32_t ExtAuthenticate(uint8_t* idata, uint8_t len, uint8_t *resAPDU, uint32_t *resLen)
{
    uint8_t apdu[30];

    apdu[0] = 0x00;
    apdu[1] = 0x82;
    apdu[2] = 00;
    apdu[3] = 00;
    apdu[4] = len;
    memcpy(apdu+5, idata, len);
    return pDoAPDU((uint8_t*)apdu,5+len,resAPDU,resLen);
}

uint32_t GetResponse( uint8_t *resAPDU, uint32_t *resLen)
{
    uint8_t apdu[5];

    apdu[0] = 0x00;
    apdu[1] = 0xC0;
    apdu[2] = 00;
    apdu[3] = 00;
    apdu[4] = 00;
    return pDoAPDU((uint8_t*)apdu,5,resAPDU,resLen);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//UTIL
/*
static uint32_t BCD2Number(uint32_t bcd)
{
    return ((bcd >> 0) & 0xf) + ((bcd >> 4) & 0xf) * 10 + ((bcd >> 8) & 0xf) * 100 + ((bcd >> 12) & 0xf) * 1000 +
        ((bcd >> 16) & 0xf) * 10000 + ((bcd >> 20) & 0xf) * 100000 + ((bcd >> 24) & 0xf) * 1000000 + ((bcd >> 28) & 0xf) * 10000000;
}*/

uint32_t BCD6byte_2Number(uint8_t *bcd)
{
    if(bcd == (uint8_t*)1) return 0;
    return ((bcd[5]) & 0xf) + ((bcd[5] >> 4) & 0xf) * 10 + ((bcd[4]) & 0xf) * 100 + ((bcd[4] >>4) & 0xf) * 1000 +
        ((bcd[3]) & 0xf) * 10000 + ((bcd[3] >>4) & 0xf) * 100000 + ((bcd[2]) & 0xf) * 1000000 + ((bcd[2] >>4) & 0xf) * 10000000 +
        ((bcd[1]) & 0xf) * 100000000 + ((bcd[1] >>4) & 0xf) * 1000000000;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//type 0: binary 1: string
#define PBOC_TRACE_TYPE_BINARY 0
#define PBOC_TRACE_TYPE_STRING 1
void PBOC_Trace(char *header, uint32_t tag, uint8_t type, uint8_t *dat, uint8_t datlen)
{
    uint8_t temp[17];

    if(type == PBOC_TRACE_TYPE_STRING)
    {
        memcpy(temp, dat, datlen);
        temp[datlen] = 0;
        TRACE("%s(0x%04X): %s\n", header, tag, temp);
    }
    else
    {
        uint8_t i;

        TRACE("%s(0x%04X): 0x", header, tag);
        for(i = 0; i < datlen ; i++)
            TRACE("%02X", dat[i]);
        TRACE("\n");

    }
}

void PBOC_3DESEnc(uint8_t *key, uint8_t *pt, uint8_t *ct)
{
    Crypto3DESEnc(key, pt, ct);
}

void PBOC_3DESDec(uint8_t *key, uint8_t *ct, uint8_t *pt)
{
    Crypto3DESDec(key, ct, pt);
}

void PBOC_InitVar(uint32_t (*papdu)(uint8_t* cmdAPDU, uint32_t len, uint8_t* resAPDU,uint32_t *reslen))
{
    int i;

//    pstm_tallplay_test();
    pDoAPDU = papdu;
}

uint8_t PBOC_SelectDF(uint8_t *name, uint8_t namelen, uint8_t **tlv, uint32_t *tlvlen, uint8_t **dfname, uint8_t P2)
{
    RESAPD resApdu;
    uint8_t *df_name;
    uint16_t result;
    uint8_t result_h;
    uint16_t getres_result;

    resApdu.data = pboc_buf;
    resApdu.len = sizeof(pboc_buf);

    //Select DR
    if(Select(name, namelen, 0x04, P2, resApdu.data, &resApdu.len))
        return 3;

    result = (resApdu.data[resApdu.len - 2] << 8) + resApdu.data[resApdu.len - 1];
    result_h = resApdu.data[resApdu.len - 2]; 
    /*
    if((result == 0x6a82) ||
       (result == 0x6300) ||
       ((result & 0x63C0) == 0x63C0)||
       (result == 0x6983)||
       (result == 0x6984)||
       (result == 0x6985)||
       (result == 0x6a83)||
       (result == 0x6a88)||
       (result == 0x6400)||
       (result == 0x6500)||
       (result == 0x9001)
       )
        return 5; //use select
*/
    if(result == 0x6a81 && P2 == 0) //abort only on non-next,  2CB.036.00 case 06
        return 4;

    if((result_h == 0x62 || result_h == 0x63)) 
    {
        if(resApdu.len == 2)
        {
            if(GetResponse(resApdu.data, &resApdu.len))
                return 5;

            getres_result = (resApdu.data[resApdu.len - 2] << 8) + resApdu.data[resApdu.len - 1];        
        }
        else
            getres_result = 0x9000;//T=1, haha
    }
    else
        getres_result = 0;
    
    if(resApdu.len > 2)
    {
        if(result== 0x9000 || ((result == 0x6283) && getres_result == 0x9000 && dfname)) //apdu success
        {
            if(resApdu.data[0] == FCI_TAG) //judge 0x6F
            {
                uint32_t len;
                uint8_t *ptr;
                uint32_t fci_len;

                if(resApdu.data[1] == 0x81)
                {
                    len = resApdu.data[2];
                    ptr = &resApdu.data[3];
                }
                else
                {
                    len = resApdu.data[1];
                    ptr = &resApdu.data[2];
                }

                fci_len = (&resApdu.data[resApdu.len - 2] - ptr);

                if(len != fci_len)
                    return 3; //2CE.003.08, FCI error, 2CL.032.00

                if(!PBOC_TLVFindTag(ptr, len, FCI_DF_TAG, &df_name)) //get df name
                {
                    //2CE.003.08
                    if(df_name[0] <= 16 && !memcmp(df_name+1, name, namelen)) //compare aid
                    {
                        if(dfname)
                        {
                            *dfname = df_name;
                        }
                        
                        if(!PBOC_TLVFindTag(ptr, len, FCI_ACI_TAG, &df_name))
                        {
                            *tlv = df_name + 1;
                            *tlvlen = df_name[0];

                            fci_len = (&resApdu.data[resApdu.len - 2] - (df_name+ 1));

                            if((fci_len != df_name[0]))
							{
								//but DF_NAME(0x84) is after fci, we need to skip
								if(df_name[1 + df_name[0]] != FCI_DF_TAG)
                                    return 3; //2CE.003.08, FCI propietary error, goto candidate
							}

                            if(result == 0x6283)
                                return 6; //not go into candidate, but may be process 'next'
                            return 0;
                        }
                    }
                }
            }

            return 2;
        }

        return 1;
    }

    return 0xff; //unknow error
}


uint8_t PBOC_GPO(uint8_t *pdol, uint8_t pdollen, PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term)
{
    RESAPD resApdu;
    uint16_t result;
    uint8_t *tlv;
    uint32_t tlvlen;
    uint8_t *gpo_buf;
    uint16_t gpo_idx;

    resApdu.data = pboc_buf;
    resApdu.len = sizeof(pboc_buf);


    tlv = &pdol[1];
    tlvlen = pdol[0];

    gpo_buf = (uint8_t*) malloc(256);
    gpo_idx = 1;
    PBOC_BuildDOLList(pboc_term, pboc_files, &tlv, &tlvlen, gpo_buf, &gpo_idx);
    gpo_buf[0] = gpo_idx - 1;


    if(GPO(gpo_buf, gpo_idx, resApdu.data, &resApdu.len))
        return 4;

    free(gpo_buf);
    result = (resApdu.data[resApdu.len - 2] << 8) + resApdu.data[resApdu.len - 1];

    if(resApdu.len < 2 || (result != 0x9000 && result != 0x6985)) //apdu fail
        return 4;

    if(result == 0x6985)
        return 3;

    if(resApdu.data[0] == 0x80) //judge 0x80
    {
        uint32_t len;
        uint8_t *ptr;
        
        if(resApdu.data[1] == 0x81)
        {
            len = resApdu.data[2];
            ptr = &resApdu.data[2];
        }
        else
        {
            len = resApdu.data[1];
            ptr = &resApdu.data[1];
        }
        
        if(len >= 2)
        {                    
            *ptr = 2; //it's trick, we stolen it for length of lv
            PBOC_SetFileData(pboc_files, CTAG_AIP, ptr);

            if((len - 2) & 0x3) //not multiple of 4, bad format, 2CA.077.01
            {
                return 4;
            }
            
            ptr[2] = len - 2; //it's trick, we stolen it for length of lv
            PBOC_SetFileData(pboc_files, CTAG_AFL, &ptr[2]);

        }
    }
    else if(resApdu.data[0] == 0x77)
    {
        uint8_t *tlv;
        uint32_t tlvlen;
        uint8_t *taglv;
        uint32_t tag;
        uint8_t aip_found = 0;

        if(resApdu.data[1] & 0x80)
        {
            tlvlen = resApdu.data[2];
            tlv = &resApdu.data[3];
        }
        else
        {
            tlvlen = resApdu.data[1];
            tlv = &resApdu.data[2];
        }
        
        while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &taglv)) != 0 || tlvlen/*for padding zero 2CA.078.01*/)
        {
            if(tag == CTAG_AIP)
			{
				if(taglv[0] == 2)
				{
                    aip_found = 1;
				}
				else
					return 4; //2CL.033.00
			}

			if(tag)/*for padding zero 2CA.078.01*/
                PBOC_SetFileData(pboc_files, tag, taglv); //AFL, AIP

        }

        if(!aip_found) //2CL.033.00
        {
            return 4;
        }

    }

    return 0;
}

uint8_t PBOC_ReadRecord(uint8_t rec, uint8_t sfi, uint8_t **tlv, uint32_t *tlvlen)
{
    RESAPD resApdu;
    uint16_t result;
    
    resApdu.data = pboc_buf;
    resApdu.len = sizeof(pboc_buf);

    if(ReadRecord(rec, 0x4 | (sfi << 3),  resApdu.data, &resApdu.len))
        return 1;

    result = (resApdu.data[resApdu.len - 2] << 8) + resApdu.data[resApdu.len - 1];

    if(resApdu.len < 2) //apdu fail
        return 4; //abort

    if(result != 0x9000)
    {
        if(result == 0x6a83) //record not found
            return 1;
        return 4; //abort
    }

    if(resApdu.data[0] == 0x70)
    {
        uint32_t temp_len;
        *tlv = &resApdu.data[0];

        if((*tlv)[1] & 0x80) //tlv length > 1 bytes
        {
            if((*tlv)[1] != 0x81) return 2;

            *tlvlen = (*tlv)[2];
            *tlv = &((*tlv)[3]);
        }
        else
        {
            *tlvlen = (*tlv)[1];
            *tlv = &((*tlv)[2]);
        }     

        temp_len = (&resApdu.data[resApdu.len - 2] - *tlv);

        if(temp_len != *tlvlen)
            return 4; //abort

        return 0;
    }    
    else if(sfi > 10)
    {//2CJ.019.00
        *tlv = &resApdu.data[0];
        *tlvlen = (&resApdu.data[resApdu.len - 2] - *tlv);
        return 0;
    }

    return 3;
}
    
uint8_t PBOC_BuildListByDF(PBOC_CARD_FILES *pboc_files, uint8_t *DF, uint8_t DFLEN, uint8_t (*aid_found)(uint8_t* aid, uint8_t len, uint8_t priority, PBOC_CARD_FILES *pboc_files))
{
    uint8_t *tlv;
    uint32_t tlvlen;
    uint8_t *p_sfi, sfi;

    uint8_t sret;
    uint8_t ret;

    ret = 0;
    sret = PBOC_SelectDF(DF, DFLEN, &tlv, &tlvlen, 0, 0);

    if(!sret)
    {
        if(!PBOC_TLVFindTag(tlv, tlvlen, 0x88, &p_sfi))                       //<-- 從FCI裡面得到SFI
        {
            uint8_t rec;

            sfi = p_sfi[1];

            if(sfi == 0 || sfi > 10)//2CL.037.00
                return 3;

            rec = 1;                                                        //<--設定讀取記錄號為1

            while(!(ret = PBOC_ReadRecord(rec, sfi, &tlv, &tlvlen)))                //<--讀取目錄記錄
            {//success, save the DDF record format
                uint8_t *p_tlv, *p_tlv_head;
                uint32_t tag;
                uint8_t *tagdata;

                p_tlv = (uint8_t*) malloc(tlvlen);
                memcpy(p_tlv, tlv, tlvlen);
                p_tlv_head = p_tlv; //save

                while((tag = PBOC_TLVSearch61(&p_tlv, &tlvlen, &tagdata)) != 0)
                {             
                    if(tag == CTAG_APP_TEMPLATE) //<--紀錄找到?
                    {
                            
                        uint8_t template_len;
                        uint8_t *df_name, *api_pri;
                        uint8_t tmp_pri[2] = {0x00, 0x00};
                        api_pri = tmp_pri;
                        
                        template_len = tagdata[0];
                        tagdata += 1;

                        if(PBOC_TLVFindTag(tagdata, template_len, APP_TAG, &df_name))
                        {//2CL.005.00
                            free(p_tlv_head);
                            return 3;
                        }


                        if(!PBOC_TLVFindTag(tagdata, template_len, CTAG_ADF_NAME, &df_name))
                        {
                            uint8_t api_pri_not_found;
                            
                            PBOC_Trace("ADF", CTAG_ADF_NAME, PBOC_TRACE_TYPE_BINARY,  df_name+1, df_name[0]);
                            api_pri_not_found = PBOC_TLVFindTag(tagdata, template_len, API_TAG, &api_pri);
                            
                            if((!api_pri_not_found && !(api_pri[1] & 0x80)) || api_pri_not_found)
                            {
                                PBOC_Trace("ADF pick", CTAG_ADF_NAME, PBOC_TRACE_TYPE_BINARY,  df_name+1, df_name[0]);   
                                if(aid_found) //if accepted
                                {
                                    aid_found(df_name+1, df_name[0], (api_pri[1] & 0xf), pboc_files);
                                }
                            }
                            else
                            {
                                PBOC_Trace("ADF unpick", CTAG_ADF_NAME, PBOC_TRACE_TYPE_BINARY,  df_name+1, df_name[0]);    
                            }
                            
                            
                        }
                        else if(!PBOC_TLVFindTag(tagdata, template_len, CTAG_DDF_NAME, &df_name))
                        {
                            PBOC_Trace("DDF", tag, PBOC_TRACE_TYPE_BINARY,  df_name+1, df_name[0]);

                            PBOC_BuildListByDF(pboc_files, df_name+1, df_name[0], aid_found); //recursive
                        }
                        else //2CL.029.05
                        {
                             free(p_tlv_head);
                             return 3;
                        }
                                                

                    }

                }
build_end:                          
             free(p_tlv_head);
             rec++;
            }

            //no record
            if(ret == 4) //except 0x6A83 and 0x9000
                return 3;
        }
    }
    
    return sret;
}

uint8_t PBOC_isTagNum(uint32_t tag)
{
    return (tag == TTAG_DATE || tag == TTAG_AUTH_QUAN || tag == TTAG_AUTH_OTHER_QUAN || tag == TTAG_CONTRY || tag == TTAG_MONEY_CONTRY);
}

uint8_t PBOC_isTagCNum(uint32_t tag)
{
    return (tag == CTAG_TRACK2) || (tag == CTAG_PAN) || (tag == CTAG_HOLD_VTYPE);
}
static const uint16_t termTags[] =
{
/*non-used tags begin*/
TTAG_AMOUNT_REF,
TTAG_ACQUIRER_ID,
TTAG_EXTRA_CABILITY,
TTAG_TEST_SEQ_COUNTER,
TTAG_AUTH_OTHER_QUAN_BINARY,
TTAG_MERCHANT_CATEGORY_CODE,
TTAG_MERCHANT_ID,
TTAG_POS_ENTRY,
TTAG_TERM_ID,
/*non-used tags end*/

/*device specified tag begin*/
TTAG_IAD,
/*device specified tag end*/

/*set AID tags begin*/
TTAG_RID,
////2CA.013.00 TTAG_ASI,
TTAG_APP_VER,
////2CA.013.00 TTAG_TAC_DEFAULT,
////2CA.013.00 TTAG_TAC_ONLINE,
////2CA.013.00 TTAG_TAC_DENIAL,
TTAG_FLOOR_LMT,
////2CA.013.00 TTAG_BIAS_RANDOM_SEL_THRESHOLD,
////2CA.013.00 TTAG_BIAS_RANDOM_SEL_MAX_PERCENTAGE,
////2CA.013.00 TTAG_RANDOM_SEL_PERCENTAGE,
////2CA.013.00 TTAG_DEFAULT_DDOL,
////2CA.013.00 TTAG_CONNECT_PIN_SUPPORT,
/*set AID tags end*/

/*set CA tags begin*/
TTAG_CA_IDX,
////2CA.013.00 TTAG_CA_EXP,
////2CA.013.00 TTAG_CA_MOD,
/*set CA tags end*/

/*fix tags begin*/
TTAG_MONEY_CONTRY,
TTAG_TRAN_REF_CUR,
TTAG_TRAN_REF_CUR_EXP,
TTAG_CONTRY,
TTAG_TERMINAL_CAP,
TTAG_MERCHANT_NAME,
TTAG_TERMINAL_TYPE,
/*fix tags end*/

/*changable tags begin*/
TTAG_UNPREDICT,
TTAG_TVR,
TTAG_TSI,
TTAG_DATE,
TTAG_TIME,
/*changable tags end*/

/*trasaction tags begin*/
TTAG_AUTH_QUAN,
TTAG_AUTH_QUAN_BINARY,
TTAG_AUTH_OTHER_QUAN,
TTAG_TRANACTION_TYPE,
////2CA.013.00 TTAG_FORCE_CONNECT,
////2CA.013.00 TTAG_VENDOR_TRANSACTION_TYPE,
////2CA.013.00 TTAG_VENDOR_FLOW,
////2CA.013.00 TTAG_VENDOR_SEL_ACCOUNT,

TTAG_ARC,
TTAG_AUTH_CODE,
TTAG_ISSUER_AUTH_DATA,
TTAG_ISSUER_SCRIPT1,
TTAG_ISSUER_SCIRPT2,
/*transaction tags end*/

/*genrated tags begin*/
////2CA.013.00 TTAG_HINT_PIN,
////2CA.013.00 TTAG_SCRIPT_RESULT,
TTAG_CVM_RESULT
/*genrated tags end*/
};

uint8_t PBOC_isTermTag(uint32_t tag)
{
    uint8_t i;
    for(i = 0; i < sizeof(termTags)/sizeof(uint16_t); i++)
    {
        if(tag == termTags[i])
            return 1;
    }

    return 0;
}

void PBOC_CopyLV(uint32_t tag, uint8_t *lv, uint8_t *target, uint8_t len)
{
    if(lv)
    {
        if(lv[0] != len)
        {
            if(lv[0] > len)
            {
                //the leftmost bytes of the data element shall be truncated if the data object has numeric (n1) format, or the rightmost 
                //bytes of the data shall be truncated for any other format
                if(PBOC_isTagNum(tag)/*tag is n*/)
                    memcpy(target, lv+1 + lv[0]- len/*truncated leftmost*/, len);
                else
                    memcpy(target, lv+1, len); /*truncated rightmost*/                
            }
            else
            {
                if(PBOC_isTagNum(tag)/*tag is n*/)
                {
                    memset(target, 0, len - lv[0]); //leading zeros
                    memcpy(target + len - lv[0], lv+1, lv[0]);
                }
                else if(PBOC_isTagCNum(tag)/*tag is cn*/) //traling 0xFF
                {
                    memcpy(target, lv+1, lv[0]);
                    memset(target+lv[0], 0xFF, len - lv[0]); //trailing 0xzero
                }
                else/*tag is other*/
                {
                    memcpy(target, lv+1, lv[0]);
                    memset(target+lv[0], 0x00, len - lv[0]); //trailing 0xzero
                }
            }
        }
        else
            memcpy(target, lv+1, len);
    }
    else
        memset(target, 0, len);
}

void PBOC_BuildDOLList(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files, uint8_t **dol, uint32_t *dollen, uint8_t *list, uint16_t *listlen)
{
    uint32_t tag, taglen;
    uint8_t *term_ptr;
    uint8_t *tvr5 = 0;

        
    while((tag = PBOC_TLEnum(dol, dollen, &taglen)) != 0)
    {
        /////tallplay fix 06/12////////////////
        if(tag == CTAG_TDOL_TC)
        {
            uint8_t *tdol;
            uint8_t *tl1;
            uint32_t tllen1;
            uint32_t tag1;
            uint32_t taglen1;
            uint8_t *hash_input;
            uint8_t hash_index;
            uint8_t hash[20];
            
           
            tdol = PBOC_GetFileData(pboc_files, CTAG_TDOL);
            if(tdol)
            {
                tl1 = &tdol[1];
                tllen1 = tdol[0];
                
                hash_index = 0;
                hash_input = (uint8_t*) malloc(256);

                while((tag1 = PBOC_TLEnum(&tl1, &tllen1, &taglen1)) != 0)
                {
                //workaround begin////
                    if(tag == TTAG_CA_IDX)
                    {
                        PBOC_GetCAPKI(pboc_files->aid+1, 0, TTAG_CA_IDX, &term_ptr);
                    }
                    else
                //workaround end////

                    if(PBOC_isTermTag(tag1))
                    {
                        term_ptr = PBOC_GetTermData(pboc_term, tag1);

                        if(!term_ptr)
                            term_ptr = PBOC_GetAID(pboc_files->aid, tag1);
                    }
                    else
                    {
                        term_ptr = PBOC_GetFileData(pboc_files, tag1);
                    }

                    
                    PBOC_CopyLV(tag1, term_ptr, hash_input+hash_index, taglen1);

                    hash_index += taglen1;

                }

                CryptoSha1(0, 0, 0, 0); //init;
                CryptoSha1(1, hash_input, hash_index, 0);
                CryptoSha1(2, 0, 0, hash);


                free(hash_input);
                memcpy(list+*listlen, hash, 20);

            }
            else
            {
                
//                PBOC_SetTVR(TVR_SET_DEFAULT_TDOL, 0);
  //              if(tvr5)
    //                *tvr5 |= TVR4_USE_DEFAUL_TDOL;
                //memset(list+*listlen, 0, 20);
                CryptoSha1(0, 0, 0, 0); //init;
                CryptoSha1(2, 0, 0, hash);
                memcpy(list+*listlen, hash, 20);

            }

            *listlen = *listlen + 20;

        }
        else
        {
          int i;
            //workaround begin////
                if(tag == TTAG_CA_IDX)
                {
                    PBOC_GetCAPKI(pboc_files->aid+1, 0, TTAG_CA_IDX, &term_ptr);
                }
                else
            //workaround end////
            if(PBOC_isTermTag(tag))
            {
                term_ptr = PBOC_GetTermData(pboc_term, tag);

                if(!term_ptr)
                    term_ptr = PBOC_GetAID(pboc_files->aid, tag);
            }
            else
            {
                term_ptr = PBOC_GetFileData(pboc_files, tag);
            }

            PBOC_CopyLV(tag, term_ptr, list+*listlen, taglen);

            if(tag == TTAG_TVR) //2CJ. 002.14
                tvr5 = &list[(*listlen)+4];

            *listlen = *listlen + taglen;
        }

    }

}

uint8_t PBOC_SelectApp(uint8_t *aid, uint8_t aidlen, uint8_t *pdol,  PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files)
{
    uint8_t *tlv;
    uint32_t tlvlen;
    uint16_t gpo_len;
    uint8_t ret;
    gpo_len = 0;

    PBOC_DeInitCardData(pboc_files);

    ret = PBOC_SelectDF((uint8_t*)aid, aidlen, &tlv, &tlvlen, 0, 0x00);
    if(ret == 4) //6a81
    {
        return 4;
    }
    else if(!ret)
    {
        uint32_t tag;
        uint8_t *tagdata;

        pboc_files->tag_num = 0;

        gpo_len = 1;

        pdol[0] = 0;
        while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &tagdata)) != 0)
        {
            if(tlvlen > 0x80000000) //negtive 2CE.003.08 case 6
            return 3;

            if(tag == PDOL_TAG)
            {
                uint8_t *tlv;
                uint32_t tlvlen;
                uint32_t taglen;

                if(pdol[0]) return 1;//2CE.003.08 case 5
                PBOC_Trace("PDOL_TAG",tag, PBOC_TRACE_TYPE_BINARY,  tagdata + 1, tagdata[0]);

                memcpy(pdol+1, &tagdata[1], tagdata[0]);
                pdol[0] = tagdata[0];

            }
            else
                PBOC_SetFileData(pboc_files, tag, tagdata);

        }

        memcpy(pboc_files->aid+1, aid, aidlen);
        pboc_files->aid[0] = aidlen;
        return 0;
    }

    return 1;
}

#define MIN(x, y) (x > y ? y : x)
uint8_t aid_found(uint8_t *found_aid, uint8_t found_aid_len, uint8_t priority, PBOC_CARD_FILES *pboc_files)
{
    uint8_t *aid, *aidx;
    
    aidx = 0;
    while(1)
    {
        if(!(aid = PBOC_GetAID(aidx, 0)))
            return 2;
        
        if(!memcmp(found_aid, aid+1, MIN(aid[0], found_aid_len))) //partial
        {        
            uint8_t *asi;
            asi = PBOC_GetAID(aid, TTAG_ASI);

            //asi found
            if(asi)
            {
                if(asi[1] == 0x1) //full compare
                {
                    if(found_aid_len != aid[0])
                    {
                        aidx++;
                        continue;
                    }
                }
            }

            PBOC_SetAidCandidate(pboc_files, found_aid, found_aid_len, priority);

            return 0;
        }

        aidx++;
    }

    return 1;
}

uint8_t PBOC_PSESearchAID(PBOC_CARD_FILES *pboc_files)
{
    uint8_t ret;
    ret = PBOC_BuildListByDF(pboc_files, (uint8_t*)"1PAY.SYS.DDF01", 14, aid_found);
    if(ret == 4/*cancel trasaction*/ || ret == 3 /*switch to candidate*/)
    {
        PBOC_ClearAidCandidate(pboc_files);
    }

    return ret;
}

uint8_t PBOC_ListSearchAID(PBOC_CARD_FILES *pboc_files)
{
    uint8_t *aid, *aidx;
    uint8_t ret;
    uint8_t *dfname;
    uint8_t P2;
    
    aidx = 0;
    while(1)
    {
        uint8_t *tlv;
        uint32_t tlvlen;

        if(!(aid = PBOC_GetAID(aidx, 0)))
            break;

        P2 = 0;
next:
        ret = PBOC_SelectDF((uint8_t*)aid+1, aid[0], &tlv, &tlvlen, &dfname, P2);

        if(ret == 4) //6a81, Abort, PBOC lvl2
        {
            PBOC_ClearAidCandidate(pboc_files);
            return 4;
        }

        if(!ret || ret == 6/*6283*/)
        {
            uint8_t api_pri_not_found;
            uint8_t *api_pri;

            
            api_pri_not_found = PBOC_TLVFindTag(tlv, tlvlen, API_TAG, &api_pri);
            
            if(!ret && ((!api_pri_not_found && !(api_pri[1] & 0x80) && (api_pri[0] == 1)) || api_pri_not_found))
            {
                //2CE.003.08, case 5, case 6
                uint8_t *tagdata;
                uint32_t tag;
                uint8_t pdol_found;
                uint8_t pick;
                pdol_found = 0;
                pick = 1;
                while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &tagdata)) != 0)
                {
                    if(tlvlen > 0x80000000) //negtive 2CE.003.08 case 6
                    {
                        pick = 0; //2CE0030806
                        break;
                    }
                    
                    if(tag == PDOL_TAG)
                    {

                        if(pdol_found)
                        {
                            pick = 0; //2CE0030805
                            break;
                        }
                        pdol_found = 1;
                    }
                }

                if(pick)
                {
                    PBOC_Trace("AID pick", CTAG_ADF_NAME, PBOC_TRACE_TYPE_BINARY,  dfname+1, dfname[0]);   
                    if(aid_found) //if accepted
                    {
                        aid_found(dfname+1, dfname[0], (api_pri[1] & 0xf), pboc_files);
                    }
                }
            }
            else
            {
                PBOC_Trace("AID unpick", CTAG_ADF_NAME, PBOC_TRACE_TYPE_BINARY,  dfname+1, dfname[0]);    
            }

            if(dfname[0] > aid[0])
            {
                P2 = 2;
                goto next;
            }
        }

        aidx++;
    }    

    return 0;
}


uint8_t PBOC_ProcessAIDSearched(PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term, uint8_t *pdol)
{
    uint8_t *found_aid, found_aid_len, pri;
    uint8_t *ptr;
    uint32_t respidx;
    uint8_t app_ver[5];

    pri = 1;
    
    found_aid_len = PBOC_GetAidCandidate(pboc_files, pri, &found_aid);
    if(found_aid_len)
    {    
        uint8_t ret;

        while((ret = PBOC_SelectApp((uint8_t*)found_aid, found_aid_len, pdol, pboc_term, pboc_files)))
        {
           /* if(ret == 4) //6a81, Abort, PBOC lvl2
            {
                //clear aid candidate
                PBOC_ClearAidCandidate(pboc_files);
                return 4;
            }*/

            pri = found_aid[found_aid_len];
           found_aid[found_aid_len] = 0xff; //destroy the unsuccessfull AID 2CL.032.00
            found_aid_len = PBOC_GetAidCandidate(pboc_files, pri, &found_aid);

            if(!found_aid_len)
                return 1;
        }

        //save aid on file 
        pboc_files->aid[0] = found_aid_len;
        memcpy(&pboc_files->aid[1], found_aid, found_aid_len);

        /*get app_ver predefined*/
        PBOC_AutoTermAppVer(&pboc_files->aid[0], app_ver);


        //save aid on terminal, Fix 2CA.115.00 begin
        ptr = (uint8_t*) malloc(found_aid_len + 3 + 10);
        respidx = 0;
        PBOC_AssmTLV(TTAG_RID, 2, found_aid_len, found_aid, ptr, &respidx);
        PBOC_AssmTLV(TTAG_APP_VER, 2, app_ver[0], app_ver+1, ptr, &respidx);

        PBOC_SetTermTransactionData(pboc_term, ptr, respidx);
        free(ptr);
        //save aid, Fix 2CA.115.00 end

        //clear aid candidate
        //PBOC_ClearAidCandidate(pboc_files);
        found_aid[found_aid_len] = 0xff; //destroy the priority2CA.030.06

        return 0;
    }

    PBOC_ClearAidCandidate(pboc_files);

    return 1;
}


//應用選擇
uint8_t PBOCF_SelectApp(uint8_t **pdol, PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term)
{
    uint8_t *ptr;
    uint32_t respidx;
    uint32_t amount_binary, ret;
    uint8_t amount_binary_array[4];

    *pdol = (uint8_t*) malloc(256); //<== free inside, be cared

    //使用PSE
    ret = PBOC_PSESearchAID(pboc_files);
    if(ret == 4) return 4;
        
    //選擇交易應用
    ret = PBOC_ProcessAIDSearched(pboc_files, pboc_term, *pdol);
    //if(ret == 4) return 4;        

    //使用AID 列表
    if(ret) 
    {
        ret = PBOC_ListSearchAID(pboc_files);
        if(ret == 4) return 4;

        //選擇交易應用
        ret = PBOC_ProcessAIDSearched(pboc_files, pboc_term, *pdol);
        //if(ret == 4) return 4;

    }

    if(ret) return 1;

    //add amount binary
    ptr = (uint8_t*) malloc(4+2);
    respidx = 0;    
    amount_binary = BCD6byte_2Number(PBOC_GetTermData(pboc_term, TTAG_AUTH_QUAN) + 1);
    amount_binary_array[0] = amount_binary >> 24;
    amount_binary_array[1] = amount_binary >> 16;
    amount_binary_array[2] = amount_binary >> 8;
    amount_binary_array[3] = amount_binary >> 0;
    PBOC_AssmTLV(TTAG_AUTH_QUAN_BINARY, 1, 4, amount_binary_array, ptr, &respidx);
    PBOC_SetTermTransactionData(pboc_term, ptr, respidx);
    free(ptr);

    return 0;
            
}

//應用初始化
uint8_t PBOCF_InitApp(uint8_t *pdol, PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term)
{
    uint8_t *afl, aflen, *aflv;

    uint8_t ret;

gpo_start:
    ret = PBOC_GPO(pdol, pdol[0], pboc_files, pboc_term);
    if(!ret)
    {
        if(aflv = PBOC_GetFileData(pboc_files, CTAG_AFL))
        {
            uint8_t i;

            afl = aflv + 1;
            aflen = aflv[0];
            
            //pboc_files->tag_num = 0; move to SelectApp
            pboc_files->file_num = aflen/4;
            pboc_files->files = (PBOC_CARD_FILE*) malloc(sizeof(PBOC_CARD_FILE) * pboc_files->file_num);
            pboc_files->sda_list = (uint8_t*)malloc(248);
            pboc_files->sda_listlen = 0;


            //save file number
            for(i = 0; i < aflen; i+=4)
            {
                pboc_files->files[i/4].sfi = afl[i] >> 3;
                pboc_files->files[i/4].record_firat =  afl[i+1];
                pboc_files->files[i/4].record_last = afl[i+2];
                pboc_files->files[i/4].sfi_sdanum = afl[i+3];

                if(afl[i+2] - afl[i+1] + 1 < afl[i+3]|| afl[i+1] > afl[i+2]) //2CL.050.00
                    goto init_app_fail;

                TRACE("SFI : 0x%x, record 0x%x~0x%x needSDA 0x%x\n",  afl[i] >> 3, afl[i+1], afl[i+2], afl[i+3]);
            }

            free(pdol);
            PBOC_ClearAidCandidate(pboc_files);
            return 0;
        }
    }
    else if(ret == 3)
    {
        //選擇交易應用
        if(!PBOC_ProcessAIDSearched(pboc_files, pboc_term, pdol))
            goto gpo_start;

    }

init_app_fail:

    PBOC_ClearAidCandidate(pboc_files);
    free(pdol);

    return 1;
}


//讀應用數據
uint8_t PBOCF_ReadAppData(PBOC_CARD_FILES *pboc_files, void (*flow_ind)(uint8_t))
{
    uint8_t i, j, k;


    for(i = 0; i < pboc_files->file_num ; i++)
    {
        RESAPD resApdu;
        uint8_t SDA_Count;

        resApdu.data = pboc_buf;

        SDA_Count = pboc_files->files[i].sfi_sdanum;
        for(j = pboc_files->files[i].record_firat; j <= pboc_files->files[i].record_last; j++)
        {

            uint8_t *tlv;
            uint32_t tlvlen;
            uint8_t *tagdata;
            uint32_t tag;
            uint8_t ret;

            TRACE("\nRead SFI:%d RECORD:%d\n", pboc_files->files[i].sfi, j);

            ret = PBOC_ReadRecord(j, pboc_files->files[i].sfi, &tlv, &tlvlen);

            if(flow_ind) flow_ind(2);

            if(!ret)
            {
                if(SDA_Count && pboc_files->sda_listlen != 0xffff)
                {

                    if(pboc_files->sda_listlen + tlvlen + 3 > 248)
                    {//2CC.144.00
                        free(pboc_files->sda_list);
                        pboc_files->sda_list = 0;
                        pboc_files->sda_listlen = 0xffff;                        
                    }
                    else
                    {
                        // range 11 to 30, we should add TAG, EMV book3 10.3
                        if(pboc_files->files[i].sfi > 10)
                        {
                            pboc_files->sda_list[pboc_files->sda_listlen++] = 0x70;
                            if(tlv[-2] == 0x81) //2 bytes length
                                pboc_files->sda_list[pboc_files->sda_listlen++] = tlv[-2]; //workaround for 2CS.015.01     
                            pboc_files->sda_list[pboc_files->sda_listlen++] = tlvlen;
                            PBOC_SetTVR(TVR_SET_SDA_SELECTED, 0);
                        }
                        
                        memcpy(pboc_files->sda_list + pboc_files->sda_listlen, tlv, tlvlen); //add new
                        pboc_files->sda_listlen += tlvlen;
                    }


                    SDA_Count--;
                }      

                while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &tagdata)) != 0)
                {
                    if((tag & 0xff00) == 0xDF00) //card didn't have df, pboc lvl testing item
                        continue;

                    if(PBOC_GetFileData(pboc_files, tag))
                    {
                        return 2; //tag exist
                    }
                        
                    PBOC_SetFileData(pboc_files, tag, tagdata);
                }

                if(tag == 0 && tlvlen != 0)
                    return 3; //wrong format
            }
            else if(ret == 3) //not 0x70 template
                return 1;
            else if(ret == 4) //not 9000 and not 6a83
                return 4;
            else if(ret == 1) //6a83
                return 4;

        }
    }

    return 0;
}

//脫機數據認證
uint8_t PBOCF_DataAuth(PBOC_TERM_DATA *pboc_data, PBOC_CARD_FILES *pboc_files)
{
    uint8_t idx, ret = 1;
    uint8_t *ptr;

    uint8_t *ca_mod = 0;
    uint8_t *ca_exp = 0;

    uint8_t *issue_mod = 0;
    uint8_t *issue_exp = 0;

    uint8_t *verified = 0;

    uint8_t DA_MODE = 0; //0: none, 1: SDA, 2: DDA, 3: CDA

    uint8_t *term_capp;
    uint8_t card_sda, card_dda, cv, trm/*終端風險管理*/, iv/*發卡行認證*/, card_cda;
    uint8_t term_sda, term_dda, term_cda;

    //sda used
    uint8_t N1;
    uint8_t Nca;
    uint8_t *residue;
    uint8_t hash[20];
    uint8_t *aip16;
    uint8_t aip;

    if(aip16 = PBOC_GetFileData(pboc_files, CTAG_AIP))
        aip = aip16[1];
    else
        aip = 0;

    term_capp = PBOC_GetTermData(pboc_data, TTAG_TERMINAL_CAP);
    if(!term_capp)
        return 2;
    term_capp ++;
    
    card_sda = !!(aip & AIP_SDA_BIT);
    card_dda = !!(aip & AIP_DDA_BIT);
    cv =  !!(aip & AIP_CV_BIT);
    trm = !!(aip & AIP_TRM_BIT);
    iv = !!(aip & AIP_IV_BIT);
    card_cda = !!(aip & AIP_CDA_BIT);

    TRACE("CARD: sda :%d, dda :%d, cv :%d, trm :%d, iv :%d, cda :%d\n", card_sda, card_dda, cv, trm, iv, card_cda);

    term_sda = !!(term_capp[2] & 0x80);
    term_dda = !!(term_capp[2] & 0x40);
    term_cda = !!(term_capp[2] & 0x08);

    TRACE("TERM: sda :%d, dda :%d, cda: %d\n", term_sda, term_dda, term_cda);

    if(term_cda && card_cda)
        DA_MODE = 3; //CDA
    else if(term_dda && card_cda)
        DA_MODE = 2; //DDA
    else if(term_sda && card_sda)
        DA_MODE = 1; //SDA
    else
    {
        PBOC_SetTVR(TVR_SET_NO_DA, 0);
        DA_MODE = 0;
    }


    TRACE("DA_MODE = %d\n", DA_MODE);

    if(DA_MODE == 1) //SDA
    {

        PBOC_SetTVR(TVR_SET_SDA_SELECTED, 0);//2CJ.018~020

        ptr = PBOC_GetFileData(pboc_files, CTAG_CA_PKI);
        if(!ptr)
            goto da_da_miss;

        
        //1. Check RID, PKI
        if(PBOC_GetCAPKI(pboc_files->aid+1, ptr[1], TTAG_CA_EXP, &ca_exp))
        {
            goto da_ca_fail;
        }


        if(PBOC_GetCAPKI(pboc_files->aid+1, ptr[1], TTAG_CA_MOD, &ca_mod))
        {
            goto da_ca_fail;
        }

        ptr = PBOC_GetFileData(pboc_files, CTAG_ISSUE_SIGNED);
        if(!ptr)
        {
            goto da_da_miss;
        }

        //2.a Check CA_mod and sign length

        if(ptr[0] != ca_mod[0])
            goto da_fail;

        verified =(uint8_t*) malloc(ca_mod[0]);
        if(!verified) goto da_malloc_fail;

        //2.b Verify issuer signed data
        if(CryptoRSAEncrypt(ptr+1, ca_exp+1, ca_mod+1, ptr[0], ca_exp[0], ca_mod[0], verified))
        {
            TRACE("rsa fail ca_mod[0-1] = %x %x ca_exp[0-1] = %x %x\n", ca_mod[0], ca_mod[1], ca_exp[0], ca_exp[1]);
            goto da_fail;
        }


        //2.c Check verified data  //public key
        if(!(verified[0] == 0x6A && verified[1] == 0x02/*pub key*/ && verified[11] == 0x01/*asm alg*/ && verified[12] == 0x01/*hash alg*/ && verified[ptr[0]-1] == 0xBC))
            goto da_fail;
        else //continue check PAN
        {
            uint8_t* pan, *date, pan_idx;
            pan = PBOC_GetFileData(pboc_files, CTAG_PAN);

            if(!pan || !pan[0])
                goto da_fail;

            for(pan_idx = 0; pan_idx < 4; pan_idx++)
            {
               //left sign
               if(((verified[2+pan_idx] & 0xf0) != 0xf0) && ((verified[2+pan_idx] & 0xf0) != (pan[1+ pan_idx] & 0xf0)))
                   goto da_fail;

               //right sign
               if(((verified[2+pan_idx] & 0x0f) != 0x0f) && ((verified[2+pan_idx] & 0x0f) != (pan[1+ pan_idx] & 0x0f)))
                   goto da_fail;
            }            

            date = PBOC_GetTermData(pboc_data, TTAG_DATE);

            if((/*1950~1999*/verified[7] >= 0x50) || ((date[1] << 8)/*YY*/ + date[2]/*MM*/ > (verified[7] << 8)/*YY*/ + verified[6]/*MM*/))
                goto da_fail;
        }

        N1 = verified[13];//key length
        Nca = ptr[0];
        residue = 0;

        if(N1 > Nca - 36)
        {
            residue = PBOC_GetFileData(pboc_files, CTAG_ISSUE_MOD_RESIDUE);
            if(!residue)
            {
                goto da_da_miss;
            }
        }


        //2.d Check Hash value
        issue_exp = PBOC_GetFileData(pboc_files, CTAG_ISSUE_EXP);
        if(!issue_exp)
            goto da_da_miss;

        CryptoSha1(0, 0, 0, 0); //init;
        CryptoSha1(1, verified+1, 14 + Nca - 36, 0);
        if(residue)
            CryptoSha1(1, residue+1, residue[0], 0);
        CryptoSha1(1, issue_exp+1, issue_exp[0], 0);
        CryptoSha1(2, 0, 0, hash);

        if(memcmp(hash, &verified[15 + Nca - 36], 20))
            goto da_fail;

        //2.e Combine issuer key
        issue_mod = (uint8_t*) malloc(N1 + 1);
        if(!issue_mod) goto da_malloc_fail;
        issue_mod[0] = N1;

        if(N1 > Nca - 36)
        {
            memcpy(issue_mod + 1, &verified[15], Nca - 36);
            memcpy(issue_mod + 1 + Nca - 36, residue + 1, N1 - (Nca - 36));
        }
        else
        {
            memcpy(issue_mod + 1, &verified[15], N1);
        }

        free(verified);
        verified = 0;

        //3.a Check data signed length
        ptr = PBOC_GetFileData(pboc_files, CTAG_DATA_SIGNED);
        if(!ptr)
            goto da_da_miss;

        if(ptr[0] != N1)
        {
            goto da_fail;
        }

        //3.b Verified signed data
        verified = (uint8_t*) malloc(N1);
        if(!verified) goto da_malloc_fail;        
        
        if(CryptoRSAEncrypt(&ptr[1], &issue_exp[1], &issue_mod[1], ptr[0], issue_exp[0], issue_mod[0], verified))
        {
            goto da_rsa_fail;
        }

        //3.c Check verified data
        if(!(verified[0] == 0x6A && verified[1] == 0x03 && verified[N1-1] == 0xBC ))
        {
            PBOC_SetTVR(TVR_SET_SDA_SELECTED, 0);//2CJ.018~020

            goto da_fail;
        }

        PBOC_SetFileDataLVSep(pboc_files, CTAG_DATA_AUTH_CODE, 2, verified+3); //3,4 is data authentication code        

        //3.d Check hash value
        CryptoSha1(0, 0, 0, 0); //init;
        CryptoSha1(1, verified+1, 4 + N1 - 26, 0);

        PBOC_BuildSDAList(pboc_files); //2nd chance, build sda_list, IF save memory before (2CC.144.00)

        if(pboc_files->sda_list)
            CryptoSha1(1, pboc_files->sda_list, pboc_files->sda_listlen, 0);

        //find if AIP(tag:0x82) need to included in hash
        if(ptr = PBOC_GetFileData(pboc_files, CTAG_DA_TAG_LIST))
        {
            if(ptr[1] == CTAG_AIP)
            {
                if(aip16)
                    CryptoSha1(1, aip16+1, 2, 0);
                else
                    CryptoSha1(1, "\x00\x00", 2, 0);
            }
        }
        CryptoSha1(2, 0, 0, hash);

        if(memcmp(hash, &verified[5 + N1 - 26], 20))
            goto da_fail;

        TRACE("auth success!\n");
        goto da_end;
    }

    goto da_no_executed;


da_da_miss:
   PBOC_SetTVR(TVR_IC_DATA_MISS, 0);
    ret = 3; goto da_fail;
da_ca_fail:
    ret = 3; goto da_fail;
da_rsa_fail:
TRACE("da_rsa_fail\n"); goto da_fail;
da_malloc_fail:
TRACE("da_malloc_fail\n");
da_fail:
TRACE("fail\n");
    if(issue_mod)
    {
        free(issue_mod);
        issue_mod = 0;
    }
    if(verified)
    {
        free(verified);
        verified = 0;
    }
    PBOC_SetTVR(TVR_SET_FAIL_SDA, 0);
    goto da_end;
da_end:
    PBOC_SetTSI(TSI_SET_DA_EXECUTED, 0);
da_no_executed:    
    if(issue_mod)
      {free(issue_mod);}
    if(verified)
        free(verified);

    return 0;
}

uint8_t PBOCF_ProcessRestriction(PBOC_TERM_DATA *pboc_data, PBOC_CARD_FILES *pboc_files)
{
    uint8_t *card_ptr, *term_ptr;    
    uint8_t *auc = 0;

    uint32_t cur_date, ttype_9c;

    term_ptr = PBOC_GetTermData(pboc_data, TTAG_TRANACTION_TYPE);

    if(!term_ptr)
        ttype_9c = 0x00;
    else
        ttype_9c = term_ptr[1];

    term_ptr = PBOC_GetTermData(pboc_data, TTAG_DATE);

    if(!term_ptr) return 2;    
    cur_date = (term_ptr[1] << 16) + (term_ptr[2] << 8) + term_ptr[3];
    TRACE("current date = %x\n", cur_date);
    
    
    //Check Ver
    if((card_ptr = PBOC_GetFileData(pboc_files, CTAG_APP_VER)))
    {
        if((term_ptr = PBOC_GetAID(pboc_files->aid, TTAG_APP_VER)))
        {
            if(memcmp(term_ptr+1, card_ptr+1, term_ptr[0]))
                PBOC_SetTVR(TVR_SET_APPVER_NOMATCH, 0);
        }
        else
        {
            uint8_t app_ver[3];
            PBOC_AutoTermAppVer(pboc_files->aid, app_ver);
            if(memcmp(app_ver+1, card_ptr+1, app_ver[0]))
                PBOC_SetTVR(TVR_SET_APPVER_NOMATCH, 0);
        }
    }

    if((card_ptr = PBOC_GetFileData(pboc_files, CTAG_ISSUER_CONTRY)))
    {
        if((auc = PBOC_GetFileData(pboc_files, CTAG_APP_AUC)))
        {
            //we are not ATM, so check
            if(!(auc[1] & 1))//card not allow terminals which is not ATM
                PBOC_SetTVR(TVR_SET_SERVICE_NOT_SUPPORT, 0);
            else //continue checking
            {
                uint8_t country[2];
                
                term_ptr = PBOC_GetTermData(pboc_data, TTAG_CONTRY);

                if(term_ptr) memcpy(country, term_ptr+1, 2);
                else
                {
                    country[0] = 0x01;
                    country[1] = 0x56;
                }

                if(!memcmp(country, card_ptr+1, 2)) //local
                {
                    if(ttype_9c == 0x00) //goods and service
                    {
                        if(!(auc[1] & 0x28)) //國內商品或服務有校失敗
                        {
                            PBOC_SetTVR(TVR_SET_SERVICE_NOT_SUPPORT, 0);
                        }
                    }
                }
                else //international
                {
                    if(ttype_9c == 0x00) //goods and service
                    {
                        if(!(auc[1] & 0x14)) //國際商品或服務有校失敗
                        {
                            PBOC_SetTVR(TVR_SET_SERVICE_NOT_SUPPORT, 0);
                        }
                    }

                }
            }
        }
    }


    //we are not ATM
    if(auc)
        if(!(auc[1] & 0x01))
            PBOC_SetTVR(TVR_SET_SERVICE_NOT_SUPPORT, 0);

    if((card_ptr = PBOC_GetFileData(pboc_files, CTAG_APP_START_DATE)))
    {
        uint32_t cdate;

        //check month date
        if(card_ptr[2] > 0x12 || !card_ptr[2] || card_ptr[3] > 0x31 || !card_ptr[3] || (card_ptr[2] == 0x2 && card_ptr[3] > 0x29))
            return 1;
        
        cdate = (card_ptr[1] << 16) + (card_ptr[2] << 8) + card_ptr[3];

        if(cdate > cur_date && (/*2000~2049*/card_ptr[1] < 0x50))
        {
            PBOC_SetTVR(TVR_SET_APP_NOT_BORN, 0);
        }
    }

    if((card_ptr = PBOC_GetFileData(pboc_files, CTAG_APPEXPIRE)))
    {
        uint32_t cdate;

        //check month date
        if(card_ptr[2] > 0x12 || !card_ptr[2] || card_ptr[3] > 0x31 || !card_ptr[3] || (card_ptr[2] == 0x2 && card_ptr[3] > 0x29))
            return 1;

        cdate = (card_ptr[1] << 16) + (card_ptr[2] << 8) + card_ptr[3];

        if(cdate < cur_date || (/*1950~1999*/card_ptr[1] >= 0x50))
        {
            PBOC_SetTVR(TVR_SET_APP_EXPIRE, 0);
        }
    }


    return 0;
}

uint8_t PBOCF_CVM(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files)
{
    uint8_t *cvm;
    uint32_t x, y;
    uint8_t *cvm_head;
    uint8_t *cvm_end;
    uint8_t *term_ptr;
    uint8_t cvm_result[3];
    uint32_t val_9f02_u32 = 0;
    
    uint8_t *aip16;
    uint8_t aip;
    uint8_t *expire, *pan;
    uint8_t *app_cur, *tran_cur;
    uint8_t curr_match;


    curr_match = 0;
    tran_cur = PBOC_GetTermData(pboc_term, TTAG_MONEY_CONTRY);
    app_cur = PBOC_GetFileData(pboc_files, CTAG_APP_CURRENCY);

    if(tran_cur && app_cur)
    {
        if(tran_cur[0] == app_cur[0])
        {
            if(!memcmp(tran_cur+1, app_cur+1, app_cur[0]))
                curr_match = 1;
        }
    }
    
    //2CJ.014.00
    expire = PBOC_GetFileData(pboc_files, CTAG_APPEXPIRE);
    pan = PBOC_GetFileData(pboc_files, CTAG_PAN);
    if(!expire || !expire[0] || !pan || !pan[0])
    {
        return 2;
    }
    
    if(aip16 = PBOC_GetFileData(pboc_files, CTAG_AIP))
        aip = aip16[1];
    else
        aip = 0;

    cvm_result[2] = 0; //unknown;

    if(!!(aip & AIP_CV_BIT))
    {
        uint8_t was_supported = 0;
        
        if(cvm = PBOC_GetFileData(pboc_files, CTAG_CVM))
        {
            if((cvm[0] & 1))
                return 2; //bad length 2CL.045.00
            if(cvm[0] >= 10)
            {
                x = (cvm[1] << 24) + (cvm[2] << 16) +(cvm[3] << 8) +(cvm[4] << 0); 
                y = (cvm[5] << 24) + (cvm[6] << 16) +(cvm[7] << 8) +(cvm[8] << 0); 
                cvm_head = cvm + 9;
                cvm_end = cvm + cvm[0];

                if(x || y)
                {
                    term_ptr = PBOC_GetTermData(pboc_term, TTAG_AUTH_QUAN);
                    if(term_ptr && term_ptr[0] == 6)
                    {
                        val_9f02_u32 = BCD6byte_2Number(term_ptr+1);
                    }
                }

                
                while(cvm_head < cvm_end)
                {
                    uint8_t execute_cvm;
                    
                    if(cvm_head[1] == CVM_COND_ALWAYS || cvm_head[1] == CVM_COND_NO_CASH)
                        execute_cvm = 1;
                    else if(cvm_head[1] == CVM_COND_SUPPORT )
                    {
                        uint8_t CVM;

                        CVM = cvm_head[0] & 0x3f; 

                        if(CVM == CVM_TYPE_NO_CVM || CVM == CVM_TYPE_FAIL || CVM == CVM_TYPE_ON_PIN) //support
                            execute_cvm = 1;
                        else
                            execute_cvm = 0;
                    }
                    else if(cvm_head[1] == CVM_COND_LESS_X && x && val_9f02_u32 < x && curr_match)
                        execute_cvm = 1;
                    else if(cvm_head[1] == CVM_COND_MORE_X && x && val_9f02_u32 > x && curr_match)
                        execute_cvm = 1;
                    else if(cvm_head[1] == CVM_COND_LESS_Y && y && val_9f02_u32 < y && curr_match)
                        execute_cvm = 1;
                    else if(cvm_head[1] == CVM_COND_MORE_Y && y && val_9f02_u32 > y && curr_match)
                        execute_cvm = 1;
                    else
                        execute_cvm = 0;

                    if(execute_cvm)
                    {
                        uint8_t CVM;

                        CVM = cvm_head[0] & 0x3f; 
                        was_supported = 1;

                        if(CVM == CVM_TYPE_FAIL)
                        {
                            PBOC_SetTVR(TVR_SET_CVM_FAIL, 0);
                            cvm_result[2] = 1; //fail
                            goto cvm_result;
                        }
                        else if(CVM == CVM_TYPE_NO_CVM)
                        {
                            cvm_result[2] = 2; //success
                            goto cvm_result;
                        }
                        else if(CVM == CVM_TYPE_OFF_PIN || CVM == CVM_TYPE_OFF_PIN_AND_SIGN || CVM == CVM_TYPE_OFF_PIN2 || CVM == CVM_TYPE_OFF_PIN3)
                        {
                            //this is recognized, but not support
                            was_supported = 0;
                            PBOC_SetTVR(TVR_KB_FAIL, 0);
                            cvm_result[2] = 1; //fail, continue check
                        }
                        else if(CVM == CVM_TYPE_ON_PIN)
                        {                            
                            PBOC_AddTermGenData(pboc_term, "\x03", 1, TTAG_HINT_PIN, 2);
                            PBOC_SetTVR(TVR_CONNECT_PIN_INPUTED, 0);
                            cvm_result[2] = 0; //unknown, specs say, 2CJ.074.00, EMV4.3 10.5.5 U
                            goto cvm_result;
                        }
                        else if(CVM == CVM_TYPE_SIGN)
                        {
                            was_supported = 0;
                            cvm_result[2] = 1; //fail, continue check
                        }
                        else //unknown
                        {
                            was_supported = 0;
                            PBOC_SetTVR(TVR_SET_UNKNOWN_CVM, 0);
                            cvm_result[2] = 1; //fail, continue check
                        }
                    }

                    if(execute_cvm) //only on recognized condition
                    {
                        if(!(cvm_head[0] & 0x40) || (cvm_head + 2 > cvm_end)) //no next
                        {
                            if(!was_supported)
                            {
                                goto cvm_fail;
                                
                                return 0;
                            }
                            else
                            {
                                PBOC_SetTVR(TVR_SET_CVM_FAIL, 0);
                                goto cvm_result;
                                return 0;
                            }
                        }
                    }

                    cvm_head += 2;
                }
                
cvm_result: //CVM Processing (Part 1 of 5) <=====(((C)))
                if(cvm_head < cvm_end)
                {
                    PBOC_SetTSI(TSI_SET_CVM_EXECUTED, 0);
                    cvm_result[0] = cvm_head[0];
                    cvm_result[1] = cvm_head[1];
                    PBOC_AddTermGenData(pboc_term, cvm_result, 3, TTAG_CVM_RESULT, 2);
                    return 0;
                }

            }
            else //2CL.044
            {
                goto cvm_miss;
            }

            cvm_result[2] = 1;

cvm_fail:
            PBOC_SetTSI(TSI_SET_CVM_EXECUTED, 0);
            PBOC_SetTVR(TVR_SET_CVM_FAIL, 0);
            cvm_result[0] = 0x3F;
            cvm_result[1] = 0;
            PBOC_AddTermGenData(pboc_term, cvm_result, 3, TTAG_CVM_RESULT, 2);
            
        }
        else
        {
cvm_miss:
            cvm_result[0] = 0x3F;
            cvm_result[1] = 0;
            PBOC_AddTermGenData(pboc_term, cvm_result, 3, TTAG_CVM_RESULT, 2);
            PBOC_SetTVR(TVR_IC_DATA_MISS, 0);
            return 0;
        }
    }
    else //CVM not support, 2CI.027.00
    {
        cvm_result[0] = 0x3F;
        cvm_result[1] = 0;
        cvm_result[2] = 0;
        PBOC_AddTermGenData(pboc_term, cvm_result, 3, TTAG_CVM_RESULT, 2);
    }
    
    return 0;
}

uint8_t PBOCF_TermRiskManager(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files)
{
    RESAPD resApdu;
    uint8_t *lower_bound, *upper_bound;
    uint8_t *miniqua, df72, val_9f02[6];
    uint32_t miniqua_u32, val_9f02_u32;
    uint16_t atc, last_atc;
    uint8_t *term_ptr;
    uint8_t *dat;
    resApdu.data = pboc_buf;
    resApdu.len = sizeof(pboc_buf);


    term_ptr = PBOC_GetTermData(pboc_term, TTAG_FORCE_CONNECT);


    if(!term_ptr || term_ptr[0] != 1)
        df72 = 0; //錯誤, 預設不連機
    else
        df72 = term_ptr[1];

    if(df72 != 1)
    {
        dat = PBOC_GetTermData(pboc_term, TTAG_TERMINAL_TYPE);
        if((dat && ((dat[1] & 0xf) == 0x4 || (dat[1] & 0xf) == 0x1))) //online only terminal
            df72 = 1;
    }
    
    term_ptr = PBOC_GetTermData(pboc_term, TTAG_AUTH_QUAN);

    if(!term_ptr && term_ptr[0] != 6)
    {
        val_9f02_u32 = 0;
    }
    else     
         val_9f02_u32 = BCD6byte_2Number(term_ptr+1);

    //異常文件檢查   
    //商戶強制交易連機
    if(df72 == 1)
    {
        PBOC_SetTVR(TVR_FORCE_CONNECTED, 0);
    }
    
    //最低限額
    miniqua_u32 = 0;

    miniqua = PBOC_GetAID(pboc_files->aid, TTAG_FLOOR_LMT);
    if(miniqua)
        miniqua_u32 = (miniqua[1] << 24) + (miniqua[2] << 16) + (miniqua[3] << 8) + (miniqua[4]);
    
//    val_9f02_u32 = (val_9f02[2] << 24) + (val_9f02[3] << 16) + (val_9f02[4] << 8) + (val_9f02[5]);
  //  val_9f02_u32 = BCD2Number(val_9f02_u32);

    if(val_9f02_u32 >= miniqua_u32)
        PBOC_SetTVR(TVR_EXCEED_FLOOR_LIMIT, 0);
    
    //隨機交易選擇
#ifndef WIN32
#warning "warninig TBD"
#endif
    //頻度檢查
    lower_bound = PBOC_GetFileData(pboc_files, CTAG_OFF_TRACK_LOWER_BOND); //連續脫機下限
    upper_bound = PBOC_GetFileData(pboc_files, CTAG_OFF_TRACK_UPPER_BOND);//連續脫機上限

    atc = last_atc = 0xffff;
    if(lower_bound && upper_bound)
    {
        if(!GetData(CTAG_ATC, resApdu.data, &resApdu.len))        
            if(resApdu.data[resApdu.len - 1] == 0x00 && resApdu.data[resApdu.len - 2] == 0x90)
            {
                atc = (resApdu.data[3] << 8) + resApdu.data[4];

                resApdu.data[2] = 2; //it's trick, we stolen it for length of lv
                PBOC_SetFileData(pboc_files, CTAG_ATC, &resApdu.data[2]);
            }
        
        if(!GetData(CTAG_CATC, resApdu.data, &resApdu.len))
            if(resApdu.data[resApdu.len - 1] == 0x00 && resApdu.data[resApdu.len - 2] == 0x90)
            {
                last_atc = (resApdu.data[3] << 8) + resApdu.data[4];
                resApdu.data[2] = 2; //it's trick, we stolen it for length of lv
                PBOC_SetFileData(pboc_files, CTAG_CATC, &resApdu.data[2]);
            }

        if(atc == 0xffff || last_atc == 0xffff)
        {
            PBOC_SetTVR(TVR_IC_DATA_MISS, 0);
            PBOC_SetTVR(TVR_EXCEED_LOWER_TIME, 0);
            PBOC_SetTVR(TVR_EXCEED_UPPER_TIME, 0);
        }
        else
        {
            if(atc <= last_atc || atc - last_atc > lower_bound[1])
            {
                PBOC_SetTVR(TVR_EXCEED_LOWER_TIME, 0);
            }

            if(atc <= last_atc || atc - last_atc > upper_bound[1])
            {
                PBOC_SetTVR(TVR_EXCEED_UPPER_TIME, 0);
            }
        }
    }

    //新卡檢查
    if(last_atc == 0)
    {
        PBOC_SetTVR(TVR_NEW_CARD, 0);
    }

    PBOC_SetTSI(TSI_SET_TRM_EXECUTED, 0);
    return 0;
}


void Hex2BCD(uint32_t len, uint8_t *bcd)
{
    bcd[1] = (len % 10);
    len = len / 10;
    bcd[1] |= ((len % 10) << 4);
    len = len / 10;
    bcd[0] = (len % 10) & 0xf;
    len = len / 10;
    bcd[0] |= ((len % 10) << 4);

}

static uint8_t Array5BitAnd(uint8_t a[5], uint8_t b[5])
{
    uint8_t i;
    for(i = 0; i < 5; i++)
    {
        if(a[i] & b[i])
            return 1;
    }

    return 0;
}

uint8_t PBOCF_TermActionAna(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files, uint8_t *AC_P1)
{
    uint8_t iac_default[5], iac_refuse[5], iac_connect[5];
    uint8_t tac_default[5], tac_refuse[5], tac_connect[5];
    uint8_t tvr[5];
    uint8_t *dat;
    uint8_t arc[2];

    *AC_P1 = 0;
    PBOC_SetTVR(TVR_GET, tvr);
    dat = PBOC_GetFileData(pboc_files, CTAG_IAC_DEFAULT);
    if(dat)
        memcpy(iac_default, dat+1, 5);
    else
        memset(iac_default, 0xff, 5);
    
    dat = PBOC_GetFileData(pboc_files, CTAG_IAC_REFUSE);
    if(dat)
        memcpy(iac_refuse, dat+1, 5);
    else
        memset(iac_refuse, 0, 5);
    
    dat = PBOC_GetFileData(pboc_files, CTAG_IAC_CONNECT);
    if(dat)
        memcpy(iac_connect, dat+1, 5);
    else
        memset(iac_connect, 0xff, 5);

    dat = PBOC_GetAID(pboc_files->aid, TTAG_TAC_DEFAULT);
    if(dat)
        memcpy(tac_default, dat+1, 5);
    else
        memset(tac_default, 0, 5);
    dat = PBOC_GetAID(pboc_files->aid, TTAG_TAC_DENIAL);
    if(dat)
        memcpy(tac_refuse, dat+1, 5);
    else
        memset(tac_refuse, 0, 5);
    dat = PBOC_GetAID(pboc_files->aid, TTAG_TAC_ONLINE);
    if(dat)
        memcpy(tac_connect, dat+1, 5);
    else
        memset(tac_connect, 0, 5);

    //handler
    if(Array5BitAnd(tvr, iac_refuse) || Array5BitAnd(tvr, tac_refuse))
    {
        TRACE("Z1!"); //脫機拒絕
        arc[0] = 'Z';
        arc[1] = '1';
        PBOC_AddTermGenData(pboc_term, arc, 2, TTAG_ARC, 1); //issuer跟 terminal都有這個tag, 但是Generate 的在第一次AC後會被清, 所以不用擔心

        //generace AAC
        *AC_P1 = 0x00; //AAC no CDA
    }
    else
    {
        if(Array5BitAnd(tvr, iac_connect) || Array5BitAnd(tvr, tac_connect))
        {
            //generate ARQC
            *AC_P1 = 0x80; //ARQC no CDA
        }
        else
        {
            uint8_t *term_ptr, df72;
            term_ptr = PBOC_GetTermData(pboc_term, TTAG_FORCE_CONNECT);

            if(!term_ptr || term_ptr[0] != 1)
                df72 = 0; //錯誤, 預設不連機
            else
                df72 = term_ptr[1];

            dat = PBOC_GetTermData(pboc_term, TTAG_TERMINAL_TYPE);

            if((dat && ((dat[1] & 0xf) == 0x4 || (dat[1] & 0xf) == 0x1)) || df72) //online only check
                *AC_P1 = 0x80; //ARQC no CDA
            else
            {
                TRACE("Y1"); //脫機批准
                arc[0] = 'Y';
                arc[1] = '1';
                PBOC_AddTermGenData(pboc_term, arc, 2, TTAG_ARC, 1); //issuer跟 terminal都有這個tag, 但是Generate 的在第一次AC後會被清, 所以不用擔心
                //generate TC
                *AC_P1 = 0x40; //TC no CDA
            }
        }

    }

    return 0;
    
}

uint8_t PBOCF_CardActionAna(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files, uint8_t AC_P1)
{
    uint8_t *cdol1;
    uint32_t tag;
    uint8_t *tl;
    uint32_t tllen;
    uint32_t taglen;
    uint8_t *ac_input, *term_ptr;
    uint16_t ac_input_idx, k;
    RESAPD resApdu;
    uint16_t result;

    resApdu.data = pboc_buf;
    resApdu.len = sizeof(pboc_buf);

    cdol1 = PBOC_GetFileData(pboc_files, CTAG_CDOL2);
    if(!cdol1 || !cdol1[0]) return 1; //2CJ.016.00
    
    cdol1 = PBOC_GetFileData(pboc_files, CTAG_CDOL1);
    if(!cdol1 || !cdol1[0]) return 1;

    ac_input = (uint8_t*) malloc(256+6);
    ac_input_idx = 5;
    
    ac_input[0] = 0x80;
    ac_input[1] = 0xAE;
    ac_input[2] = AC_P1;
    ac_input[3] = 0x00;
  //  ac_input[4] = len
    
    tl = &cdol1[1];
    tllen = cdol1[0];


    PBOC_BuildDOLList(pboc_term, pboc_files, &tl, &tllen, ac_input, &ac_input_idx);

    ac_input[4] = ac_input_idx - 5;

//    ac_input[ac_input_idx] = 0;
    resApdu.len = sizeof(pboc_buf);
    if(TransparantAPDU(ac_input, ac_input_idx, resApdu.data, &resApdu.len))
        goto cb_fail;


    result = (resApdu.data[resApdu.len - 2] << 8) + resApdu.data[resApdu.len - 1];
    if(result != 0x9000)
        goto cb_fail;

    
    if(resApdu.data[0] == 0x80)
    {
        uint8_t *ptr;
        uint8_t *dat;
        uint32_t template_len;

        if(resApdu.data[1] & 0x80) //two byte length 0x81 0xXX
            ptr = &resApdu.data[1];
        else
            ptr = &resApdu.data[0];


        template_len = (&resApdu.data[resApdu.len - 2] - &ptr[2]);

        if(ptr[1] != template_len)
            goto cb_fail; //2CL.004.00

        ptr[0] = ptr[1]; //move length to 80 place
        
        ptr[1] = 1; //it's trick, we stolen it for length of lv

//        if(AC_P1 != CID_TC)               
  //          ptr[2] &= AC_P1; //resist up grade response, 2CK.015.00

        PBOC_SetFileData(pboc_files, CTAG_CID, &ptr[1]);


        dat = PBOC_GetFileData(pboc_files, CTAG_ATC);

        if(!dat)
        {
            ptr[2] = 2; //it's trick, we stolen it for length of lv
            PBOC_SetFileData(pboc_files, CTAG_ATC, &ptr[2]);
        }
        else
        {
            memcpy(dat+1, &ptr[3], 2);
        }


        ptr[4] = 8; //it's trick, we stolen it for length of lv
        PBOC_SetFileData(pboc_files, CTAG_AC, &ptr[4]);

        if(ptr[0] >= 11 && (ptr[0] <= 32+11))
        {
            ptr[12] = (ptr[0]-11); //it's trick, we stolen it for length of lv
            PBOC_SetFileData(pboc_files, CTAG_ISSUE_APDATA, &ptr[12]);
        }
        else
           goto cb_fail;
    }
    else if(resApdu.data[0] == 0x77)
    {
        uint8_t *tlv;
        uint32_t tlvlen, template_len;
        uint8_t *taglv;

        if(resApdu.data[1] & 0x80)
        {
            tlvlen = resApdu.data[2];
            tlv = &resApdu.data[3];
        }
        else
        {
            tlvlen = resApdu.data[1];
            tlv = &resApdu.data[2];
        }

        template_len = (&resApdu.data[resApdu.len - 2] - tlv);

        if(tlvlen != template_len)
            goto cb_fail; //2CL.004.00
        
        while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &taglv)) != 0)
        {
            uint8_t *dat;
			uint8_t no_store;

			no_store = 0;

            if(tag == CTAG_CID)
            {
                if(taglv[0] != 1)
                    goto cb_fail;

//                if(AC_P1 != CID_TC)               
  //                  taglv[1] &= AC_P1; //resist up grade response

            }
            else if(tag == CTAG_ATC)
            {
                if(taglv[0] != 2)
                    goto cb_fail;
            }
            else if(tag == CTAG_AC)
            {
                if(taglv[0] != 8)
                    goto cb_fail;
            }
            else if(tag == CTAG_ISSUE_APDATA)
            {
            }
            else
            {
//                goto cb_fail;
                no_store = 1;
            }

           if(!no_store)
            {
                dat = PBOC_GetFileData(pboc_files, tag);
                if(!dat)
                    PBOC_SetFileData(pboc_files, tag, taglv); //CID, ATC, AC, APDATA
                else
                    memcpy(dat+1, taglv+1, dat[0]);
            }
        } 

        if(tlvlen != 0) //2CL.004.00 case 3
            goto cb_fail;
    }
    else //bad tag
        goto cb_fail;
    
    free(ac_input);
    PBOC_SetTSI(TSI_SET_CARD_BEHAVIOR_ANA_EXECUTED, 0);
    return 0;
    
cb_fail:
    free(ac_input);
    return 1;    
}

uint8_t PBOCF_ExtAuth_2ndAC(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files, uint8_t *AC_P2)
{
    uint8_t iac_default[5];
    uint8_t tac_default[5];
    uint8_t tvr[5];    
    uint8_t *iauth_data, *iarc, *dat;
    uint8_t arc[2];
    uint8_t *lv71, *cdol2;
    uint8_t ac_p1;
    RESAPD resApdu;
    uint32_t tag;
    uint8_t *tl;
    uint32_t tllen;
    uint32_t taglen;
    uint8_t *ac_input, *term_ptr;
    uint16_t ac_input_idx, k;
    
    uint8_t *aip16;
    uint8_t aip;

    uint8_t *tlv;
    uint32_t tlvlen;
    uint8_t *taglv;
    uint8_t tag_check;
    uint32_t exceed_128_cnt;


    if(aip16 = PBOC_GetFileData(pboc_files, CTAG_AIP))
        aip = aip16[1];
    else
        aip = 0;

    resApdu.data = pboc_buf;
    resApdu.len = sizeof(pboc_buf);
    
    
    //是否有發卡行認證數據?
    if(iauth_data = PBOC_GetTermData(pboc_term, TTAG_ISSUER_AUTH_DATA))
    {
        //card 是否支持外部認證
        if(!!(aip & AIP_IV_BIT)) //支持
        {
            ExtAuthenticate(iauth_data+1, iauth_data[0], resApdu.data, &resApdu.len); 

            PBOC_SetTSI(TSI_SET_IA_EXECUTED, 0);
            
            if(resApdu.data[resApdu.len - 1] != 0x00 || resApdu.data[resApdu.len - 2] != 0x90)
            {
                PBOC_SetTVR(TVR_ISSUER_AUTH_FAIL, 0);
            }

        }
    }

    //是否連機?用授權響應馬判斷
    iarc = PBOC_GetTermData(pboc_term, TTAG_ARC);
    if(iarc && (iarc[0] == 2))
    {
        memcpy(arc, iarc+1, 2);
    }
    else //沒有或長度錯誤, 自己做一個   //後來改成直接AAC, BCTC要求
    {
#if 0    
        PBOC_SetTVR(TVR_GET, tvr);
        dat = PBOC_GetFileData(pboc_files, CTAG_IAC_DEFAULT);
        if(dat)
            memcpy(iac_default, dat+1, 5);
        else
            memset(iac_default, 0xff, 5);

        dat = PBOC_GetAID(pboc_files->aid, TTAG_TAC_DEFAULT);
        if(dat)
            memcpy(tac_default, dat+1, 5);
        else
            memset(tac_default, 0xff, 5);


        if(Array5BitAnd(tvr, iac_default) || Array5BitAnd(tvr, tac_default))
#else
        if(1)
#endif            
        {
            TRACE("Z3!");
            memcpy(arc, "Z3", 2);
        }
        else
        {
            TRACE("Y3!");
            memcpy(arc, "Y3", 2);
        }       

        if(iarc) iarc[-1] = TTAG_FAKE;//destrory previous TTAG_ARC
        
        PBOC_AddTermGenData(pboc_term, arc, 2, TTAG_ARC, 1);
    }
    if(!memcmp(arc, "00", 2) || !memcmp(arc, "10", 2) || !memcmp(arc, "11", 2) || !memcmp(arc, "Y3", 2)) //TC
    {
        *AC_P2 = ac_p1 = 0x40;//tc no cda
    }
    else
        *AC_P2 = ac_p1 = 0x00; //aac no cda

    //是否有腳本1?
    tlv = pboc_term->transaction_ltlv+1;
    tlvlen = pboc_term->transaction_ltlv[0];
    exceed_128_cnt = 0;            
    while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv71)))
    {

        //if(script = PBOC_GetTermData(pboc_term, TTAG_ISSUER_SCRIPT1))
        if(tag == TTAG_ISSUER_SCRIPT1)
        {
            uint8_t *tlvscripts, *lvscripts;
            uint32_t tlvscripts_len, tagscripts;
            uint8_t script_result[5], script_cmd_idx;

            //TRACE("script found");
            tlvscripts_len = lv71[0];
            tlvscripts = lv71 + 1;
            script_cmd_idx = 0;

            
            memset(script_result, 0, 5);

    
            exceed_128_cnt += (tlvscripts_len+2/*including 71 and len*/);
            
            while((tagscripts = PBOC_TLVEnum(&tlvscripts, &tlvscripts_len, &lvscripts)))
            {

                if(tagscripts == 0x9F18) //scripts idendifier
                {
                    if(lvscripts[0] == 4)
                        memcpy(script_result+1, lvscripts+1, 4);

                    script_result[0] = 0;
                }
                else if(tagscripts == 0x86)
                {
                    if(exceed_128_cnt > 128)
                        break;
                    script_cmd_idx++;

                    if(lvscripts[0] != lvscripts[5] + 5)
                        break;

                    if(!TransparantAPDU(lvscripts + 1, lvscripts[0], resApdu.data, &resApdu.len))
                    {
                        if(resApdu.data[resApdu.len - 2] != 0x90 && resApdu.data[resApdu.len - 2] != 0x63 && resApdu.data[resApdu.len - 2] != 0x62)
                        {
//                            PBOC_SetTVR(TVR_SCRIPT_FAIL_BEFORE_LAST_AC, 0); //腳本失敗 before generate AC
//                            PBOC_SetTSI(TSI_SET_SCRIPT_EXECUTED, 0); //腳本已執行
                            script_result[0] = (1 << 4) + (script_cmd_idx & 0xf);
                            break;
                        }
                    }
                    else
                        break;
                }
                else
                    break;
            }
                            

            if(tagscripts == 0)
            {
                script_result[0] = (2 << 4) + 0;
            }
            else
                PBOC_SetTVR(TVR_SCRIPT_FAIL_BEFORE_LAST_AC, 0); //腳本失敗 before generate AC                

            PBOC_SetTSI(TSI_SET_SCRIPT_EXECUTED, 0); //腳本已執行

            PBOC_AddTermGenData(pboc_term, script_result, 5, TTAG_SCRIPT_RESULT, 2);            
        }
    }

    //第二次generate AC
    cdol2 = PBOC_GetFileData(pboc_files, CTAG_CDOL2);
    if(!cdol2) return 1;

    ac_input = (uint8_t*) malloc(256+6);
    ac_input_idx = 5;
    
    ac_input[0] = 0x80;
    ac_input[1] = 0xAE;
    ac_input[2] = ac_p1;
    ac_input[3] = 0x00;
    
    tl = &cdol2[1];
    tllen = cdol2[0];

    PBOC_BuildDOLList(pboc_term, pboc_files, &tl, &tllen, ac_input, &ac_input_idx);

    ac_input[4] = ac_input_idx - 5;
//    ac_input[ac_input_idx] = 0;

    resApdu.len = sizeof(pboc_buf);

    if(TransparantAPDU(ac_input, ac_input_idx, resApdu.data, &resApdu.len))
    {
        free(ac_input);
        return 1;
    }

    if((resApdu.data[resApdu.len - 2] << 8) + resApdu.data[resApdu.len - 1] != 0x9000)
    {
        free(ac_input);
        return 1;
    }


    if(resApdu.data[0] == 0x80)
    {
        uint8_t *ptr;
        uint8_t *dat;

        if(resApdu.data[1] & 0x80) //two byte length 0x81 0xXX
            ptr = &resApdu.data[1];
        else
            ptr = &resApdu.data[0];

        if(ptr[1] < 11)
        {
            free(ac_input);
            return 1;
        }

        ptr[0] = ptr[1]; //move length to 80 place
        
        dat = PBOC_GetFileData(pboc_files, CTAG_CID);

        if(dat)
        {
//           if(ac_p1 != CID_TC)               
  //             dat[1] = ptr[2] & ac_p1; //update CID
    //       else
               dat[1] = ptr[2];
        }

        dat = PBOC_GetFileData(pboc_files, CTAG_ATC);

        if(dat)
        {
            memcpy(dat+1, &ptr[3], 2);
        }

        dat = PBOC_GetFileData(pboc_files, CTAG_AC);

        if(dat)
        {
            memcpy(dat+1, &ptr[5], 8);
        }

        dat = PBOC_GetFileData(pboc_files, CTAG_ISSUE_APDATA);

        if(dat)
        {
             memcpy(dat+1, &ptr[13], dat[0]);
        }

        tag_check = 3;//or 4 what ever
    }
    else if(resApdu.data[0] == 0x77)
    {
        uint8_t *tlv;
        uint32_t tlvlen;
        uint8_t *taglv;

        if(resApdu.data[1] & 0x80)
        {
            tlvlen = resApdu.data[2];
            tlv = &resApdu.data[3];
        }
        else
        {
            tlvlen = resApdu.data[1];
            tlv = &resApdu.data[2];
        }

        tag_check = 0;
        while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &taglv)) != 0)
        {
            uint8_t *dat;


            if(tag == CTAG_CID)
            {
                if(taglv[0] != 1)
                    goto ac2_fail;
                tag_check++;

                //if(ac_p1 != CID_TC)               
                  //  taglv[1] &= ac_p1; //resist up grade response

            }
            else if(tag == CTAG_ATC)
            {
                if(taglv[0] != 2)
                    goto ac2_fail;
                tag_check++;

            }
            else if(tag == CTAG_AC)
            {
                if(taglv[0] != 8)
                    goto ac2_fail;
                tag_check++;
            }
            else if(tag == CTAG_ISSUE_APDATA)
            {
            }
            else
            {
                goto ac2_fail;
            }
            
            dat = PBOC_GetFileData(pboc_files, tag);
            if(!dat)
                PBOC_SetFileData(pboc_files, tag, taglv); //CID, ATC, AC, APDATA
            else
                memcpy(dat+1, taglv+1, MIN(dat[0], taglv[0]));
        }

        if(tlvlen != 0) //2CL.004.00 case 3
            goto ac2_fail;

    }
    else
        goto ac2_fail;
            
    free(ac_input);    
    if(tag_check < 3)
        return 3; //data miss

    tlv = pboc_term->transaction_ltlv+1;
    tlvlen = pboc_term->transaction_ltlv[0];
    exceed_128_cnt = 0;
    while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv71)))
    {
        
        //if(script = PBOC_GetTermData(pboc_term, TTAG_ISSUER_SCRIPT1))
        if(tag == TTAG_ISSUER_SCIRPT2)
        {
            uint8_t *tlvscripts, *lvscripts;
            uint32_t tlvscripts_len, tagscripts;
            uint8_t script_result[5], script_cmd_idx;
            
            //TRACE("script found");
            tlvscripts_len = lv71[0];
            tlvscripts = lv71 + 1;
            script_cmd_idx = 0;
            memset(script_result, 0, 5);

            exceed_128_cnt += (tlvscripts_len+2/*including 72 and len*/);

            while((tagscripts = PBOC_TLVEnum(&tlvscripts, &tlvscripts_len, &lvscripts)))
            {
                if(tagscripts == 0x9F18) //scripts idendifier
                {
                    if(lvscripts[0] == 4)
                        memcpy(script_result+1, lvscripts+1, 4);
                    script_result[0] = 0;
                }
                else if(tagscripts == 0x86)
                {
                    if(exceed_128_cnt > 128)
                        break;

                    script_cmd_idx++;

                    if(lvscripts[0] != lvscripts[5] + 5)
                        break;
                        
                    if(!TransparantAPDU(lvscripts + 1, lvscripts[0], resApdu.data, &resApdu.len))
                    {
                        if(resApdu.data[resApdu.len - 2] != 0x90 && resApdu.data[resApdu.len - 2] != 0x63 && resApdu.data[resApdu.len - 2] != 0x62)
                        {
         //                   PBOC_SetTVR(TVR_SCRIPT_FAIL_AFTER_LAST_AC, 0); //腳本失敗 after generate AC
           //                 PBOC_SetTSI(TSI_SET_SCRIPT_EXECUTED, 0); //腳本已執行
                            script_result[0] = (1 << 4) + (script_cmd_idx & 0xf);
                            break;
                        }
                    }
                    else
                        break;
                }
                else
                    break;
            }
                            

            if(tagscripts == 0)
            {
                script_result[0] = (2 << 4) + 0;
            }
            else
                 PBOC_SetTVR(TVR_SCRIPT_FAIL_AFTER_LAST_AC, 0); //腳本失敗 after generate AC                

            PBOC_SetTSI(TSI_SET_SCRIPT_EXECUTED, 0); //腳本已處理, 不是執行
            

            PBOC_AddTermGenData(pboc_term, script_result, 5, TTAG_SCRIPT_RESULT, 2);            
        }
    }    
    
    
    return 0;
ac2_fail:
    free(ac_input);
    return 1;
}

void PBOC_SetTVR(uint8_t op, uint8_t *tvr)
{
    static uint8_t TVR[5] = {0, 0, 0, 0, 0};
    switch(op)
    {
        case TVR_RESET:
            memset(TVR, 0, 5);
            break;
        case TVR_GET:
            memcpy(tvr, TVR, 5);
            break;
        case TVR_SET_NO_DA:
            TVR[0] |= TVR0_NO_DA; //no data authentication
            break;
        case TVR_SET_FAIL_SDA:
            TVR[0] |= TVR0_SDA_FAIL; //data auth fail;
            break;
        case TVR_SET_SDA_SELECTED:
            TVR[0] |= TVR0_SDA_SELECTED;
            break;
        case TVR_SET_APPVER_NOMATCH:
            TVR[1] |= TVR1_APPVER_NOTMATCH; //app version not match
            break;
        case TVR_SET_SERVICE_NOT_SUPPORT:
            TVR[1] |= TVR1_SERVICE_NOT_ALLOW;
            break;
        case TVR_SET_APP_NOT_BORN:
            TVR[1] |= TVR1_APP_NOT_BORN;
            break;
        case TVR_SET_APP_EXPIRE:
            TVR[1] |= TVR1_APP_EXPIRE;
            break;
        case TVR_SET_CVM_FAIL:
            TVR[2] |= TVR2_CVM_FAIL;
            break;
        case TVR_SET_UNKNOWN_CVM:
            TVR[2] |= TVR2_UNKNOWN_CVM;
            break;
        case TVR_FORCE_CONNECTED:
            TVR[3] |= TVR3_FORCE_CONNECTED;
            break;
        case TVR_EXCEED_FLOOR_LIMIT:
            TVR[3] |= TVR3_EXCEED_FLOOR_LIMIT;
            break;
        case TVR_EXCEED_LOWER_TIME:
            TVR[3] |= TVR3_EXCEED_LOWER_TIME;
            break;
        case TVR_EXCEED_UPPER_TIME:
            TVR[3] |= TVR3_EXCEED_UPPER_TIME;
            break;
        case TVR_NEW_CARD:
            TVR[1] |= TVR1_NEW_CARD;
            break;
        case TVR_IC_DATA_MISS:
            TVR[0] |= TVR0_IC_DATA_MISS;
            break;     
        case TVR_KB_FAIL:
            TVR[2] |= TVR2_KB_FAIL;
            break;
        case TVR_ISSUER_AUTH_FAIL:
            TVR[4] |= TVR4_ISSUER_AUTH_FAIL;
            break;
        case TVR_CONNECT_PIN_INPUTED:
            TVR[2] |= TVR2_CONNECT_PIN_INPUTED;
            break;
        case TVR_SCRIPT_FAIL_BEFORE_LAST_AC:
            TVR[4] |= TVR4_SCRIPT_FAIL_BEFORE_LAST_AC;
            break;
        case TVR_SCRIPT_FAIL_AFTER_LAST_AC:
            TVR[4] |= TVR4_SCRIPT_FAIL_AFTER_LAST_AC;
            break;
        case TVR_SET_DEFAULT_TDOL:
            TVR[4] |= TVR4_USE_DEFAUL_TDOL;
            break;

            
    }
}


void PBOC_SetTSI(uint8_t op, uint8_t *tsi)
{
    static uint8_t TSI[2] = {0, 0};
    switch(op)
    {
        case TSI_RESET:
            memset(TSI, 0, 2);
            break;
        case TSI_GET:
            memcpy(tsi, TSI, 2);
            break;
        case TSI_SET_DA_EXECUTED:
            TSI[0] |= 0x80;
            break;
        case TSI_SET_CARD_BEHAVIOR_ANA_EXECUTED:
            TSI[0] |= 0x20;
            break;
        case TSI_SET_CVM_EXECUTED:
            TSI[0] |= 0x40;
            break;
        case TSI_SET_IA_EXECUTED:
            TSI[0] |= 0x10;
            break;
        case TSI_SET_TRM_EXECUTED: //terminal risk managerment
            TSI[0] |= 0x08;
            break;
        case TSI_SET_SCRIPT_EXECUTED:
            TSI[0] |= 0x04;
            break;
    }
}

void PBOC_AutoTermAppVer(uint8_t* aid, uint8_t *app_ver)
{
//——'0096'  A000000003 
//——'0001'  A000000025 
//——'0002'  A000000004 
//——'0200'  A000000065 
//——'0030'  A000000333 
//——'0001'  A000000152 
//——'0009'  A000000099 
//——'9999'  A000009999 

    uint16_t aid_compress;
    aid_compress = (aid[4] << 8) + (aid[5]);

    app_ver[0] = 0x02;
    app_ver[1] = 0x00;
    switch(aid_compress)
    {
        case 0x0003:
            app_ver[2] = 0x96;
            break;
        case 0x0025:
            app_ver[2] = 0x01;
            break;
        case 0x0004:
            app_ver[2] = 0x02;
            break;
        case 0x0065:
            app_ver[1] = 0x02;
            app_ver[2] = 0x00;
            break;
        case 0x0333:
            app_ver[2] = 0x30;
            break;
        case 0x0152:
            app_ver[2] = 0x01;
            break;
        case 0x0099:
            app_ver[2] = 0x09;
            break;
        case 0x9999:
            app_ver[1] = 0x99;
            app_ver[2] = 0x99;
            break;
        case 0x4455:
            app_ver[1] = 0x12;
            app_ver[2] = 0x34;
            break;
    }        
}

uint8_t *PBOC_GetAID(uint8_t *aid/*lv or idx*/,uint32_t tag)
{
    if(tag == TTAG_APP_VER)
        return 0;

    return MPosGetAIDHook(aid, tag);

}

#ifndef WIN32
const uint8_t ca_storage[0xC6] = 
{0xf0, 0xC4,//xxx
0x9F, 0x06, 0x05, 0xA0, 0x00, 0x00, 0x03, 0x33,
0x9f, 0x22, 0x01, 0x03, 

0xDF, 0x02, 0x81, 0xB0, 0xB0, 0x62, 0x7D, 0xEE, 0x87, 0x86, 0x4F, 0x9C, 0x18, 0xC1, 0x3B, 0x9A, 0x1F, 0x02, 0x54, 0x48, 0xBF, 0x13, 
0xC5, 0x83, 0x80, 0xC9, 0x1F, 0x4C, 0xEB, 0xA9, 0xF9, 0xBC, 0xB2, 0x14, 0xFF, 0x84, 0x14, 0xE9, 0xB5, 0x9D, 0x6A, 0xBA, 0x10, 
0xF9, 0x41, 0xC7, 0x33, 0x17, 0x68, 0xF4, 0x7B, 0x21, 0x27, 0x90, 0x7D, 0x85, 0x7F, 0xA3, 0x9A, 0xAF, 0x8C, 0xE0, 0x20, 0x45, 
0xDD, 0x01, 0x61, 0x9D, 0x68, 0x9E, 0xE7, 0x31, 0xC5, 0x51, 0x15, 0x9B, 0xE7, 0xEB, 0x2D, 0x51, 0xA3, 0x72, 0xFF, 0x56, 0xB5, 
0x56, 0xE5, 0xCB, 0x2F, 0xDE, 0x36, 0xE2, 0x30, 0x73, 0xA4, 0x4C, 0xA2, 0x15, 0xD6, 0xC2, 0x6C, 0xA6, 0x88, 0x47, 0xB3, 0x88, 
0xE3, 0x95, 0x20, 0xE0, 0x02, 0x6E, 0x62, 0x29, 0x4B, 0x55, 0x7D, 0x64, 0x70, 0x44, 0x0C, 0xA0, 0xAE, 0xFC, 0x94, 0x38, 0xC9, 
0x23, 0xAE, 0xC9, 0xB2, 0x09, 0x8D, 0x6D, 0x3A, 0x1A, 0xF5, 0xE8, 0xB1, 0xDE, 0x36, 0xF4, 0xB5, 0x30, 0x40, 0x10, 0x9D, 0x89, 
0xB7, 0x7C, 0xAF, 0xAF, 0x70, 0xC2, 0x6C, 0x60, 0x1A, 0xBD, 0xF5, 0x9E, 0xEC, 0x0F, 0xDC, 0x8A, 0x99, 0x08, 0x91, 0x40, 0xCD, 
0x2E, 0x81, 0x7E, 0x33, 0x51, 0x75, 0xB0, 0x3B, 0x7A, 0xA3, 0x3D,

0xDF, 0x04, 0x01, 0x03
};


#endif


uint8_t PBOC_GetCAPKI(uint8_t *rid, uint8_t idx, uint32_t tag, uint8_t **tag_lv)
{    
    return MPosGetCAPKIHook(rid, idx,  tag, tag_lv);            
}


void PBOC_InitTermData(PBOC_TERM_DATA *pboc_term)
{
    pboc_term->fix_ltlv = 0;
    pboc_term->generate_ltlv = 0;
    pboc_term->transaction_ltlv = 0;
}

void PBOC_DeInitTermData(PBOC_TERM_DATA *pboc_term)
{
  //  if(pboc_term->fix_ltlv)
  //  {
  //      free(pboc_term->fix_ltlv);
  //      pboc_term->fix_ltlv = 0;
  //  }
    
    if(pboc_term->generate_ltlv)
    {
        free(pboc_term->generate_ltlv);
        pboc_term->generate_ltlv = 0;
    }
    
    if(pboc_term->transaction_ltlv)
    {
        free(pboc_term->transaction_ltlv);
        pboc_term->transaction_ltlv = 0;
    }

    pboc_term->date[0] = 0;
    pboc_term->time[0] = 0;
    
    memset(&pboc_term->unpredict[0], 0, 4);    
}

void PBOC_DeleteTermGenData(PBOC_TERM_DATA *pboc_term)
{
    if(pboc_term->generate_ltlv)
    {
        free(pboc_term->generate_ltlv);
        pboc_term->generate_ltlv = 0;
    }
}

uint8_t PBOC_GetAidCandidate(PBOC_CARD_FILES *pboc_files, uint8_t min_pri, uint8_t **aid)
{
    uint8_t totallen;
    uint8_t *ptr;
    uint8_t aidlen;

    uint8_t cur_min_pri;
    uint8_t *cur_min_pri_ptr;
    uint8_t cur_min_pri_len;

    ptr = pboc_files->candidate_aid;
    if(!ptr)
        return 0;

    cur_min_pri = 16;
    
    totallen = ptr[0];

    ptr++;

    do
    {
        aidlen = ptr[0];
        ptr++;
        
        if(ptr[aidlen-1] == (min_pri))
        {
            *aid = ptr;
            return aidlen - 1/*pri*/;
        }
        else if(ptr[aidlen-1] >= min_pri)
        {
            if(ptr[aidlen-1] < cur_min_pri) //save miniman
            {
                cur_min_pri = ptr[aidlen-1];
                cur_min_pri_ptr = ptr;
                cur_min_pri_len = aidlen-1; 
            }
        }


        ptr+= aidlen;
        totallen -= (aidlen + 1);
        
    }            
    while(totallen);

    if(cur_min_pri != 16)
    {
        *aid = cur_min_pri_ptr;
        return cur_min_pri_len;
    }

    return 0;
}

void PBOC_ClearAidCandidate(PBOC_CARD_FILES *pboc_files)
{
    if(pboc_files->candidate_aid)
    {
        free(pboc_files->candidate_aid);
        pboc_files->candidate_aid = 0;
    }
}

void PBOC_SetAidCandidate(PBOC_CARD_FILES *pboc_files, uint8_t *found_aid, uint8_t found_aid_len, uint8_t priority)
{
    uint8_t *alloc;
    uint8_t *tail;

    if(!pboc_files->candidate_aid)
    {
        alloc = (uint8_t*)malloc(found_aid_len+2/*one lv*/+1/*total_len*/);
        alloc[0] = 0;
    }
    else
    {
        uint8_t *tmp;

        alloc = (uint8_t*)malloc(pboc_files->candidate_aid[0]/*all lv*/ + 1/*total len*/ +  found_aid_len+2/*one lv*/);

        memcpy(alloc, pboc_files->candidate_aid, pboc_files->candidate_aid[0] + 1);

        free(pboc_files->candidate_aid);
    }

    tail = &alloc[alloc[0]+1];
    *tail++ = found_aid_len + 1/*pri*/;
    memcpy(tail, found_aid, found_aid_len);
    tail+=found_aid_len;
    *tail = priority;
    alloc[0] += ( found_aid_len+2);
    pboc_files->candidate_aid = alloc;
}


void PBOC_AddTermGenData(PBOC_TERM_DATA *pboc_term, uint8_t *dat, uint8_t len, uint32_t tag, uint8_t taglen)
{
    uint8_t *alloc;
    uint8_t *tail;
    if(!pboc_term->generate_ltlv)
    {
        alloc = (uint8_t*) malloc(taglen + len + 1 + 1);
        alloc[0] = 0;
    }
    else
    {
        alloc = (uint8_t*) malloc(pboc_term->generate_ltlv[0] + 1 + taglen + len + 1 + 1);
        
        memcpy(alloc, pboc_term->generate_ltlv, pboc_term->generate_ltlv[0] + 1);
        free(pboc_term->generate_ltlv);
    }

    tail = &alloc[alloc[0]+1];

    //next tag
    if(taglen == 2)
        *tail++ = (tag >> 8) & 0xff;
    *tail++  = tag & 0xff;
    *tail++ = len;
    memcpy(tail, dat, len);
    alloc[0] += taglen + 1 + len;
    pboc_term->generate_ltlv = alloc;
}

void PBOC_FreeTermFixData(PBOC_TERM_DATA *pboc_term)
{
    if(pboc_term->fix_ltlv)
    {
        free(pboc_term->fix_ltlv);
        pboc_term->fix_ltlv = 0;
    }
}

void PBOC_AddTermFixData(PBOC_TERM_DATA *pboc_term, uint8_t *dat, uint8_t len, uint32_t tag, uint8_t taglen)
{
    uint8_t *alloc;
    uint8_t *tail;
    if(!pboc_term->fix_ltlv)
    {
        alloc = (uint8_t*) malloc(taglen + len + 1 + 1);
        alloc[0] = 0;
    }
    else
    {
        alloc = (uint8_t*) malloc(pboc_term->fix_ltlv[0] + 1 + taglen + len + 1 + 1);
        
        memcpy(alloc, pboc_term->fix_ltlv, pboc_term->fix_ltlv[0] + 1);
        free(pboc_term->fix_ltlv);
    }

    tail = &alloc[alloc[0]+1];

    //next tag
    if(taglen == 2)
        *tail++ = (tag >> 8) & 0xff;
    *tail++  = tag & 0xff;
    *tail++ = len;
    memcpy(tail, dat, len);
    alloc[0] += taglen + 1 + len;
    pboc_term->fix_ltlv = alloc;
}


void PBOC_SetTermTransactionData(PBOC_TERM_DATA *pboc_term, uint8_t *tlv, uint32_t tlvlen)
{
    uint8_t *alloc;
    uint8_t *tail;
    if(!pboc_term->transaction_ltlv)
    {
        alloc = (uint8_t*) malloc(tlvlen + 1);
        alloc[0] = 0;
    }
    else
    {
        alloc = (uint8_t*) malloc(tlvlen + pboc_term->transaction_ltlv[0] + 1);
        memcpy(alloc, pboc_term->transaction_ltlv, pboc_term->transaction_ltlv[0] + 1);
        free(pboc_term->transaction_ltlv);
    }

    tail = &alloc[alloc[0] + 1];
    memcpy(tail, tlv, tlvlen);    
    alloc[0] += tlvlen;
    pboc_term->transaction_ltlv = alloc;
}

uint8_t *PBOC_GetTermData(PBOC_TERM_DATA *pboc_term, uint32_t tag)
{
    uint8_t *ret;
    if(tag == TTAG_UNPREDICT ||
        tag == TTAG_TVR ||
        tag == TTAG_TSI
    )
    {//changelable
        if(tag == TTAG_TVR)
        {
            PBOC_SetTVR(TVR_GET, pboc_term->changeable_lv + 1);
            pboc_term->changeable_lv[0] = 5;
        }
        else if(tag == TTAG_TSI)
        {
            PBOC_SetTSI(TSI_GET, pboc_term->changeable_lv + 1);
            pboc_term->changeable_lv[0] = 2;
        }
        else if(tag == TTAG_UNPREDICT)
        {
            /*
            if(pboc_term->generate_ltlv)
            {
                if(!PBOC_TLVFindTag(pboc_term->generate_ltlv+1, pboc_term->generate_ltlv[0], TTAG_UNPREDICT, &ret))
                {
                    return ret;
                }
            }
            */
            if(!memcmp(&pboc_term->unpredict[0], "\x00\x00\x00\x00", 4))
                GetRandom(&pboc_term->unpredict[0]);
            
            memcpy(pboc_term->changeable_lv + 1, &pboc_term->unpredict[0], 4);
            pboc_term->changeable_lv[0] = 4;

            //PBOC_AddTermGenData(pboc_term, pboc_term->changeable_lv + 1, 4,  TTAG_UNPREDICT,  2);//save it
        }

        return pboc_term->changeable_lv;
    
    }
    else if(tag == TTAG_DATE || tag == TTAG_TIME)
    {//need to keep the date and time same
        uint8_t tmp[15];
        extern void GetTime(uint8_t time_str[15]);

        if(tag == TTAG_DATE)
        {
            if(pboc_term->date[0] == 3)
            {
                return &pboc_term->date[0];
            }
        }
        else if(tag == TTAG_TIME)
        {
            if(pboc_term->time[0] == 3)
            {
                return &pboc_term->time[0];
            }
        }
        

        GetTime(tmp);

        //toBCD
        if(tag == TTAG_DATE)
        {
            pboc_term->date[0] = 3;
            pboc_term->date[1] = ((tmp[2] - '0') << 4) + (tmp[3] - '0');
            pboc_term->date[2] = ((tmp[4] - '0') << 4) + (tmp[5] - '0');
            pboc_term->date[3] = ((tmp[6] - '0') << 4) + (tmp[7] - '0');
            return &pboc_term->date[0];
        }
        else
        {
            pboc_term->time[0] = 3;
            pboc_term->time[1] = ((tmp[8] - '0') << 4) + (tmp[9] - '0');
            pboc_term->time[2] = ((tmp[10] - '0') << 4) + (tmp[11] - '0');
            pboc_term->time[3] = ((tmp[12] - '0') << 4) + (tmp[13] - '0');
            return &pboc_term->time[0];
        }

    }
    else if(tag == TTAG_IAD) //device specific
    {
        uint32_t id, i;
        extern uint32_t StorageGetID(void); //HAL function

        id = StorageGetID();
        pboc_term->changeable_lv[0] = 8;

        pboc_term->changeable_lv[1] = 0x30;
        for(i = 0; i < 8; i++)
        {
            uint8_t tmp;
            tmp = ((id >> (28 - (i<<2))) & 0xf);

            pboc_term->changeable_lv[1+i] = (tmp % 10) + '0';
            
            //sprintf(&pboc_term->changeable_lv[1], "%s%2X", &pboc_term->changeable_lv[i], ((id >> (28 - (i<<2))) & 0xf));
        }
        return &pboc_term->changeable_lv[0];
    }
    else
    {//這三個順序不能掉換
        if(pboc_term->fix_ltlv)
            if(!PBOC_TLVFindTag(pboc_term->fix_ltlv+1, pboc_term->fix_ltlv[0], tag, &ret))
            {
                return ret;
            }

        if(pboc_term->transaction_ltlv)
            if(!PBOC_TLVFindTag(pboc_term->transaction_ltlv+1, pboc_term->transaction_ltlv[0], tag, &ret))
            {
                return ret;
            }

        if(pboc_term->generate_ltlv)
            if(!PBOC_TLVFindTag(pboc_term->generate_ltlv+1, pboc_term->generate_ltlv[0], tag, &ret))
            {
                return ret;
            }

    }

    return 0;

}

void PBOC_InitCardData(PBOC_CARD_FILES *pboc_files)
{
    pboc_files->files = 0;
    pboc_files->sda_list = 0;
    pboc_files->tag_num = 0;
}

void PBOC_DeInitCardData(PBOC_CARD_FILES *pboc_files)
{

    if(pboc_files->files)
    {
        free(pboc_files->files);
        pboc_files->files = 0;
    }

    if(pboc_files->sda_list)
    {
        free(pboc_files->sda_list);
        pboc_files->sda_list = 0;
    }

    while(pboc_files->tag_num)
    {
        free(pboc_files->tag_data[pboc_files->tag_num-1]);
        pboc_files->tag_num--;
    }

}


uint8_t *PBOC_GetFileData(PBOC_CARD_FILES *pboc_files, uint32_t tag)
{
    uint8_t i;
    
    for(i = 0; i < pboc_files->tag_num; i++)
    {
        if(tag == (pboc_files->tag_data[i][0] << 8) + pboc_files->tag_data[i][1])
        {
            return &pboc_files->tag_data[i][2]; //lv
        }
    }

    return 0;
}

void PBOC_SetFileDataLVSep(PBOC_CARD_FILES *pboc_files, uint32_t tag, uint8_t tagl, uint8_t *tagv)
{
    uint8_t k;
    if(pboc_files->tag_num >= MAX_CTAG)
        return;
    pboc_files->tag_data[pboc_files->tag_num] = (uint8_t*)malloc(tagl+3/*data+tag(2)+len(1)*/);

    if(pboc_files->tag_data[pboc_files->tag_num])
    {
        pboc_files->tag_data[pboc_files->tag_num][0] = tag >> 8;
        pboc_files->tag_data[pboc_files->tag_num][1] = tag & 0xff;
        pboc_files->tag_data[pboc_files->tag_num][2] = tagl;
        memcpy(&pboc_files->tag_data[pboc_files->tag_num][3], tagv, tagl);

        pboc_files->tag_num++;
        TRACE("[0x%x]:", tag);
        for(k = 0; k < tagl; k++)
            TRACE("%02X", tagv[k]);
        TRACE("\n");

    }
}

void PBOC_SetFileData(PBOC_CARD_FILES *pboc_files, uint32_t tag, uint8_t *taglv)
{
#if 0
    uint8_t k;
    if(pboc_files->tag_num >= MAX_CTAG)
        return;
    pboc_files->tag_data[pboc_files->tag_num] = (uint8_t*)malloc(taglv[0]+3/*data+tag(2)+len(1)*/);

    if(pboc_files->tag_data[pboc_files->tag_num])
    {
        pboc_files->tag_data[pboc_files->tag_num][0] = tag >> 8;
        pboc_files->tag_data[pboc_files->tag_num][1] = tag & 0xff;
        pboc_files->tag_data[pboc_files->tag_num][2] = taglv[0];
        memcpy(&pboc_files->tag_data[pboc_files->tag_num][3], &taglv[1], taglv[0]);

        pboc_files->tag_num++;
        TRACE("[0x%x]:", tag);
        for(k = 0; k < taglv[0]; k++)
            TRACE("%02X", taglv[k+1]);
        TRACE("\n");

    }
#else
    PBOC_SetFileDataLVSep(pboc_files, tag, taglv[0], taglv+1);
#endif

}

uint8_t PBOC_TLVFindTag(uint8_t *tlv, uint32_t tlvlen, uint32_t tag_wanted, uint8_t **ret) //0 found, 1: not found, 2: error
{
    uint8_t *tlv0;
    uint32_t tlvlen0;

    tlv0 = tlv;
    tlvlen0 = tlvlen;
    while(1)
    {
        uint32_t tag;
        tag = PBOC_TLVEnum(&tlv0, &tlvlen0, ret);
        if(!tag)
            break;
        else if(tag == tag_wanted)
        {
            return 0;
        }

    }
    *ret = 0;
    return 1;
}

void PBOC_BuildSDAList(PBOC_CARD_FILES *pboc_files)
{
    uint8_t i, j, k;

    if(pboc_files->sda_listlen != 0xffff)
        return;

    pboc_files->sda_listlen = 0;
    for(i = 0; i < pboc_files->file_num ; i++)
    {
        RESAPD resApdu;
        uint8_t SDA_Count;

        resApdu.data = pboc_buf;

        SDA_Count = pboc_files->files[i].sfi_sdanum;

        for(j = pboc_files->files[i].record_firat; j <= pboc_files->files[i].record_last; j++)
        {
            uint8_t *tlv;
            uint32_t tlvlen;
            uint8_t *tagdata;
            uint32_t tag;
            uint8_t ret;
            
            if(SDA_Count)
            {

                TRACE("\nRead SFI:%d RECORD:%d\n", pboc_files->files[i].sfi, j);

                ret = PBOC_ReadRecord(j, pboc_files->files[i].sfi, &tlv, &tlvlen);
                if(!ret)
                {
                    if(SDA_Count)
                    {
                        uint8_t *new_alloc;

                        new_alloc = (uint8_t*) malloc(pboc_files->sda_listlen + tlvlen + 3);

                        if(pboc_files->sda_list) //move old to new
                            memcpy(new_alloc, pboc_files->sda_list, pboc_files->sda_listlen);

                        // range 11 to 30, we should add TAG, EMV book3 10.3
                        if(pboc_files->files[i].sfi > 10)
                        {
                            new_alloc[pboc_files->sda_listlen++] = 0x70;
                            if(tlv[-2] == 0x81) //2 bytes length
                                new_alloc[pboc_files->sda_listlen++] = tlv[-2]; //workaround for 2CS.015.01     
                            new_alloc[pboc_files->sda_listlen++] = tlvlen;     
                            PBOC_SetTVR(TVR_SET_SDA_SELECTED, 0);//2CJ.018~020
                        }

                        memcpy(new_alloc + pboc_files->sda_listlen, tlv, tlvlen); //add new
                        pboc_files->sda_listlen += tlvlen;

                        if(pboc_files->sda_list) //free old
                            free(pboc_files->sda_list);

                        pboc_files->sda_list = new_alloc;

                        SDA_Count--;
                        if(!SDA_Count)
                            break; //next file
                    }      
                        
                }
                else if(ret == 3)
                {
                    PBOC_SetTVR(TVR_SET_SDA_SELECTED, 0);//2CJ.018~020
                }
                else
                    return;
        }

        }
    }
}


uint32_t PBOC_TLEnum(uint8_t **tl, uint32_t *tllen, uint32_t *len) //0 not found, else tag
{
    uint32_t tag;
    uint32_t taglen;
    uint8_t *ptr;

    tag = 0;

    if(*tllen == 0)
        return 0;

    //2CJ.025.03 2CL.054.00
    while(**tl == 0xff || **tl==0x00) //1 bytes
    {
        *tl += 1;
        *tllen -= 1;

        if(*tllen == 0)
            return 0;

    }

    if(*tllen < 2)
        return 0;

    if(!((**tl & 0x1f) == 0x1f)) //1 bytes
    {
        tag = **tl;
        ptr = &(*tl)[1];

        *tllen -= 1;
        *tl += 1;

    }
    else // 2 bytes
    {
        tag = ((*tl)[0] << 8) + (*tl)[1];
        ptr = &(*tl)[2];

        *tllen -= 2;
        *tl += 2;

    }

    if(*ptr & 0x80) //length > 1 byte
    {
        if(*ptr != 0x81)
        {
            return 0;
        }

        taglen = ptr[1];
        *tllen -= 2;
        *tl += 1;

        ptr += 1;
    }
    else
    {
        taglen = ptr[0];
        *tllen -= 1;
        *tl += 1;
    }


    *len = taglen;
    return tag;

}

void PBOC_AssmTLV(uint32_t tag, uint8_t taglen, uint16_t dlen, uint8_t *dat, uint8_t tlv[1024], uint32_t *tlvidx)
{
    
    //T
    if(taglen == 2)
    {
        tlv[(*tlvidx)++] = (tag >> 8) & 0xff;
        tlv[(*tlvidx)++] = tag & 0xff;
    }
    else
        tlv[(*tlvidx)++] = tag & 0xff;

    //L    
    if(dlen > 0x80)
    {
        tlv[(*tlvidx)++] = 0x81;
    }

    tlv[(*tlvidx)++] = dlen;

    //V, haha
    memcpy(tlv+*tlvidx, dat, dlen);
    (*tlvidx) += dlen;

}

uint32_t PBOC_TLVSearch61(uint8_t **tlv, uint32_t *tlvlen, uint8_t **lv) //0 not found, else tag
{
    uint8_t *ptr;
    uint32_t taglen;


    if(*tlvlen < 2)
        return 0;

    while((*tlv)[0] != 0x61)
    {
       *tlv += 1;
       *tlvlen -= 1;

       if(*tlvlen == 0)
            return 0;
    }

    ptr = &(*tlv)[1];

    *tlvlen -= 1;
    *tlv += 1;

    if(*ptr & 0x80) //length > 1 byte
    {
        if(*ptr != 0x81)
        {
            return 0;
        }

        taglen = ptr[1];
        *tlvlen -= 2;
        *tlv += 2;

        ptr += 1;
    }
    else
    {
        taglen = ptr[0];
        *tlvlen -= 1;
        *tlv += 1;
    }


    *tlvlen -= taglen;
    *tlv += taglen;

    *lv = ptr;

    return 0x61;
}


uint32_t PBOC_TLVEnum(uint8_t **tlv, uint32_t *tlvlen, uint8_t **ret) //0 not found, else tag
{
    uint32_t tag;
    uint32_t taglen;
    uint8_t *ptr;

    if(*tlvlen == 0)
        return 0;


    //2CJ.025.03, 2CL.054.00
    while(**tlv == 0xff || **tlv == 0x00) //bypass PADDING 0xff or PADING 0x00
    {
        *tlv += 1;
        *tlvlen -= 1;

        if(*tlvlen == 0)
            return 0;
    }

    if(*tlvlen < 2)
        return 0;


    if(!((**tlv & 0x1f) == 0x1f)) //1 bytes
    {
        tag = **tlv;
        ptr = &(*tlv)[1];

        *tlvlen -= 1;
        *tlv += 1;

    }
    else // 2 bytes
    {
        tag = ((*tlv)[0] << 8) + (*tlv)[1];
        ptr = &(*tlv)[2];

        *tlvlen -= 2;
        *tlv += 2;
    }

    if(*ptr & 0x80) //length > 1 byte
    {
        if(*ptr != 0x81)
        {
            return 0;
        }

        taglen = ptr[1];
        *tlvlen -= 2;
        *tlv += 2;

        ptr += 1;
    }
    else
    {
        taglen = ptr[0];
        *tlvlen -= 1;
        *tlv += 1;
    }


    *tlvlen -= taglen;
    *tlv += taglen;

    *ret = ptr;
    return tag;
}



//////////////////////////////////////////////////////////////////////////////////////
uint8_t PBOCFF_End(PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term)
{
    PBOC_DeInitCardData(pboc_files);
    PBOC_DeInitTermData(pboc_term);

    return 0;
}

uint8_t PBOCFF_2ndAuth(PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term, uint8_t *transaction, uint32_t transaction_len, void (*result_callback)(uint8_t* resp, uint32_t resp_len), void (*flow_ind)(uint8_t))
{
    uint8_t *resp = 0;
    uint32_t respidx;
    uint8_t *ptr;
    uint8_t *iarc;
    uint8_t DF_75 = 0xFF;
    uint8_t *tlv, *lv;
    uint32_t tlvlen, tag, cnt;
    uint8_t ac_p2;

    //from issuer input
    PBOC_SetTermTransactionData(pboc_term, transaction, transaction_len);

    ac_p2 = 0;
    if(PBOCF_ExtAuth_2ndAC(pboc_term, pboc_files, &ac_p2))
        goto exec_2nd_fail;


    memset(&pboc_term->unpredict[0], 0, 4);
        
    resp = (uint8_t*) malloc(256);
    respidx = 2;

#ifdef PBOC_EXTENDED_STORAGE
    resp[respidx++] = TTAG_TSI;
    resp[respidx++] = 0x02;
    PBOC_SetTSI(TSI_GET, resp+respidx);
    respidx+=2;
#endif

    //0x9f26
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_AC))
        PBOC_AssmTLV(CTAG_AC, 2, ptr[0], ptr+1, resp, &respidx);

    //0x9f36
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_ATC))
        PBOC_AssmTLV(CTAG_ATC, 2, ptr[0], ptr+1, resp, &respidx);

    //0x9f37
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_UNPREDICT))
        PBOC_AssmTLV(TTAG_UNPREDICT, 2, ptr[0], ptr+1, resp, &respidx);

    //0x95
    resp[respidx++] = TTAG_TVR;
    resp[respidx++] = 0x05;
    PBOC_SetTVR(TVR_GET, resp+respidx);
    respidx+=5;

    //0x9a
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_DATE))
        PBOC_AssmTLV(TTAG_DATE, 1, ptr[0], ptr+1, resp, &respidx);

    //0x9C
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_TRANACTION_TYPE))
        PBOC_AssmTLV(TTAG_TRANACTION_TYPE, 1, ptr[0], ptr+1, resp, &respidx);

    //0xDF31, we have many, so...see!!!
    if(pboc_term->generate_ltlv)
    {
        cnt = 0;
        tlv = pboc_term->generate_ltlv+1;
        tlvlen = pboc_term->generate_ltlv[0];
        while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv)))
        {
            if(tag == TTAG_SCRIPT_RESULT)
                cnt++;
        }
        
        if(cnt)
        {
            uint8_t *result;
            result = (uint8_t *)malloc(5*cnt);

            cnt = 0;
            tlv = pboc_term->generate_ltlv+1;
            tlvlen = pboc_term->generate_ltlv[0];
            while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv)))
            {
                if(tag == TTAG_SCRIPT_RESULT)
                {
                    memcpy(&result[5*cnt], lv+1, 5);
                    cnt++;
                }
            }


            PBOC_AssmTLV(TTAG_SCRIPT_RESULT, 2, 5*cnt, result, resp, &respidx);

            free(result);
        }
    }

    
    //0x9f27 for DF75
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_CID))
    {
        if((ptr[1] & CID_MASK) == CID_AAC || (ptr[1] & CID_MASK) == CID_ARQC)
        {/*2CK0150002 AAC->ARQC*/ /*2CK0180002 AAC->ARQC*/ /*2CK0180003 TC->ARQC*/
            DF_75 = 0x04; //Generate 返回 AAC, DECLINE
        }
        else if((ptr[1] & CID_MASK) == CID_TC)
        {
            if(ac_p2 == CID_TC)
                DF_75 = 0x01; //交易收受
            else 
            {
                DF_75 = 0x04; //DECLINE/*2CK0150003, 2CK0180001(AAC->TC)
            }

        }        

        if((ptr[1] & REASON_MASK) == REASON_SERVICE_NOT_ALLOW)
            DF_75 = 0x02; //交意拒絕, TERMINATE, NOT ACCEPT

//        if((ptr[1] & ADVICE_MASK))
  //          DF_75 = 0x02;
        
    }

exec_end_2nd:

    if(!resp)
    {
        resp = (uint8_t*) malloc(6);
        respidx = 2;
    }

    //0xDF75
    resp[respidx++] = 0xDF;
    resp[respidx++] = 0x75;
    resp[respidx++] = 0x01;
    resp[respidx++] = DF_75; //success

    Hex2BCD(respidx-2, resp); 

    result_callback(resp, respidx);

    free(resp);

    return 0;

exec_2nd_fail:
    goto exec_end_2nd;

    
}

uint8_t PBOCFF_Execute(PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term, uint8_t *transaction, uint32_t transaction_len, void (*result_callback)(uint8_t* resp, uint32_t resp_len), void (*flow_ind)(uint8_t))
{
    uint8_t *pdol;
    uint8_t ac_p1;
    uint8_t DF_75 = 0xFE;
    uint8_t *resp = 0;
    uint32_t respidx;
    uint8_t *ptr;
    
    /*PBOC core*/
    PBOC_DeInitTermData(pboc_term);

    //from user input
    PBOC_SetTermTransactionData(pboc_term, transaction, transaction_len);

    //clear TVR, TSI
    PBOC_SetTVR(TVR_RESET, 0);
    PBOC_SetTSI(TSI_RESET, 0);

    if(flow_ind) flow_ind(0);

    if(PBOCF_SelectApp(&pdol, pboc_files, pboc_term))
    {
        free(pdol);
        goto execute_err;
    }

    if(flow_ind) flow_ind(1);
    
    if(PBOCF_InitApp(pdol, pboc_files, pboc_term))
    {
        goto execute_err;
    }

    if(flow_ind) flow_ind(2);

    if(PBOCF_ReadAppData(pboc_files, flow_ind))
        goto execute_err;

    if(flow_ind) flow_ind(3);

    if(PBOCF_DataAuth(pboc_term, pboc_files))
        goto execute_err;

    if(flow_ind) flow_ind(4);

    if(PBOCF_ProcessRestriction(pboc_term, pboc_files))
        goto execute_err;

    if(flow_ind) flow_ind(5);

    if(PBOCF_CVM(pboc_term, pboc_files))
        goto execute_err;

    if(flow_ind) flow_ind(6);

    if(PBOCF_TermRiskManager(pboc_term, pboc_files))
        goto execute_err;

    if(flow_ind) flow_ind(7);

    if(PBOCF_TermActionAna(pboc_term, pboc_files, &ac_p1))
        goto execute_err;

    if(flow_ind) flow_ind(8);

    if(PBOCF_CardActionAna(pboc_term, pboc_files, ac_p1))
        goto execute_err;
    
    resp = (uint8_t*) malloc(512);
    respidx = 2;


    //0x9f06
    PBOC_AssmTLV(TTAG_RID, 2, pboc_files->aid[0], &pboc_files->aid[1], resp, &respidx);

    //0x9f26
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_AC))
        PBOC_AssmTLV(CTAG_AC, 2, ptr[0], ptr+1, resp, &respidx);

    //0x9f27
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_CID))
    {
        PBOC_AssmTLV(CTAG_CID, 2, ptr[0], ptr+1, resp, &respidx);
        if((ptr[1] & CID_MASK) == CID_MASK)
            DF_75 = 0xFE; //TERMINATE
        else if((ptr[1] & CID_MASK) == CID_AAC)
        {
            DF_75 = 0x02; //交易拒絕, DECLINE
        }
        else if((ptr[1] & CID_MASK) == CID_TC)
        {
            if(ac_p1 == CID_TC)
                DF_75 = 0x01; //交易收受
            else //CID_AAC or CID_ARQC
                DF_75 = 0xFE; //TERMINATE, 2CK.017.00
        }
        else if((ptr[1] & CID_MASK) == CID_ARQC)
        {
            if(ac_p1 == CID_AAC)
                DF_75 = 0xFE; //TERMINATE, 2CK.017.00
            else
                DF_75 = 0x03; //聯機
                
        }

        
        if((ptr[1] & REASON_MASK) == REASON_SERVICE_NOT_ALLOW)
            DF_75 = 0xFF; //TERMINATE, NOT_ACCEPT

//        if((ptr[1] & ADVICE_MASK))
  //          DF_75 = 0x02;
        
    }

    //0x9f10
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_ISSUE_APDATA))
        PBOC_AssmTLV(CTAG_ISSUE_APDATA, 2, ptr[0], ptr+1, resp, &respidx);

    //0x9f37
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_UNPREDICT))
        PBOC_AssmTLV(TTAG_UNPREDICT, 2, ptr[0], ptr+1, resp, &respidx);

    //0x9f36
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_ATC))
        PBOC_AssmTLV(CTAG_ATC, 2, ptr[0], ptr+1, resp, &respidx);
    
    //0x95
    resp[respidx++] = TTAG_TVR;
    resp[respidx++] = 0x05;
    PBOC_SetTVR(TVR_GET, resp+respidx);
    respidx+=5;

    //0x9a
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_DATE))
        PBOC_AssmTLV(TTAG_DATE, 1, ptr[0], ptr+1, resp, &respidx);

    //0x9c
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_TRANACTION_TYPE))
        PBOC_AssmTLV(TTAG_TRANACTION_TYPE, 1, ptr[0], ptr+1, resp, &respidx);

    //0x9f02 
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_AUTH_QUAN))
        PBOC_AssmTLV(TTAG_AUTH_QUAN, 2, ptr[0], ptr+1, resp, &respidx);
    else
        PBOC_AssmTLV(TTAG_AUTH_QUAN, 2, 6, "\x00\x00\x00\x00\x00\x00", resp, &respidx);

    //0x5f2a 
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_MONEY_CONTRY))
        PBOC_AssmTLV(TTAG_MONEY_CONTRY, 2, ptr[0], ptr+1, resp, &respidx);
    
    //0x82
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_AIP))
        PBOC_AssmTLV(CTAG_AIP, 1, ptr[0], ptr+1, resp, &respidx);

    //0x9F1A
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_CONTRY))
        PBOC_AssmTLV(TTAG_CONTRY, 2, ptr[0], ptr+1, resp, &respidx);

    //0x9F03 
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_AUTH_OTHER_QUAN))
        PBOC_AssmTLV(TTAG_AUTH_OTHER_QUAN, 2, ptr[0], ptr+1, resp, &respidx);
    else
        PBOC_AssmTLV(TTAG_AUTH_OTHER_QUAN, 2, 6, "\x00\x00\x00\x00\x00\x00", resp, &respidx);
    
    //0x9F33 
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_TERMINAL_CAP))
        PBOC_AssmTLV(TTAG_TERMINAL_CAP, 2, ptr[0], ptr+1, resp, &respidx);

    //0x9F34 
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_CVM_RESULT))
        PBOC_AssmTLV(TTAG_CVM_RESULT, 2, ptr[0], ptr+1, resp, &respidx);   

    //0x9F35 
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_TERMINAL_TYPE))
        PBOC_AssmTLV(TTAG_TERMINAL_TYPE, 2, ptr[0], ptr+1, resp, &respidx);   

    //0x9F1E
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_IAD))
        PBOC_AssmTLV(TTAG_IAD, 2, ptr[0], ptr+1, resp, &respidx);
    
    //0x84
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_RID))
        PBOC_AssmTLV(FCI_DF_TAG, 1, ptr[0], ptr+1, resp, &respidx);

    //0x9f09
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_APP_VER))
        PBOC_AssmTLV(TTAG_APP_VER, 2, ptr[0], ptr+1, resp, &respidx);

    //0x9f41
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_TEST_SEQ_COUNTER))
        PBOC_AssmTLV(TTAG_TEST_SEQ_COUNTER, 2, ptr[0], ptr+1, resp, &respidx);
    else
        PBOC_AssmTLV(TTAG_TEST_SEQ_COUNTER, 2, 4, "\x00\x00\x00\x00", resp, &respidx);


    //0x9F63 
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_CARD_TYPE))
        PBOC_AssmTLV(CTAG_CARD_TYPE, 2, ptr[0], ptr+1, resp, &respidx);

    //0x5f34 
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_CARD_SERIAL))
        PBOC_AssmTLV(CTAG_CARD_SERIAL, 2, ptr[0], ptr+1, resp, &respidx);

    //0x5A
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_PAN))
        PBOC_AssmTLV(CTAG_PAN, 1, ptr[0], ptr+1, resp, &respidx);
    
    //0x57 
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_T2))
        PBOC_AssmTLV(CTAG_T2, 1, ptr[0], ptr+1, resp, &respidx);

    //0x5F24
    if(ptr = PBOC_GetFileData(pboc_files, CTAG_APPEXPIRE))
        PBOC_AssmTLV(CTAG_APPEXPIRE, 2, ptr[0], ptr+1, resp, &respidx);

    //0xDF37 
    if(ptr = PBOC_GetTermData(pboc_term, TTAG_HINT_PIN))
        PBOC_AssmTLV(TTAG_HINT_PIN, 2, ptr[0], ptr+1, resp, &respidx);   


execute_end:
    if(flow_ind) flow_ind(9);

    if(!resp)
    {
        resp = (uint8_t*) malloc(6);
        respidx = 2;
    }

    //0xDF75
    resp[respidx++] = 0xDF;
    resp[respidx++] = 0x75;
    resp[respidx++] = 0x01;
    resp[respidx++] = DF_75; //success

    Hex2BCD(respidx-2, resp); 

    PBOC_DeleteTermGenData(pboc_term);

    result_callback(resp, respidx);
    
    free(resp);
    return 0;

execute_err:
    PBOC_DeInitCardData(pboc_files);
    PBOC_DeInitTermData(pboc_term);    
    goto execute_end;
}
