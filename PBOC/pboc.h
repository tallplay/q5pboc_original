#ifndef __PBOC_HH__
#define __PBOC_HH__

#ifdef  __cplusplus
extern "C"{
#endif

#ifdef WIN32
#include "stdiint.h"
#else
#include <stdint.h>
#endif

typedef struct aaa {
    uint8_t * data;
    uint32_t len;
} RESAPD;

//data structure
typedef struct {
    uint8_t sfi;
    uint8_t record_firat;
    uint8_t record_last;
    uint8_t sfi_sdanum;
}PBOC_CARD_FILE;

#define MAX_CTAG 50

typedef struct {
    PBOC_CARD_FILE *files;
    uint8_t *candidate_aid;
    uint8_t aid[17]; //lv
    uint8_t *tag_data[MAX_CTAG];
    uint8_t *sda_list;
    
    uint16_t sda_listlen;        
    uint8_t file_num;
    uint8_t tag_num;
}PBOC_CARD_FILES;

typedef struct {
    uint8_t *fix_ltlv;
    uint8_t *transaction_ltlv;
    uint8_t *generate_ltlv;
    uint8_t changeable_lv[9];
    uint8_t unpredict[4];
    uint8_t date[4];
    uint8_t time[4];
}PBOC_TERM_DATA;

#define SECTOR_CA_NUM 4
#ifdef PBOC_EXTENDED_STORAGE
#define EXTENDED_CA_NUM 28
#endif

#define SECTOR_AID_NUM 5

#ifdef PBOC_EXTENDED_STORAGE
#define EXTENDED_AID_NUM 11
#endif


#define FCI_TAG 0x6F
#define FCI_DF_TAG 0x84
#define FCI_ACI_TAG 0xA5
#define FCI_SFI_TAG 0x88
#define APP_TAG 0x50
#define PDOL_TAG 0x9F38
#define API_TAG 0x87
#define API_NAME_TAG 0x9F12

//card TAG begin
#define CTAG_AIP 0x82
#define CTAG_PAN 0x5A //CN
#define CTAG_TRACK2 0x9F20
#define CTAG_CARD_SERIAL 0x5F34
#define CTAG_T2  0x57
#define CTAG_CA_PKI 0x8F
#define CTAG_CVM 0x8E
#define CTAG_ISSUE_MOD_RESIDUE 0x92
#define CTAG_ISSUE_EXP 0x9F32
#define CTAG_ISSUE_SIGNED 0x90
#define CTAG_DATA_SIGNED 0x93
#define CTAG_DA_TAG_LIST 0x9F4A
#define CTAG_IC_SIGNED 0x9F46
#define CTAG_DATA_AUTH_CODE 0x9F45
#define CTAG_APP_VER 0x9F08
#define CTAG_APP_AUC 0x9F07
#define CTAG_APPEXPIRE 0x5F24
#define CTAG_ISSUER_CONTRY 0x5F28
#define CTAG_APP_START_DATE 0x5F25
#define CTAG_APP_STOP_DATE 0x5F26
#define CTAG_OFF_TRACK_LOWER_BOND 0x9F14
#define CTAG_OFF_TRACK_UPPER_BOND 0x9F23
#define CTAG_IAC_DEFAULT 0x9F0D
#define CTAG_IAC_REFUSE 0x9F0E
#define CTAG_IAC_CONNECT 0x9F0F
#define CTAG_TDOL 0x97
#define CTAG_TDOL_TC 0x98
#define CTAG_CDOL1 0x8c
#define CTAG_CDOL2 0x8d
#define CTAG_CID 0x9F27
#define CTAG_AC 0x9F26
#define CTAG_ISSUE_APDATA 0x9F10
#define CTAG_CARD_TYPE 0x9F63 //卡產品標示
#define CTAG_EC_ISSUE_AUTH_CODE 0x9F74 //電子現金發卡授權碼
#define CTAG_APP_TEMPLATE 0x61
#define CTAG_ADF_NAME 0x4F
#define CTAG_DDF_NAME 0x9D
#define CTAG_HOLD_VTYPE 0x9F62  //CN
#define CTAG_AFL 0x94
#define CTAG_APP_CURRENCY 0x9F42
/*use on get data*/
#define CTAG_ATC 0x9F36
#define CTAG_CATC 0x9F13
//card TAG end
/////////////////////////////termimal TAGS//////////////////////////////////////////////////////////

/*non-used tags begin*/
#define TTAG_AMOUNT_REF 0x9F3A
#define TTAG_ACQUIRER_ID 0x9F01
#define TTAG_EXTRA_CABILITY 0x9F40
#define TTAG_TEST_SEQ_COUNTER 0x9F41
#define TTAG_AUTH_OTHER_QUAN_BINARY 0x9F04
#define TTAG_MERCHANT_CATEGORY_CODE 0x9F15
#define TTAG_MERCHANT_ID 0x9F16
#define TTAG_POS_ENTRY 0x9F39
#define TTAG_TERM_ID 0x9F1C
/*non-used tags end*/

/*device specified tag begin*/
#define TTAG_IAD 0x9F1E
/*device specified tag end*/

/*set AID tags begin*/
#define TTAG_RID 0x9F06
#define TTAG_ASI 0xDF01
#define TTAG_APP_VER 0x9F09
#define TTAG_TAC_DEFAULT 0xDF11
#define TTAG_TAC_ONLINE 0xDF12
#define TTAG_TAC_DENIAL 0xDF13
#define TTAG_FLOOR_LMT 0x9F1B
#define TTAG_BIAS_RANDOM_SEL_THRESHOLD 0xDF15
#define TTAG_BIAS_RANDOM_SEL_MAX_PERCENTAGE 0xDF16
#define TTAG_RANDOM_SEL_PERCENTAGE 0xDF17
#define TTAG_DEFAULT_DDOL 0xDF14
#define TTAG_CONNECT_PIN_SUPPORT 0xDF18
/*set AID tags end*/

/*set CA tags begin*/
#define TTAG_CA_IDX 0x9F22
#define TTAG_CA_EXP 0xDF04
#define TTAG_CA_MOD 0xDF02
/*set CA tags end*/

/*fix tags begin*/
#define TTAG_TRAN_REF_CUR 0x9F3C
#define TTAG_TRAN_REF_CUR_EXP 0x9F3D
#define TTAG_MONEY_CONTRY 0x5F2A
#define TTAG_CONTRY 0x9F1A
#define TTAG_TERMINAL_CAP 0x9F33
#define TTAG_MERCHANT_NAME 0x9F4E //20 bytes
#define TTAG_TERMINAL_TYPE 0x9F35
/*fix tags end*/

/*changable tags begin*/
#define TTAG_UNPREDICT 0x9F37
#define TTAG_TVR 0x95
#define TTAG_TSI 0x9B
#define TTAG_DATE 0x9A
#define TTAG_TIME 0x9F21
/*changable tags end*/

/*trasaction tags begin*/
#define TTAG_AUTH_QUAN 0x9F02
#define TTAG_AUTH_QUAN_BINARY 0x81
#define TTAG_AUTH_OTHER_QUAN 0x9F03
#define TTAG_TRANACTION_TYPE 0x9C
#define TTAG_FORCE_CONNECT 0xDF72
#define TTAG_VENDOR_TRANSACTION_TYPE 0xDF7C
#define TTAG_VENDOR_FLOW 0xDF71
#define TTAG_VENDOR_SEL_ACCOUNT 0xDF73

#define TTAG_ARC 0x8A //授權響應馬, 2
#define TTAG_AUTH_CODE 0x89 //授權碼, 6
#define TTAG_ISSUER_AUTH_DATA 0x91 //發卡行認證數據, 8-16
#define TTAG_ISSUER_SCRIPT1 0x71
#define TTAG_ISSUER_SCIRPT2 0x72
/*transaction tags end*/

/*genrated tags begin*/
#define TTAG_HINT_PIN 0xDF37
#define TTAG_SCRIPT_RESULT 0xDF31
#define TTAG_CVM_RESULT 0x9F34
/*genrated tags end*/

#define TTAG_FAKE 0x2F

//CVM
#define CVM_TYPE_FAIL 0x0
#define CVM_TYPE_OFF_PIN 0x1
#define CVM_TYPE_ON_PIN 0x2
#define CVM_TYPE_OFF_PIN_AND_SIGN 0x3
#define CVM_TYPE_OFF_PIN2 0x4
#define CVM_TYPE_OFF_PIN3 0x5
#define CVM_TYPE_SIGN 0x1E
#define CVM_TYPE_NO_CVM 0x1F

#define CVM_COND_ALWAYS 0x00
#define CVM_COND_ATM_CASH 0x1
#define CVM_COND_NO_CASH 0x2
#define CVM_COND_SUPPORT 0x3
#define CVM_COND_PEOPLE_CASH 0x4
#define CVM_COND_WITHDRAW 0x5
#define CVM_COND_LESS_X 0x6
#define CVM_COND_MORE_X 0x7
#define CVM_COND_LESS_Y 0x8
#define CVM_COND_MORE_Y 0x9

//AIP bits
#define AIP_SDA_BIT 0x40
#define AIP_DDA_BIT 0x20
#define AIP_CV_BIT 0x10
#define AIP_TRM_BIT 0x08
#define AIP_IV_BIT 0x04
#define AIP_CDA_BIT 0x01

//PBOC_SetTVR()
#define TVR_RESET 0
#define TVR_GET 1
#define TVR_SET_NO_DA 2
#define TVR_SET_FAIL_SDA 3
#define TVR_SET_APPVER_NOMATCH 4
#define TVR_SET_SERVICE_NOT_SUPPORT 5
#define TVR_SET_APP_NOT_BORN 6
#define TVR_SET_APP_EXPIRE 7
#define TVR_SET_CVM_FAIL 8
#define TVR_FORCE_CONNECTED 9
#define TVR_EXCEED_FLOOR_LIMIT 10
#define TVR_EXCEED_LOWER_TIME  11
#define TVR_EXCEED_UPPER_TIME  12
#define TVR_NEW_CARD 13
#define TVR_IC_DATA_MISS 14
#define TVR_KB_FAIL 15
#define TVR_ISSUER_AUTH_FAIL 16
#define TVR_CONNECT_PIN_INPUTED 17
#define TVR_SCRIPT_FAIL_BEFORE_LAST_AC 18
#define TVR_SCRIPT_FAIL_AFTER_LAST_AC 19
#define TVR_SET_DEFAULT_TDOL 20
#define TVR_SET_SDA_SELECTED 21
#define TVR_SET_UNKNOWN_CVM 22


#define TVR0_NO_DA                      0x80
#define TVR0_SDA_FAIL                   0x40
#define TVR0_IC_DATA_MISS               0x20
#define TVR0_IC_BAD_FILE                0x10
#define TVR0_DDA_FAIL                   0x08
#define TVR0_CDA_FAIL                   0x04
#define TVR0_SDA_SELECTED               0x02 //un-documented
#define TVR1_APPVER_NOTMATCH            0x80
#define TVR1_APP_EXPIRE                 0x40
#define TVR1_APP_NOT_BORN               0x20
#define TVR1_SERVICE_NOT_ALLOW          0x10
#define TVR1_NEW_CARD                   0x08
#define TVR2_CVM_FAIL                   0x80
#define TVR2_UNKNOWN_CVM                0x40
#define TVR2_PIN_EXCEED                 0x20
#define TVR2_KB_FAIL                    0x10
#define TVR2_PIN_NOT_INPUT              0x08
#define TVR2_CONNECT_PIN_INPUTED        0x04
#define TVR3_EXCEED_FLOOR_LIMIT         0x80
#define TVR3_EXCEED_LOWER_TIME          0x40
#define TVR3_EXCEED_UPPER_TIME          0x20
#define TVR3_RANDOM_ONLINE              0x10
#define TVR3_FORCE_CONNECTED            0x08
#define TVR4_USE_DEFAUL_TDOL            0x80
#define TVR4_ISSUER_AUTH_FAIL           0x40
#define TVR4_SCRIPT_FAIL_BEFORE_LAST_AC 0x20
#define TVR4_SCRIPT_FAIL_AFTER_LAST_AC  0x10

//PBOC_SetTSI()
#define TSI_RESET 0
#define TSI_GET 1
#define TSI_SET_DA_EXECUTED 2
#define TSI_SET_CARD_BEHAVIOR_ANA_EXECUTED 3
#define TSI_SET_CVM_EXECUTED 4
#define TSI_SET_IA_EXECUTED 5
#define TSI_SET_TRM_EXECUTED 6
#define TSI_SET_SCRIPT_EXECUTED 7

//CID begin
#define CID_MASK 0xc0
#define CID_AAC 0x00
#define CID_TC 0x40
#define CID_ARQC 0x80

#define REASON_MASK 0x07
#define REASON_SERVICE_NOT_ALLOW 0x01
#define REASON_PIN_TRY_LIMIT 0x02
#define REASON_ISSUER_AUTH_FAIL 0x03

#define ADVICE_MASK 0x08
//CID end


//hal
void PBOC_InitVar(uint32_t (*papdu)(uint8_t * cmdAPDU, uint32_t len, uint8_t * resAPDU,uint32_t *reslen));
uint8_t *MPosGetAIDHook(uint8_t *aid/*lv or idx*/,uint32_t tag);
uint8_t MPosGetCAPKIHook(uint8_t *rid, uint8_t idx, uint32_t tag, uint8_t **tag_lv);

//data model    
void PBOC_SetTVR(uint8_t op, uint8_t *tvr);
void PBOC_SetTSI(uint8_t op, uint8_t *tsi);
void PBOC_InitCardData(PBOC_CARD_FILES *pboc_files);
void PBOC_DeInitCardData(PBOC_CARD_FILES *pboc_files);
uint8_t *PBOC_GetFileData(PBOC_CARD_FILES *pboc_files, uint32_t tag);
void PBOC_SetFileData(PBOC_CARD_FILES *pboc_files, uint32_t tag, uint8_t *taglv);
void PBOC_SetFileDataLVSep(PBOC_CARD_FILES *pboc_files, uint32_t tag, uint8_t tagl, uint8_t *tagv);
void PBOC_SetAidCandidate(PBOC_CARD_FILES *pboc_files, uint8_t *found_aid, uint8_t found_aid_len, uint8_t pri);
void PBOC_ClearAidCandidate(PBOC_CARD_FILES *pboc_files);
uint8_t PBOC_GetAidCandidate(PBOC_CARD_FILES *pboc_files, uint8_t idx, uint8_t **aid);
uint8_t *PBOC_GetTermData(PBOC_TERM_DATA *term_data, uint32_t tag);
void PBOC_AddTermFixData(PBOC_TERM_DATA *pboc_term, uint8_t *dat, uint8_t len, uint32_t tag, uint8_t taglen);
void PBOC_FreeTermFixData(PBOC_TERM_DATA *pboc_term);
void PBOC_AddTermGenData(PBOC_TERM_DATA *pboc_term, uint8_t *dat, uint8_t len, uint32_t tag, uint8_t taglen);
void PBOC_SetTermTransactionData(PBOC_TERM_DATA *pboc_term, uint8_t *tlv, uint32_t tlvlen);
void PBOC_InitTermData(PBOC_TERM_DATA *pboc_term);
void PBOC_DeInitTermData(PBOC_TERM_DATA *pboc_term);
void PBOC_DeleteTermGenData(PBOC_TERM_DATA *pboc_term);
void PBOC_AutoTermAppVer(uint8_t* aid, uint8_t *app_ver);
void PBOC_BuildSDAList(PBOC_CARD_FILES *pboc_file);

/*ca key*/
uint8_t PBOC_GetCAPKI(uint8_t *rid, uint8_t idx, uint32_t tag, uint8_t **tag_lv);

/*aid*/
uint8_t *PBOC_GetAID(uint8_t *aid/*lv*/,uint32_t tag);

//util
void PBOC_3DESEnc(uint8_t *key, uint8_t *pt, uint8_t *ct);
void PBOC_3DESDec(uint8_t *key, uint8_t *ct, uint8_t *pt);
uint8_t PBOC_TLVFindTag(uint8_t *tlv, uint32_t tlvlen, uint32_t tag_wanted, uint8_t **ret); //0 found, 1: not found, 2: error
uint32_t PBOC_TLVEnum(uint8_t **tlv, uint32_t *tlvlen, uint8_t **ret); //0 not found, else tag
uint32_t PBOC_TLVSearch61(uint8_t **tlv, uint32_t *tlvlen, uint8_t **lv); //0 not found, else tag
uint32_t PBOC_TLEnum(uint8_t **tlv, uint32_t *tlvlen, uint32_t *len); //0 not found, else tag
void PBOC_AssmTLV(uint32_t tag, uint8_t taglen, uint16_t dlen, uint8_t *dat, uint8_t tlv[1024], uint32_t *tlvidx);
uint32_t BCD6byte_2Number(uint8_t *bcd);
uint8_t PBOC_BuildListByDF(PBOC_CARD_FILES *pboc_files, uint8_t *DF, uint8_t DFLEN, uint8_t (*aid_found)(uint8_t* aid, uint8_t len, uint8_t priority, PBOC_CARD_FILES *pboc_files));
void PBOC_BuildDOLList(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files, uint8_t **dol, uint32_t *dollen, uint8_t *list, uint16_t *listlen);
void PBOC_CopyLV(uint32_t tag, uint8_t *lv, uint8_t *target, uint8_t len);
void Hex2BCD(uint32_t len, uint8_t *bcd);

//command
uint8_t PBOC_SelectDF(uint8_t *name, uint8_t namelen, uint8_t **tlv, uint32_t *tlvlen, uint8_t **dfname, uint8_t P2);
uint8_t PBOC_GPO(uint8_t *pdol, uint8_t pdollen, PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term);
uint8_t PBOC_ReadRecord(uint8_t rec, uint8_t sfi, uint8_t **tlv, uint32_t *tlvlen);
uint8_t PBOC_SelectApp(uint8_t *aid, uint8_t aidlen, uint8_t *gpo_buf, PBOC_TERM_DATA *pboc_data, PBOC_CARD_FILES *pboc_files);

//PBOC terminal flows
//應用選擇
uint8_t PBOCF_SelectApp(uint8_t **pdol, PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term);
//應用初始化
uint8_t PBOCF_InitApp(uint8_t *pdol, PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term);
//讀應用資料
uint8_t PBOCF_ReadAppData(PBOC_CARD_FILES *pboc_files, void (*flow_ind)(uint8_t));
//脫機數據認證
uint8_t PBOCF_DataAuth(PBOC_TERM_DATA *pboc_data, PBOC_CARD_FILES *pboc_files);
//處理限制
uint8_t PBOCF_ProcessRestriction(PBOC_TERM_DATA *pboc_data, PBOC_CARD_FILES *pboc_files);
//持卡人驗證
uint8_t PBOCF_CVM(PBOC_TERM_DATA *pboc_data, PBOC_CARD_FILES *pboc_files);
//終端風險管理
uint8_t PBOCF_TermRiskManager(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files);
//終端行為分析
uint8_t PBOCF_TermActionAna(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files, uint8_t *AC_P1);
//卡片行為分析
uint8_t PBOCF_CardActionAna(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files, uint8_t AC_P1);
//外部認證, 二次AC
uint8_t PBOCF_ExtAuth_2ndAC(PBOC_TERM_DATA *pboc_term, PBOC_CARD_FILES *pboc_files, uint8_t *AC_P2);


//PBOC Top FLOWs
uint8_t PBOCFF_Execute(PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term, uint8_t *transaction, uint32_t transaction_len, void (*result_callback)(uint8_t* resp, uint32_t resp_len), void (*flow_ind)(uint8_t));
uint8_t PBOCFF_2ndAuth(PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term, uint8_t *transaction, uint32_t transaction_len, void (*result_callback)(uint8_t* resp, uint32_t resp_len), void (*flow_ind)(uint8_t));
uint8_t PBOCFF_End(PBOC_CARD_FILES *pboc_files, PBOC_TERM_DATA *pboc_term);
#ifdef  __cplusplus
}
#endif

#endif
