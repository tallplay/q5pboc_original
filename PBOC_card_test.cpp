// PBOC_card_test.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "PBOC_card_test.h"
#include "PBOC_card_testDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPBOC_card_testApp

BEGIN_MESSAGE_MAP(CPBOC_card_testApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CPBOC_card_testApp construction

CPBOC_card_testApp::CPBOC_card_testApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CPBOC_card_testApp object

CPBOC_card_testApp theApp;


// CPBOC_card_testApp initialization

BOOL CPBOC_card_testApp::InitInstance()
{
	CWinApp::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CPBOC_card_testDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
