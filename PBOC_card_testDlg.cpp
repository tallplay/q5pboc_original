// PBOC_card_testDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PBOC_card_test.h"
#include "PBOC_card_testDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CPBOC_card_testDlg dialog
#define MODE_Q5 0
#define MODE_PC 1
#define READER_IDX 0 //first device
#define DEFAULT_MODE MODE_PC//MODE_Q5

uint8_t auto_out_in = 0;


CPBOC_card_testDlg::CPBOC_card_testDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPBOC_card_testDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	Issuer = 0;
	mode = DEFAULT_MODE; //0: q5 1:pc
}

PBOC_TERM_DATA pboc_term_data;
PBOC_CARD_FILES pboc_card_files;

void CPBOC_card_testDlg::ReaderOut()
{
    if(mode == MODE_PC)
	{
	    PBOC_DeInitCardData(&pboc_card_files);
	    PBOC_DeInitTermData(&pboc_term_data);

		if(pboc_term_data.fix_ltlv)
	    {
	        free(pboc_term_data.fix_ltlv);
	        pboc_term_data.fix_ltlv = 0;
	    }
	}
	else //Q5
	{
		if(serial_port.IsOpen())
    		serial_port.Close();
	}

	// TODO: Add your control notification handler code here

	if(Issuer)
	{
	    Issuer->DestroyWindow();
		delete Issuer;
    }
}

void CPBOC_card_testDlg::ReaderIn()
{
    OnBnClickedAtr();
}

CPBOC_card_testDlg::~CPBOC_card_testDlg()
{
    if(mode == MODE_PC)
	{	    PBOC_DeInitCardData(&pboc_card_files);
	    PBOC_DeInitTermData(&pboc_term_data);

		if(pboc_term_data.fix_ltlv)
	    {
	        free(pboc_term_data.fix_ltlv);
	        pboc_term_data.fix_ltlv = 0;
	    }
	}

	if(Issuer)
	{
	    Issuer->DestroyWindow();
		delete Issuer;
    }

}


void CPBOC_card_testDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_AID, myListAID);
	DDX_Control(pDX, IDC_LIST_CA, myListCA);
	DDX_Control(pDX, IDC_EDIT_9F02_AMOUNT, m_edit_9f02);
	DDX_Control(pDX, IDC_EDIT_9C_TYPE, m_edit_9c);
	DDX_Control(pDX, IDC_COMBO_TERMFIX, m_TermFix);
	DDX_Control(pDX, IDC_STATIC_PBOC_END_RESULT, m_pboc_result);
	DDX_Control(pDX, IDC_CHECK_FORCE_ONLINE, m_check_isforce_online);
	DDX_Control(pDX, IDC_CHECK_NO_AMOUNT, m_check_no_amount);
}

BEGIN_MESSAGE_MAP(CPBOC_card_testDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(ID_ATR, &CPBOC_card_testDlg::OnBnClickedAtr)
	ON_WM_TIMER()
//	ON_BN_CLICKED(IDC_BUTTON_EDIT_AID, &CPBOC_card_testDlg::OnBnClickedButtonEditAid)
	ON_BN_CLICKED(IDC_RADIO_PC, &CPBOC_card_testDlg::OnBnClickedRadioS)
	ON_BN_CLICKED(IDC_RADIO_Q5, &CPBOC_card_testDlg::OnBnClickedRadioS)
	ON_EN_UPDATE(IDC_EDIT_9F02_AMOUNT, &CPBOC_card_testDlg::OnEnUpdateEdit9f02Amount)
	ON_EN_CHANGE(IDC_EDIT_9F02_AMOUNT, &CPBOC_card_testDlg::OnEnChangeEdit9f02Amount)
	ON_BN_CLICKED(IDC_BUTTON_EDIT_WAITIN, &CPBOC_card_testDlg::OnBnClickedButtonEditWaitin)
	ON_BN_CLICKED(IDC_BUTTON_LOCK_TIME, &CPBOC_card_testDlg::OnBnClickedButtonLockTime)
	ON_BN_CLICKED(IDC_CHECK_FORCE_ONLINE, &CPBOC_card_testDlg::OnBnClickedCheckForceOnline)
	ON_BN_CLICKED(IDC_CHECK_NO_AMOUNT, &CPBOC_card_testDlg::OnBnClickedCheckNoAmount)
	ON_BN_CLICKED(IDC_BUTTON_Swipe, &CPBOC_card_testDlg::OnBnClickedButtonSwipe)
END_MESSAGE_MAP()


BOOL CPBOC_card_testDlg::CheckCard()
{
    if(mode == MODE_PC)
	{
		if((m_SCReader.TrackingCard(SCARD_STATE_UNAWARE) & (SCARD_STATE_PRESENT)) == (SCARD_STATE_PRESENT))
		{
			BYTE atr[32];
			DWORD atrlen = 32;

			//TRACE("SCARD_STATE_PRESENT\n");
//		    if(lRet == 0)
			{
		    	m_SCReader.GetCardState(atr, &atrlen);
		    	m_SCReader.GetCardState(atr, &atrlen);

				return TRUE;
			}
		}
		
	    return FALSE;
	}
	else
	{
	    uint16_t resp;
		uint8_t plugged;

	    //Q5
	    if((MposSendCmd(0, 0,  CMD_OP_VENDOR_CHECK_SCR, 0, resp, &plugged, 1, 10000) == 1) && plugged)
			return TRUE;
		
		return FALSE;
	}	
}



// CPBOC_card_testDlg message handlers

CPcScCtrl *cheatPCSC;

uint32_t DoAPDU_p(uint8_t* cmdAPDU,uint32_t  len,uint8_t* resAPDU,uint32_t *reslen)
{
	DWORD rlen;
	uint32_t ret;
	rlen = *reslen;
	ret = cheatPCSC->DoAPDU_p(cmdAPDU, len, resAPDU, &rlen);
	*reslen = (uint32_t)rlen;
    return 0; //!(ret == 0x9000);
}

CPBOC_card_testDlg *ThisIsMe = 0;
uint8_t terminal_type = 0x34;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
uint8_t sbyte(uint8_t byte)
{
    return ThisIsMe->serial_port.Write(&byte, 1);
}

uint32_t sbytes(uint32_t len, uint8_t * buf)
{
    return ThisIsMe->serial_port.Write(buf, len);
}

uint32_t rbytes(uint32_t len, uint8_t * buf)
{
    return ThisIsMe->serial_port.Read(buf, len);
}


#define PBOC_WAIT_READER_IN 1
#define PBOC_WAIT_CARD_IN 2
#define PBOC_EXECUTE 3
#define PBOC_WAIT_CARD_OUT 4
#define PBOC_EXECUTE_END 5 //to prevent status miss
#define PBOC_UPDATE_TIME 6
#define PBOC_WAIT_SWIPE 7
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct LANGANDCODEPAGE {
  WORD wLanguage;
  WORD wCodePage;
} *lpTranslate;

#pragma comment(lib, "Version.lib ")
#include <Strsafe.h>
CString GetProductVersion()
{
 DWORD   dwSize,dwHandle;   
 TCHAR   *byData, *lpVers;//*lpTranslate ;     
 TCHAR   sAppName[MAX_PATH];   
 TCHAR   SubBlock[MAX_PATH];
 CString   strVersion;   
 unsigned   int   uLen, cbTranslate;  

 //?得版本?   
 _stprintf(sAppName,_T("%s.exe"),AfxGetApp()->m_pszExeName);//若?MFC程序,     可用此句?得?前程序(或DLL)文件所在目?  

 dwSize=GetFileVersionInfoSize(sAppName,&dwHandle);   
 byData=new TCHAR[dwSize+10];
 memset(byData, 0, dwSize+10);
 GetFileVersionInfo(sAppName,NULL,dwSize,byData); 

 VerQueryValue(byData, 
              TEXT("\\VarFileInfo\\Translation"),
              (LPVOID*)&lpTranslate,
              &cbTranslate);

 for(int i=0; i < (cbTranslate/sizeof(struct LANGANDCODEPAGE)); i++ )
{
  if(FAILED(StringCchPrintf(SubBlock, 50,
            TEXT("\\StringFileInfo\\%04x%04x\\ProductVersion"),
            lpTranslate[i].wLanguage,
            lpTranslate[i].wCodePage)))
	{
		return NULL;
	// TODO: write error handler.
	}

  // Retrieve file description for language and code page "i". 
  VerQueryValue(byData, 
                SubBlock, 
                (LPVOID*)&lpVers, 
                &uLen); 
}

 //VerQueryValueW(byData,TEXT("\\StringFileInfo\\080403a8\\ProductVersion"),(void **)&lpVers, &uLen);  

 //去掉版本?的最后一位,并?','改?'.'
 strVersion=lpVers;   
 strVersion.Replace(_T(","),_T("."));   
 strVersion.Replace(_T("   "),_T(""));  

 delete [] byData;

 return strVersion;
}

CString str_wait_swipe = TEXT("=>[Waiting Swipe]<=");
CString str_wait_reader_in = TEXT("=>[Waiting Reader In]<=");
CString str_wait_card_in = TEXT("=>[Waiting Card In]<=");
CString str_wait_card_out = TEXT("=>[Waiting Card Out]<=");
CString str_wait_execute = TEXT("...[Executing]...");
static uint8_t static_tick = 0;

void CPBOC_card_testDlg::Go2State(int state)
{
	flow_state = state;	
	static_tick = 0;

	switch(state)
	{
		case PBOC_WAIT_READER_IN:
			this->SetWindowTextW(version + str_wait_reader_in);			
		    this->GetDlgItem(IDC_BUTTON_EDIT_WAITIN)->EnableWindow(0);
		    this->GetDlgItem(IDC_BUTTON_Swipe)->EnableWindow(0);
			this->GetDlgItem(IDC_BUTTON_LOCK_TIME)->EnableWindow(0);
			m_check_isforce_online.EnableWindow(0);
			break;
		case PBOC_WAIT_CARD_IN:
			if(auto_out_in)
			    KillTimer(PBOC_AUTO_OUT_IN);
			this->SetWindowTextW(version + str_wait_card_in);
		    this->GetDlgItem(IDC_BUTTON_EDIT_WAITIN)->EnableWindow(0);
		    this->GetDlgItem(IDC_BUTTON_Swipe)->EnableWindow(1);
			this->GetDlgItem(IDC_BUTTON_LOCK_TIME)->EnableWindow(1);
			m_check_isforce_online.EnableWindow(1);
			break;
		case PBOC_WAIT_SWIPE:
		{
			CString str;
			this->SetWindowTextW(version + str_wait_swipe);
		    this->GetDlgItem(IDC_BUTTON_EDIT_WAITIN)->EnableWindow(0);
		    this->GetDlgItem(IDC_BUTTON_Swipe)->EnableWindow(0);
			this->GetDlgItem(IDC_BUTTON_LOCK_TIME)->EnableWindow(1);
			m_check_isforce_online.EnableWindow(1);
			second = 0;
			SetTimer(SECOND_TIMER, 1000, 0);
			RunSwipe(1);


	    	ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T(""));
			//////prompt
			ThisIsMe->m_pboc_result.SetTextColor(LIGHTBLUE);
			str.LoadStringW(IDS_STRING_PLEASE_SWIPE);
			ThisIsMe->m_pboc_result.SetWindowTextW(str);
            
			break;
		}
		case PBOC_WAIT_CARD_OUT:
		{
			if(auto_out_in)
				SetTimer(PBOC_AUTO_OUT_IN, 10000, 0);
			this->SetWindowTextW(version + str_wait_card_out);
		    this->GetDlgItem(IDC_BUTTON_EDIT_WAITIN)->EnableWindow(1);			
		    this->GetDlgItem(IDC_BUTTON_Swipe)->EnableWindow(1);
			this->GetDlgItem(IDC_BUTTON_LOCK_TIME)->EnableWindow(1);
			break;
		}
	    case PBOC_EXECUTE:
			this->SetWindowTextW(version + str_wait_execute);
	    	ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T(""));
	    	ThisIsMe->m_pboc_result.SetWindowTextW(_T(""));		
			ThisIsMe->m_pboc_result.SetTextColor(LIGHTBLUE);
			this->GetDlgItem(IDC_BUTTON_LOCK_TIME)->EnableWindow(0);
			
			break;
	}	
}

void CPBOC_card_testDlg::StateProcess()
{
    switch (flow_state)
	{
	    case PBOC_WAIT_READER_IN:
			if(CheckReader())
			{
				try 
				{
					this->SetWindowTextW(version + TEXT("          >>>>>Initializing Reader<<<<<"));
    				ReaderIn();
				}
				catch(CSerialException *e)
				{
					ReaderOut();
		 			Go2State(PBOC_WAIT_READER_IN);
					return;
				}
			    Go2State(PBOC_WAIT_CARD_IN);
				return;
			}		
			else
			{
			    CString str;
				str = version;
				
				for(int i = 0; i < (static_tick); i++)
					str += _T(" ");
               
    			this->SetWindowTextW(str + str_wait_reader_in);			
		    }
				
			break;
		case PBOC_WAIT_SWIPE:
			try 
			{
			    CString str;
				str = version;
				
				for(int i = 0; i < static_tick; i++)
					str += _T(" ");
               
    			this->SetWindowTextW(str + str_wait_swipe);			
				
			}
			catch(CSerialException *e)
			{
				ReaderOut();
	 			Go2State(PBOC_WAIT_READER_IN);
				return;
			}
			break;
	    case PBOC_WAIT_CARD_IN:
			try 
			{
		        if(CheckCard())
		    	{
					TRACE(TEXT("card found!!"));

                    Go2State(PBOC_EXECUTE);
					second = 0;
					SetTimer(SECOND_TIMER, 1000, 0);
					RunPBOC(1);
					
		    	}
				else
				{
				    CString str;
					str = version;
					
					for(int i = 0; i < static_tick; i++)
						str += _T(" ");
	               
	    			this->SetWindowTextW(str + str_wait_card_in);			
			    }
				
			}
			catch(CSerialException *e)
			{
				ReaderOut();
	 			Go2State(PBOC_WAIT_READER_IN);
				return;
			}
			break;
	    case PBOC_EXECUTE:
		{
            CString str, str1;
			
			str1 = version;
			for(int i = 0; i < static_tick&0xf; i++)
			{
				str += _T(".");					
				str1 += _T(" ");
			}
	               
			this->SetWindowTextW(str1 + str_wait_execute);		
			
				
	    	ThisIsMe->m_pboc_result.SetWindowTextW(str);
			break;
    	}
	    case PBOC_WAIT_CARD_OUT:
			try 
			{
			    if(!CheckCard())
		    	{
					TRACE(TEXT("card out!!"));
					
				    Go2State(PBOC_WAIT_CARD_IN);
		    	}
				else
				{
				    CString str;
					str = version;
					
					for(int i = 0; i < static_tick; i++)
						str += _T(" ");
	               
	    			this->SetWindowTextW(str + str_wait_card_out);			
			    }			
			}
			catch(CSerialException *e)
			{
				ReaderOut();
	 			Go2State(PBOC_WAIT_READER_IN);
				return;
			}
			break;
	    case PBOC_UPDATE_TIME:
			AdjustTime();
			Go2State(PBOC_WAIT_CARD_OUT);
			break;
	}

	static_tick++;

}

BOOL isTimeLocked = FALSE;
int year_idx,month_idx,day_idx,hour_idx,min_idx,sec_idx;


extern "C" {
#include <time.h>
void GetTime(uint8_t time_str[15])
{
    if(isTimeLocked == FALSE)
	{
		time_t timep;
		struct tm *tm_now;
		time(&timep);
		tm_now=localtime(&timep);

	    sprintf((char*)time_str, "%04d%02d%02d%02d%02d%02d", tm_now->tm_year + 1900, tm_now->tm_mon+1, tm_now->tm_mday, 
	                    tm_now->tm_hour, tm_now->tm_min, tm_now->tm_sec);
	}
	else
	{
	    sprintf((char*)time_str, "%04d%02d%02d%02d%02d%02d", year_idx , month_idx, day_idx, 
	                    hour_idx, min_idx, sec_idx);
	}
    return;
}

DWORD GetSecs()
{
	time_t timep;
	struct tm *tm_now;
	time(&timep);
	tm_now=localtime(&timep);

	return timep;
}



void GetTimes(int *year_ix, int *month_ix, int *day_ix, int *hour_ix, int *min_ix, int *sec_ix)
{
    if(isTimeLocked == FALSE)
	{
		time_t timep;
		struct tm *tm_now;
		time(&timep);
		tm_now=localtime(&timep);
		*year_ix = tm_now->tm_year + 1900; 
		*month_ix = tm_now->tm_mon+1;
		*day_ix = tm_now->tm_mday; 
	    *hour_ix = tm_now->tm_hour;
		*min_ix = tm_now->tm_min;
		*sec_ix = tm_now->tm_sec	;
	}
	else
	{
		*year_ix  = year_idx ;
		*month_ix = month_idx;
		*day_ix   = day_idx  ;
	    *hour_ix  = hour_idx ;
		*min_ix   = min_idx  ;
		*sec_ix   = sec_idx  ;
	}
}

void GetRandom(uint8_t randnum[8])
{
    uint8_t i;
    for(i = 0; i < 8; i++)
	{
	    randnum[i] = rand();
	}
}

};

void CPBOC_card_testDlg::AdjustTime()
{
    uint16_t resp;
	uint8_t result[1024];
	uint32_t len;
	uint8_t time[15];
	GetTime(time);
	len = MposSendCmd(time, 14, CMD_OP_SET_TIME, 0, resp, result, 1024, 0, 0);

	if(resp != RES_OP_OK)
	    TRACE(TEXT("SHIT"));

}

BYTE random_number_analysis[10000][8];
void CPBOC_card_testDlg::InitRandom()
{
    uint16_t resp;
	uint8_t result[1024];
	uint32_t len;
	
	for(int j = 0; j < 1; j++)
	{
	    len = MposSendCmd(0, 0, CMD_OP_GET_RANDOM, 0, resp, result, 1024, 0, 0);

    	if(resp != RES_OP_OK)
	        TRACE(TEXT("SHIT"));

		else
		{
			TRACE(TEXT("%d\n"), j);

			memcpy(random_number_analysis[j], result, 8);
           /*
			for(int i = 0; i < 8; i++)
     		{
     			TRACE(TEXT("%02X"), result[i]);
     		}
			TRACE(TEXT("\n"));*/
		}
	}

#if 0
	FILE *fp;
	char str[20];
	fp = fopen("random.txt", "a+");
	for(int i = 0; i < 1000; i++)
	{
        sprintf(str, "%02X%02X%02X%02X%02X%02X%02X%02X\n", random_number_analysis[i][0],  random_number_analysis[i][1], random_number_analysis[i][2], random_number_analysis[i][3], random_number_analysis[i][4]
	,random_number_analysis[i][5], random_number_analysis[i][6], random_number_analysis[i][7]);
	fwrite(str, 1, strlen(str), fp);
	}
	fclose(fp);
#endif	
#if 0
	FILE *fp;
	char str[20];
	fp = fopen("random.txt", "a+");
	for(int i = 0; i < 10000; i++)
	{
       fscanf(fp, "%02X%02X%02X%02X%02X%02X%02X%02X\n", &random_number_analysis[i][0],  &random_number_analysis[i][1], &random_number_analysis[i][2], &random_number_analysis[i][3], &random_number_analysis[i][4]
	,&random_number_analysis[i][5], &random_number_analysis[i][6], &random_number_analysis[i][7]);
	}
	fclose(fp);

#endif

#if 0
    //judge if reduncy
	for(int i = 0; i < 10000; i++)
	{
		for(int j = i+1; j < 10000; j++)
		{
			    if(!memcmp(&random_number_analysis[i][4], &random_number_analysis[j][4], 4)) 
				{
					CString str;
					str.Format(TEXT("Random number error in %d and %d!!"), i, j);
					MessageBox(str);
				}
		}
	}
#endif	

}

BOOL CPBOC_card_testDlg::OnInitDialog()
{
    DWORD dwCurrentState =  SCARD_STATE_UNAWARE;
	CString cstr;

	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	ThisIsMe = this;

	//Init HW
    m_SCReader.SetCLA(0x00);
	cheatPCSC = &m_SCReader;

   	MposRegisterSender(&sbyte, &sbytes);
    MposRegisterReceiver(&rbytes);

    //Read Setting
	if(!ReadFileSetting())
	{
	    cstr.Format(_T("File Error!"));		
		Helper::ShowUserMsg(_T("Error"), cstr, MB_OK);
		exit(0);
	}

	//Init UI
	InitUI();
    InitCAUI();
    InitAIDUI();	

    CButton* radio = (CButton*)GetDlgItem(IDC_RADIO_PC);
	radio->SetCheck(mode);

	Go2State(PBOC_WAIT_READER_IN);
	SetTimer(PBOC_STATE_PROCESS, 250, 0);

	second = 0;

	m_pboc_result.SetFont(200, TEXT("Arial Bold"));
	version = TEXT("PBOC Level 2 Testing Tool(") + GetProductVersion() + TEXT(")");
	tsc_9f41 = GetSecs();
    m_check_isforce_online.SetCheck(1);

    return TRUE;
	
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPBOC_card_testDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPBOC_card_testDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void toHex(TCHAR hi, TCHAR lo, char *hex)
{
    TCHAR digstr[2] = _T("0");
    unsigned int n, m;

    digstr[0] = hi;    _stscanf(digstr, _T("%x"), &n);
    digstr[0] = lo;    _stscanf(digstr, _T("%x"), &m);
    *hex = (n << 4) + m;
}


void Text2Bin(TCHAR *text, int textlen, char*bin)
{
   char dat;
   for(int i = 0; i < textlen; i+=2)
    {
        toHex(text[i], text[i+1], &dat);    
        bin[i/2] = dat;
    }
}



BOOL pack_ltlv_hex(char * inbuf, unsigned char * outbuf)
{
    int len;
	DWORD bcd_len;
	DWORD hex_len;
    unsigned char c;

	strupr(inbuf);
	
    len = strlen(inbuf);

    if(len & 1)
		return FALSE;
	
////////////////////////////////////////////////////////////
		c = (inbuf[0] > '9' ? inbuf[0] - 'A' + 0xa : inbuf[0] - '0');
		c <<= 4;
		c += (inbuf[1] > '9' ? inbuf[1] - 'A' + 0xa : inbuf[1] - '0');
        bcd_len = c;
		c = (inbuf[2] > '9' ? inbuf[2] - 'A' + 0xa : inbuf[2] - '0');
		c <<= 4;
		c += (inbuf[3] > '9' ? inbuf[3] - 'A' + 0xa : inbuf[3] - '0');
		bcd_len = (bcd_len << 8) + c;

		hex_len = (bcd_len & 0xf) + ((bcd_len >> 4) & 0xf)*10 + ((bcd_len >> 8) & 0xf)*100 + ((bcd_len >> 12) & 0xf)*1000;

		if(hex_len   != (len - 4)/2)
		{
		    return FALSE;
		}
		
		*outbuf++ = hex_len >> 8;
		*outbuf++ = hex_len & 0xff;

        
////////////////////////////////////////////////////////////	
	for(int i = 4; i < len; i+=2)
	{
		c = (inbuf[i] > '9' ? inbuf[i] - 'A' + 0xa : inbuf[i] - '0');
		c <<= 4;
		c += (inbuf[i+1] > '9' ? inbuf[i+1] - 'A' + 0xa : inbuf[i+1] - '0');

		*outbuf++ = c;
	}

	return TRUE;
} 

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//storage modal
///////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct {
#ifdef PBOC_EXTENDED_STORAGE
    uint8_t data[SECTOR_CA_NUM+EXTENDED_CA_NUM][512]; //only 4 sectors
#else
    uint8_t data[SECTOR_CA_NUM][512]; //only 4 sectors
#endif    
}PBOC_CA_KEY_STORAGE;

typedef struct {
#ifdef PBOC_EXTENDED_STORAGE
    uint8_t data[SECTOR_AID_NUM+EXTENDED_AID_NUM][512]; //only 4 sectors
#else
    uint8_t data[SECTOR_AID_NUM][512]; //only 4 sectors
#endif
}PBOC_AID_STORAGE;

#ifdef WIN32
PBOC_CA_KEY_STORAGE ca_key_storage;
PBOC_AID_STORAGE aid_storage;
#endif

uint8_t Storage_SetCAPKI(uint8_t *LLtlv) //LL is BCD
{
    uint8_t i;
    uint32_t len;
#ifdef WIN32
    //check if full
    for(i = 0; i < SECTOR_CA_NUM; i++)
    {
        if(ca_key_storage.data[i][0] == 0xff) //empty
            break;
    }


    if(i == SECTOR_CA_NUM)
        goto check_extend_ca;

     len = (LLtlv[0] << 8) + LLtlv[1];

    //write data
    if(len < 510)
    {
        ca_key_storage.data[i][0] = (len >> 8) | 0xf0; //0xf0 is mark for used, and (len >> 8) shouldn't be 0xF
        ca_key_storage.data[i][1] = len & 0xff;
        memcpy(&ca_key_storage.data[i][2], &LLtlv[2], len);
    }

check_extend_ca:
    #ifdef PBOC_EXTENDED_STORAGE

    //check if full
    for(i = SECTOR_CA_NUM; i < SECTOR_CA_NUM+EXTENDED_CA_NUM; i++)
    {
        if(ca_key_storage.data[i][0] == 0xff) //empty
            break;
    }


    if(i == SECTOR_CA_NUM+EXTENDED_CA_NUM)
        return 1; //full

     len = (LLtlv[0] << 8) + LLtlv[1];

    //write data
    if(len < 510)
    {
        ca_key_storage.data[i][0] = (len >> 8) | 0xf0; //0xf0 is mark for used, and (len >> 8) shouldn't be 0xF
        ca_key_storage.data[i][1] = len & 0xff;
        memcpy(&ca_key_storage.data[i][2], &LLtlv[2], len);
    }
#endif	

#endif    

    return 0;
}


uint8_t Storage_SetAID(uint8_t *lltlv)
{
    uint8_t i;
    uint32_t len;
#ifdef WIN32
    //check if full
    for(i = 0; i < SECTOR_AID_NUM; i++)
    {
        if(aid_storage.data[i][0] == 0xff) //empty
            break;
    }


    if(i == SECTOR_AID_NUM)
        goto check_extend_aid;

    //bcd to number
//    len = (lltlv[0] & 0xf) * 100 + (lltlv[1] >> 4) * 10 + (lltlv[1] & 0xf);
	len = (lltlv[0] << 8) + lltlv[1]; //its hex now

    //write data
    if(len < 510) 
    {
        aid_storage.data[i][0] = (len >> 8) | 0xf0; //0xf0 is mark for used, and (len >> 8) shouldn't be 0xF
        aid_storage.data[i][1] = len & 0xff;
        memcpy(&aid_storage.data[i][2], &lltlv[2], len);
    }

    return 0;
    
check_extend_aid:
    #ifdef PBOC_EXTENDED_STORAGE
    //check if full
    for(i = SECTOR_AID_NUM; i < SECTOR_AID_NUM+EXTENDED_AID_NUM; i++)
    {
        if(aid_storage.data[i][0] == 0xff) //empty
            break;
    }


    if(i == SECTOR_AID_NUM+EXTENDED_AID_NUM)
        return 1;

    //bcd to number
//    len = (lltlv[0] & 0xf) * 100 + (lltlv[1] >> 4) * 10 + (lltlv[1] & 0xf);
	len = (lltlv[0] << 8) + lltlv[1]; //its hex now

    //write data
    if(len < 510)
    {
        aid_storage.data[i][0] = (len >> 8) | 0xf0; //0xf0 is mark for used, and (len >> 8) shouldn't be 0xF
        aid_storage.data[i][1] = len & 0xff;
        memcpy(&aid_storage.data[i][2], &lltlv[2], len);
    }
    
    #else
    return 1; //full
    #endif    
#else    
#endif    

    return 0;
}

extern "C"{
uint32_t StorageGetID(void) //HAL function
{
	return 0x12345678;
}

uint8_t *MPosGetAIDHook(uint8_t *aid/*lv or idx*/,uint32_t tag)
{
    uint8_t i;
    uint32_t len;
    uint8_t *candidate_lv;    

    if(aid < (uint8_t*)SECTOR_AID_NUM)
        i = (uint8_t)aid;
    else if(aid < (uint8_t*)100)
        goto check_extended;
    else
        i = 0;
    for(; i < SECTOR_AID_NUM; i++)
    {
        if(aid_storage.data[i][0] != 0xff) //not empty
        {
            len = ((aid_storage.data[i][0] & 0xf) << 8) + aid_storage.data[i][1];

            if(!PBOC_TLVFindTag(&aid_storage.data[i][2], len, TTAG_RID, &candidate_lv))
            {
                uint8_t aidlen;

                if(aid < (uint8_t*)SECTOR_AID_NUM)
                {
                    return candidate_lv;
                }

                aidlen = aid[0] > candidate_lv[0] ? candidate_lv[0] : aid[0];
                if(aid[0] == candidate_lv[0] && !memcmp(candidate_lv+1, aid+1, aidlen))
                {
                    if(!PBOC_TLVFindTag(&aid_storage.data[i][2], len, tag, &candidate_lv))
                    {
                        return candidate_lv;;
                    }
                }
            }
        }
    }
check_extended:
    #ifdef PBOC_EXTENDED_STORAGE
    if(aid < (uint8_t*)SECTOR_AID_NUM+EXTENDED_AID_NUM)
        i = (uint8_t)aid;
    else if(aid < (uint8_t*)100)
        return 0;
    else
        i = SECTOR_AID_NUM;
    for(; i < SECTOR_AID_NUM+EXTENDED_AID_NUM; i++)
    {
        if(aid_storage.data[i][0] != 0xff) //not empty
        {
            len = ((aid_storage.data[i][0] & 0xf) << 8) + aid_storage.data[i][1];

            if(!PBOC_TLVFindTag(&aid_storage.data[i][2], len, TTAG_RID, &candidate_lv))
            {
                uint8_t aidlen;

                if(aid < (uint8_t*)SECTOR_AID_NUM+EXTENDED_AID_NUM)
                {
                    return candidate_lv;
                }

                aidlen = aid[0] > candidate_lv[0] ? candidate_lv[0] : aid[0];
                if(aid[0] == candidate_lv[0] &&!memcmp(candidate_lv+1, aid+1, aidlen))
                {
                    if(!PBOC_TLVFindTag(&aid_storage.data[i][2], len, tag, &candidate_lv))
                    {
                        return candidate_lv;;
                    }
                }
            }
        }
    }
    #endif

	return 0;
}

uint8_t MPosGetCAPKIHook(uint8_t *rid, uint8_t idx, uint32_t tag, uint8_t **tag_lv)
{
    uint8_t i;
    uint32_t len;
    uint8_t *candidate_lv;

    uint8_t start_i = 0;

    if(TTAG_RID == tag)
        start_i = idx;

    for(i = start_i; i < SECTOR_CA_NUM; i++)
    {
        if(ca_key_storage.data[i][0] != 0xff) //not empty
        {
            len = ((ca_key_storage.data[i][0] & 0xf) << 8) + ca_key_storage.data[i][1];

            if(!PBOC_TLVFindTag(&ca_key_storage.data[i][2], len, TTAG_RID, &candidate_lv))
            {
                if(tag == TTAG_RID) //found rid only
                {
                    *tag_lv = candidate_lv;
                    return 0;
                }
                else if(!memcmp(candidate_lv+1, rid, 5)) //found tag belong rid
                {
                    if(!PBOC_TLVFindTag(&ca_key_storage.data[i][2], len, TTAG_CA_IDX, &candidate_lv))
                    {
                        if(tag == TTAG_CA_IDX) //found idx below rid
                        {
                            *tag_lv = candidate_lv;
                            return 0;
                        }
                        else if(idx == candidate_lv[1]) //found tag below (rid, idx)
                        {
                            if(!PBOC_TLVFindTag(&ca_key_storage.data[i][2], len, tag, &candidate_lv))
                            {
                                *tag_lv = candidate_lv;
                                return 0;
                            }
                        }
                    }
                }
            }
        }
    }
	
    #ifdef PBOC_EXTENDED_STORAGE
    for(i = SECTOR_CA_NUM; i < SECTOR_CA_NUM+EXTENDED_CA_NUM; i++)
    {
        if(ca_key_storage.data[i][0] != 0xff) //not empty
        {
            len = ((ca_key_storage.data[i][0] & 0xf) << 8) + ca_key_storage.data[i][1];

            if(!PBOC_TLVFindTag(&ca_key_storage.data[i][2], len, TTAG_RID, &candidate_lv))
            {
                if(tag == TTAG_RID) //found rid only
                {
                    *tag_lv = candidate_lv;
                    return 0;
                }
                else if(!memcmp(candidate_lv+1, rid, 5)) //found tag belong rid
                {
                    if(!PBOC_TLVFindTag(&ca_key_storage.data[i][2], len, TTAG_CA_IDX, &candidate_lv))
                    {
                        if(tag == TTAG_CA_IDX) //found idx below rid
                        {
                            *tag_lv = candidate_lv;
                            return 0;
                        }
                        else if(idx == candidate_lv[1]) //found tag below (rid, idx)
                        {
                            if(!PBOC_TLVFindTag(&ca_key_storage.data[i][2], len, tag, &candidate_lv))
                            {
                                *tag_lv = candidate_lv;
                                return 0;
                            }
                        }
                    }
                }
            }
        }
    }
	#endif
	

	return 2;

}
};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define ATT_TERM_FIX "[TERMINAL_FIX]"
#define ATT_TERM_SPECIAL "[TERMINAL_SPECIAL]"
#define ATT_AID "[AID]"
#define ATT_CA "[CA]"
#define ATT_DICT "[DICTIONARY]"
#define ATT_TEST "[TEST_SETTING]"

BOOL CPBOC_card_testDlg::CheckStorageSetting()
{
    if(mode == MODE_PC) //PC
	{    	
	    FILE *fp_embeded, *fp_external;
		int size_embeded, size_external;

		fp_embeded = fopen("pboc_storage.bin", "rb");
		fp_external = fopen("pboc_storage_ext.bin", "rb");

		if(!fp_embeded || !fp_external)
		{
		    BYTE ff[512*50];

			memset(ff, 0xff, 512*50);
			fp_embeded = fopen("pboc_storage.bin", "wb+");
			fp_external = fopen("pboc_storage_ext.bin", "wb+");

			fwrite(ff, 1, 512*(SECTOR_AID_NUM+SECTOR_CA_NUM), fp_embeded);
			fwrite(ff, 1, 512*(EXTENDED_AID_NUM+EXTENDED_CA_NUM), fp_external);
	    	if(!fp_embeded || !fp_external)
	    	    return FALSE;
		}

		fseek(fp_embeded, 0, SEEK_END);
		fseek(fp_external, 0, SEEK_END);

		size_embeded = ftell(fp_embeded);
		size_external = ftell(fp_external);

		fclose(fp_embeded);
		fclose(fp_external);
	    
		if(size_embeded != 512*(SECTOR_AID_NUM+SECTOR_CA_NUM) ||
			size_external != 512 *(EXTENDED_AID_NUM+EXTENDED_CA_NUM))
		{
		    return FALSE;
		}
		
        return TRUE;
	}
	else //Q5
	{
	    return TRUE;
	}
}

BOOL CPBOC_card_testDlg::CheckConsistence()
{
    if(aid_num != file_aid_num)
	    return FALSE;

	if(ca_num != file_ca_num)
		return FALSE;

	for(int i = 0; i < aid_num; i++)
	{
	    if((aid_bltlv[i][0] << 8) + aid_bltlv[i][1] != (file_aid_bltlv[i][0] << 8) + file_aid_bltlv[i][1])
			return FALSE;
	    if(memcmp(&aid_bltlv[i][2], &file_aid_bltlv[i][2], (aid_bltlv[i][0]<<8) + aid_bltlv[i][1]))
			return FALSE;
	}

	for(int i = 0; i < ca_num; i++)
	{
	    if((ca_bltlv[i][0] << 8) + ca_bltlv[i][1] != (file_ca_bltlv[i][0] << 8) + file_ca_bltlv[i][1])
			return FALSE;
	    if(memcmp(&ca_bltlv[i][2], &file_ca_bltlv[i][2], (ca_bltlv[i][0]<<8) + ca_bltlv[i][1]))
			return FALSE;
	}


	//IF PC, save data into fake storage
   int i;
#ifdef PBOC_EXTENDED_STORAGE
    for(i = 0; i < SECTOR_CA_NUM+EXTENDED_CA_NUM ; i++)
#else
    for(i = 0; i < SECTOR_CA_NUM ; i++)
#endif        
		memset(ca_key_storage.data[i], 0xff, 512);

#ifdef PBOC_EXTENDED_STORAGE
    for(i = 0; i < SECTOR_AID_NUM+EXTENDED_AID_NUM ; i++)
#else
    for(i = 0; i < SECTOR_AID_NUM ; i++)
#endif
        memset(aid_storage.data[i], 0xff, 512);

/**********************************************************************************/	
//Save CA infomation	
    for(int i = 0; i < ca_num; i++)
	{
	    Storage_SetCAPKI(ca_bltlv[i]);
	}
	
/**********************************************************************************/	

/**********************************************************************************/	
//Save AID information
    for(int i = 0; i < aid_num; i++)
	{
	    Storage_SetAID(aid_bltlv[i]);
	}

/**********************************************************************************/		

	return TRUE;
}


#define AID_END_CHECK 0xA2
#define CA_END_CHECK 0xA4


BOOL CPBOC_card_testDlg::WriteStorageSetting(BYTE aid[16][256], BYTE aidnum)
{
    return TRUE;
}

void CPBOC_card_testDlg::OverWriteStorageSetting()
{
    int sector_idx;
	BYTE sector_buf[512];
	if(mode == MODE_PC)
	{
	    FILE *fp_embeded, *fp_extened;
		fp_embeded = fopen("pboc_storage.bin", "r+b"); //w+ will truncated
		fp_extened = fopen("pboc_storage_ext.bin", "r+b");

        //AID Write back to storage		
	    for(sector_idx = 0; sector_idx < SECTOR_AID_NUM+EXTENDED_AID_NUM; sector_idx++)
		{
	   	    memset(sector_buf, 0xff, 512); //erase

			if(sector_idx < file_aid_num)
			{
			    memcpy(sector_buf, file_aid_bltlv[sector_idx], 2 + (file_aid_bltlv[sector_idx][0] << 8) + file_aid_bltlv[sector_idx][1]);  
				sector_buf[0] |= 0xf0;
				sector_buf[511] = AID_END_CHECK;
			}

		    if(sector_idx < SECTOR_AID_NUM) /*embeded storage*/
	    	{
	    	    fseek(fp_embeded, 512 * sector_idx, SEEK_SET);
				fwrite(sector_buf, 1, 512, fp_embeded);
			}
			else /*extened storage*/
			{
	    	    fseek(fp_embeded, 512 * (sector_idx - SECTOR_AID_NUM), SEEK_SET);
				fwrite(sector_buf, 1, 512, fp_extened);
			}
		}

        //CA Write back to storage		
	    for(sector_idx = 0; sector_idx < SECTOR_CA_NUM + EXTENDED_CA_NUM; sector_idx++)
		{
		  
	   	    memset(sector_buf, 0xff, 512); //erase

			if(sector_idx < file_ca_num)
			{
			    memcpy(sector_buf, file_ca_bltlv[sector_idx], 2 + (file_ca_bltlv[sector_idx][0] << 8) + file_ca_bltlv[sector_idx][1]);  
				sector_buf[0] |= 0xf0;
				sector_buf[511] = CA_END_CHECK;
			}

		    if(sector_idx < SECTOR_CA_NUM) /*embeded storage*/
	    	{
	    	    fseek(fp_embeded, 512 * (SECTOR_AID_NUM + sector_idx), SEEK_SET);
				fwrite(sector_buf, 1, 512, fp_embeded);
			}
			else /*extened storage*/
			{
	    	    fseek(fp_embeded, 512 * (sector_idx - SECTOR_CA_NUM + EXTENDED_AID_NUM), SEEK_SET);
				fwrite(sector_buf, 1, 512, fp_extened);
			}
		}

		fclose(fp_embeded);
		fclose(fp_extened);
	}
	else
	{
	    uint16_t resp;
		uint8_t resp_dat[1024];
		uint8_t session_key[16];
		uint8_t *uid;
		uint8_t *random_prim;
		uint8_t parm[512+12];
		CString str_sector;
		CString str_result;
		uint16_t len;
	    wchar_t *dat;

		MposSendCmd(0, 0, CMD_OP_VENDOR_READ_UID, 0, resp, resp_dat, 1024);
		uid = resp_dat;
		random_prim = resp_dat + 16;

		Mpos_DeriveSessionKey(uid, random_prim, session_key);

		Mpos_3DESEnc(session_key, (uint8_t*)MPOS_SIGNATURE, parm);

        //Write AID 
		for(sector_idx = 0; sector_idx < SECTOR_AID_NUM + EXTENDED_AID_NUM; sector_idx++)
		{
	   	    memset(parm+12, 0xff, 512); //erase

			if(sector_idx < file_aid_num)
			{
			    memcpy(parm+12, file_aid_bltlv[sector_idx], 2 + (file_aid_bltlv[sector_idx][0] << 8) + file_aid_bltlv[sector_idx][1]);  
				parm[12+0] |= 0xf0;
				parm[12+511] = AID_END_CHECK;
			}
			
            if(sector_idx < SECTOR_AID_NUM)
    		    parm[8] = sector_idx + 2; //0 is config, 1 is key
    		else
				parm[8] = sector_idx + 2 + SECTOR_CA_NUM; //0 is config, 1 is key
    		len = MposSendCmd(parm, 512+12, CMD_OP_VENDOR_WRITE_FLASH, 0, resp, resp_dat, 1024);	    

			if(resp != RES_OP_OK)
				TRACE("SHIT!!\n\n");

		}			

        //Write CA
		for(sector_idx = 0; sector_idx < SECTOR_CA_NUM + EXTENDED_CA_NUM; sector_idx++)
		{
	   	    memset(parm+12, 0xff, 512); //erase

			if(sector_idx < file_ca_num)
			{
			    memcpy(parm+12, file_ca_bltlv[sector_idx], 2 + (file_ca_bltlv[sector_idx][0] << 8) + file_ca_bltlv[sector_idx][1]);  
				parm[12+0] |= 0xf0;
				parm[12+511] = CA_END_CHECK;
			}
			
            if(sector_idx < SECTOR_CA_NUM)
    		    parm[8] = sector_idx + 2 + SECTOR_AID_NUM; //0 is config, 1 is key
    		else
				parm[8] = sector_idx + 2 + SECTOR_AID_NUM + EXTENDED_AID_NUM; //0 is config, 1 is key
    		len = MposSendCmd(parm, 512+12, CMD_OP_VENDOR_WRITE_FLASH, 0, resp, resp_dat, 1024);	    

			if(resp != RES_OP_OK)
				TRACE("SHIT!!\n\n");

		}			

	}
}

BOOL CPBOC_card_testDlg::ReadStorageSetting()
{
	BYTE sector_buf[512];
	BYTE sector_idx;

    if(mode == MODE_PC)
	{
	    FILE *fp_embeded, *fp_extened;
		
		fp_embeded = fopen("pboc_storage.bin", "rb");
    	fp_extened = fopen("pboc_storage_ext.bin", "rb");
			
		if(!fp_extened)
			return FALSE;

		if(!fp_embeded)
			return FALSE;
        //////////////////////////////////////AID//////////////////////////////////////////////////		
	    aid_num = 0;

		//read BASIC storage
		for(sector_idx = 0; sector_idx < SECTOR_AID_NUM; sector_idx++)
		{
			fread(sector_buf, 1, 512, fp_embeded);

		    if(sector_buf[0] != 0xff && sector_buf[511] == AID_END_CHECK)
			{
			    sector_buf[0] &= 0x0f;
			    memcpy(aid_bltlv[aid_num++], sector_buf, 2 + (sector_buf[0] << 8) + sector_buf[1]);
			}
		}	

	    //read EXTENDED storage
		for(sector_idx = 0; sector_idx < EXTENDED_AID_NUM; sector_idx++)
		{
			fread(sector_buf, 1, 512, fp_extened);

		    if(sector_buf[0] != 0xff && sector_buf[511] == AID_END_CHECK)
			{
			    sector_buf[0] &= 0x0f;
			    memcpy(aid_bltlv[aid_num++], sector_buf, 2+(sector_buf[0] << 8) + sector_buf[1]);
			}
		}					
        //////////////////////////////////////CA//////////////////////////////////////////////////		
	    ca_num = 0;

		//read BASIC storage
		fseek(fp_embeded, 512 * SECTOR_AID_NUM, SEEK_SET);
		for(sector_idx = 0; sector_idx < SECTOR_CA_NUM; sector_idx++)
		{
			fread(sector_buf, 1, 512, fp_embeded);

		    if(sector_buf[0] != 0xff && sector_buf[511] == CA_END_CHECK)
			{
			    sector_buf[0] &= 0x0f;
			    memcpy(ca_bltlv[ca_num++], sector_buf, 2 + (sector_buf[0] << 8) + sector_buf[1]);
			}
		}	

	    //read EXTENDED storage
		fseek(fp_extened, 512 * EXTENDED_AID_NUM, SEEK_SET);
		for(sector_idx = 0; sector_idx < EXTENDED_CA_NUM; sector_idx++)
		{
			fread(sector_buf, 1, 512, fp_extened);

		    if(sector_buf[0] != 0xff && sector_buf[511] == CA_END_CHECK)
			{
			    sector_buf[0] &= 0x0f;
			    memcpy(ca_bltlv[ca_num++], sector_buf, 2+(sector_buf[0] << 8) + sector_buf[1]);
			}
		}					
        
		fclose(fp_embeded);
		fclose(fp_extened);

	    return TRUE;
	}
	else
	{
	    uint16_t resp;
		uint8_t resp_dat[1024];
		uint8_t session_key[16];
		uint8_t *uid;
		uint8_t *random_prim;
		uint8_t parm[13];
		CString str_sector;
		CString str_result;
		uint16_t len;
	    wchar_t *dat;

		MposSendCmd(0, 0, CMD_OP_VENDOR_READ_UID, 0, resp, resp_dat, 1024);
		uid = resp_dat;
		random_prim = resp_dat + 16;

		Mpos_DeriveSessionKey(uid, random_prim, session_key);

		Mpos_3DESEnc(session_key, (uint8_t*)MPOS_SIGNATURE, parm);


		parm[9] = 0; //offset 0
		parm[10] = 0;
		parm[11] = 0x2; //length 0x200
		parm[12] = 0;

        //read AID from BASIC
	    aid_num = 0;
		for(sector_idx = 0; sector_idx < SECTOR_AID_NUM; sector_idx++)
		{
    		parm[8] = sector_idx + 2; //0 is config, 1 is key
    		len = MposSendCmd(parm, 13, CMD_OP_VENDOR_READ_FLASH, 0, resp, sector_buf, 1024);	    

		    if(sector_buf[0] != 0xff && sector_buf[511] == AID_END_CHECK)
			{
			    sector_buf[0] &= 0x0f;
			    memcpy(aid_bltlv[aid_num++], sector_buf, 2 + (sector_buf[0] << 8) + sector_buf[1]);
			}
		}	

		//read AID from EXTENDED
		for(sector_idx = 0; sector_idx < EXTENDED_AID_NUM; sector_idx++)
		{
    		parm[8] = sector_idx + 2 + SECTOR_AID_NUM + SECTOR_CA_NUM;
    		len = MposSendCmd(parm, 13, CMD_OP_VENDOR_READ_FLASH, 0, resp, sector_buf, 1024);	    

		    if(sector_buf[0] != 0xff && sector_buf[511] == AID_END_CHECK)
			{
			    sector_buf[0] &= 0x0f;
			    memcpy(aid_bltlv[aid_num++], sector_buf, 2+(sector_buf[0] << 8) + sector_buf[1]);
			}
		}					


        //read CA from BASIC
	    ca_num = 0;
		for(sector_idx = 0; sector_idx < SECTOR_CA_NUM; sector_idx++)
		{
    		parm[8] = sector_idx + 2 + SECTOR_AID_NUM; //0 is config, 1 is key
    		len = MposSendCmd(parm, 13, CMD_OP_VENDOR_READ_FLASH, 0, resp, sector_buf, 1024);	    

		    if(sector_buf[0] != 0xff && sector_buf[511] == CA_END_CHECK)
			{
			    sector_buf[0] &= 0x0f;
			    memcpy(ca_bltlv[ca_num++], sector_buf, 2 + (sector_buf[0] << 8) + sector_buf[1]);
			}
		}	

		//read AID from EXTENDED
		for(sector_idx = 0; sector_idx < EXTENDED_CA_NUM; sector_idx++)
		{
    		parm[8] = sector_idx + 2 + SECTOR_AID_NUM + SECTOR_CA_NUM + EXTENDED_AID_NUM;
    		len = MposSendCmd(parm, 13, CMD_OP_VENDOR_READ_FLASH, 0, resp, sector_buf, 1024);	    

		    if(sector_buf[0] != 0xff && sector_buf[511] == CA_END_CHECK)
			{
			    sector_buf[0] &= 0x0f;
			    memcpy(ca_bltlv[ca_num++], sector_buf, 2+(sector_buf[0] << 8) + sector_buf[1]);
			}
		}					

		return TRUE;
	}
}

uint8_t UTF82UCS2(uint8_t *utf8, uint8_t *ucs2, uint8_t len)
{
    
    uint8_t *src, *tgt;
    src = utf8;
    tgt = ucs2;

    while(len)
    {
        if((*src & 0xF0) == 0xE0) //0x800 ~ 0x10000
        {
            if(len > 2)
            {
                *tgt++ = ((src[1] & 0x3) << 6) | (src[2] & 0x3f);
                *tgt++ = ((src[0] & 0xf) << 4) | ((src[1] >> 2) & 0xf);
                src += 3;
                len -= 3;
            }
            else
                len = 0;
        }
        else if((*src & 0xE0) == 0xC0) //0x80 ~ 0x800
        {
            if(len > 1)
            {
                *tgt++ = ((src[0] & 0x3) << 6) | (src[1] & 0x3f);
                *tgt++ = ((src[0] >> 2) & 0x7);
                src += 2;
                len -=2;
            }
            else
                len = 0;
        }
        else if(*src < 0x80)
        {
            *tgt++ = src[0] & 0x7f;
            *tgt++ = 0x00;
            src += 1;
            len -= 1;
        }
        else
        {
            break;
        }
    }

    *tgt++ = 0x00; *tgt++ = 0x00;
	
    return tgt - ucs2;

}

BOOL IsX(char *str)
{
    for (int i = 0; i < strlen(str); i++)
    { 
        if(!isxdigit(str[i]))
        {
             return FALSE;         
		}
    }
    return TRUE;
}

//0: success 1: fail 2: header
BYTE ParseTagAssign(char *string, char *seps, char *ltlv)
{
    char *tag;
	char *value;
//check header
if(!memcmp(string, ATT_TERM_FIX, sizeof(ATT_TERM_FIX) - 1) ||
   !memcmp(string, ATT_TERM_SPECIAL, sizeof(ATT_TERM_SPECIAL) - 1) ||
   !memcmp(string, ATT_AID, sizeof(ATT_AID) - 1) ||
   !memcmp(string, ATT_CA, sizeof(ATT_CA) - 1) ||
   !memcmp(string, ATT_TEST, sizeof(ATT_TEST) - 1) ||   
   !memcmp(string, ATT_DICT, sizeof(ATT_DICT) - 1))
{
    return 2;
}

	
	
   tag = strtok( string, seps ); // C4996
   // Note: strtok is deprecated; consider using strtok_s instead
   if( tag != NULL && IsX(tag))
   {
      // Get next token: 
      value = strtok( NULL, seps ); // C4996
	  if(value == NULL || !IsX(value))
		  return 1;
      strupr(tag);
	  strupr(value);

	  sprintf(ltlv, "%04d%s%02x%s", (strlen(tag)+strlen(value))/2 + 1, tag, strlen(value)/2, value);
	  return 0;
   }

   return 1;
}

//0: success 1: fail 2: header
BYTE ParseTest(char *string, char *seps, char ltlv[5][512])
{
    char *tname, *tag[10];
	char *values, *value[10];
	int tag_num = 0;
	int value_num = 0;
	int total_len = 0;
//check header
if(!memcmp(string, ATT_TERM_FIX, sizeof(ATT_TERM_FIX) - 1) ||
   !memcmp(string, ATT_TERM_SPECIAL, sizeof(ATT_TERM_SPECIAL) - 1) ||
   !memcmp(string, ATT_AID, sizeof(ATT_AID) - 1) ||
   !memcmp(string, ATT_CA, sizeof(ATT_CA) - 1) ||
   !memcmp(string, ATT_TEST, sizeof(ATT_TEST) - 1) ||
   !memcmp(string, ATT_DICT, sizeof(ATT_DICT) - 1))
	{
	    return 2;
	}

		
   tname = strtok( string, seps ); // C4996
   // Note: strtok is deprecated; consider using strtok_s instead
   if(tname != NULL)
   {
		// Get next token: 
		values = strtok( NULL, seps ); // C4996
     

		value[0] = strtok(values, ", ");
		value[1] = strtok(NULL, ", ");
		value[2] = strtok(NULL, ", ");
		value[3] = strtok(NULL, ", ");


		strupr(value[0]);
		strupr(value[1]);
		strupr(value[2]);
		strupr(value[3]);

        if(!value[0] || !value[1] || !value[2] ||!value[3])
			return 1;

		strcpy(ltlv[0], tname);
		strcpy(ltlv[1], value[0]);
		strcpy(ltlv[2], value[1]);
        strcpy(ltlv[3], value[2]);
        strcpy(ltlv[4], value[3]);

    	for(int i = 0; i < 4; i++)
		{
    		if(!strcmp(ltlv[i], "NONE"))
				strcpy(ltlv[i], "");
		}

        if(!strcmp(ltlv[1], "DEFAULT"))
			strcpy(ltlv[1], "3030");

        if(!strcmp(ltlv[2], "DEFAULT"))
			strcpy(ltlv[2], "01020102010201023030");

		if(!strcmp(ltlv[3], "DEFAULT"))
			strcpy(ltlv[3], "000000000000");

		if(!strcmp(ltlv[4], "DEFAULT"))
			ltlv[4][0] = 0;



       /*
		for(int i = 0; i < value_num; i++)
		{
			strupr(tag[i]);
			strupr(value[i]);
			total_len += strlen(tag[i])/2;
		    total_len += strlen(value[i])/2;
			total_len += strlen(value[i])/2 < 0x80 ? 1 : 2;
		}

		sprintf(ltlv, "%04d", total_len);
		for(int i = 0; i < value_num; i++)
		{
		    if(strlen(value[i])/2 < 0x80)
	     	    sprintf(ltlv, "%s%s%02x%s", ltlv, tag[i], strlen(value[i])/2, value[i]);
			else
	     	    sprintf(ltlv, "%s%s81%02x%s", ltlv, tag[i], strlen(value[i])/2, value[i]);		
		}*/


	  return 0;
   }

   return 1;
}
//0: success 1: fail 2: header
BYTE ParseTagsAssign(char *string, char *seps, char *ltlv)
{
    char *tags, *tag[10];
	char *values, *value[10];
	int tag_num = 0;
	int value_num = 0;
	int total_len = 0;
//check header
if(!memcmp(string, ATT_TERM_FIX, sizeof(ATT_TERM_FIX) - 1) ||
   !memcmp(string, ATT_TERM_SPECIAL, sizeof(ATT_TERM_SPECIAL) - 1) ||
   !memcmp(string, ATT_AID, sizeof(ATT_AID) - 1) ||
   !memcmp(string, ATT_CA, sizeof(ATT_CA) - 1) ||
   !memcmp(string, ATT_TEST, sizeof(ATT_TEST) - 1) ||
   !memcmp(string, ATT_DICT, sizeof(ATT_DICT) - 1))
	{
	    return 2;
	}

		
   tags = strtok( string, seps ); // C4996
   // Note: strtok is deprecated; consider using strtok_s instead
   if(tags != NULL)
   {
		// Get next token: 
		values = strtok( NULL, seps ); // C4996
     
		tag[tag_num] = strtok(tags, ", ");

		while(tag[tag_num])
		{
		    tag_num++;
			tag[tag_num] = strtok(NULL, ", ");
		}

		value[value_num] = strtok(values, ", ");

		while(value[value_num])
		{
		    value_num++;
			value[value_num] = strtok(NULL, ", ");
		}

        if(value_num != tag_num)
			return 1;

		for(int i = 0; i < value_num; i++)
		{
			strupr(tag[i]);
			strupr(value[i]);
			total_len += strlen(tag[i])/2;
		    total_len += strlen(value[i])/2;
			total_len += strlen(value[i])/2 < 0x80 ? 1 : 2;
		}

		sprintf(ltlv, "%04d", total_len);
		for(int i = 0; i < value_num; i++)
		{
		    if(strlen(value[i])/2 < 0x80)
	     	    sprintf(ltlv, "%s%s%02x%s", ltlv, tag[i], strlen(value[i])/2, value[i]);
			else
	     	    sprintf(ltlv, "%s%s81%02x%s", ltlv, tag[i], strlen(value[i])/2, value[i]);		
		}


	  return 0;
   }

   return 1;
}


BOOL CPBOC_card_testDlg::ReadFileSetting()
{
	FILE *fp;
	char att_type[800];
	char att_ltlv[800];
    char string[800];

	file_aid_num = 0;
	file_ca_num = 0;
	file_term_num = 0;
	file_term_special_num = 0;
	file_dict_num = 0;
	file_test_num = 0;
	
	fp = fopen("pboc.ini", "r");

    if(!fp)
	{
		MessageBox(TEXT("Open pboc.ini Fail!"));
		exit(0);
	}

    while(1)
	{
	    int ret;
		BYTE bret;
	
		if(!fgets(att_type, 800, fp))
			goto FileEnd;
check_head:		
		if(!memcmp(att_type, ATT_TERM_FIX, sizeof(ATT_TERM_FIX) - 1))
		{
		    while(fgets(att_type, 800, fp))
	    	{
				bret = ParseTagAssign(att_type, " =;", att_ltlv);
				if(!bret)
				{
					if(!pack_ltlv_hex(att_ltlv, file_term_bltlv[file_term_num++]))
						goto ReadFailEnd;
				}
				else if(bret == 2)
					goto check_head;
	    	}
		}

		if(!memcmp(att_type, ATT_TERM_SPECIAL, sizeof(ATT_TERM_SPECIAL) - 1))
		{
		    while(fgets(att_type, 800, fp))
	    	{
				bret = ParseTagAssign(att_type, " =;", att_ltlv);
				if(!bret)
				{
					if(!pack_ltlv_hex(att_ltlv, file_term_special_bltlv[file_term_special_num++]))
						goto ReadFailEnd;
				}
				else if(bret == 2)
					goto check_head;
	    	}
		}

		if(!memcmp(att_type, ATT_AID, sizeof(ATT_AID) - 1))
		{
		    while(fgets(att_type, 800, fp))
	    	{
				bret = ParseTagsAssign(att_type, "=;", att_ltlv);
				if(!bret)
				{
					if(file_aid_num < SECTOR_AID_NUM+EXTENDED_AID_NUM)
						if(!pack_ltlv_hex(att_ltlv, file_aid_bltlv[file_aid_num++]))
							goto ReadFailEnd;
				}
				else if(bret == 2)
					goto check_head;
	    	}
/*
		    ret = fscanf(fp, "%s", att_ltlv);

			if(file_aid_num < SECTOR_AID_NUM+EXTENDED_AID_NUM)
				if(!pack_ltlv_hex(att_ltlv, file_aid_bltlv[file_aid_num++]))
					goto ReadFailEnd;*/
		}

		if(!memcmp(att_type, ATT_CA, sizeof(ATT_CA) - 1))
		{
		    while(fgets(att_type, 800, fp))
	    	{
				bret = ParseTagsAssign(att_type, "=;", att_ltlv);
				if(!bret)
				{
					if(file_ca_num < SECTOR_CA_NUM+EXTENDED_CA_NUM)
						if(!pack_ltlv_hex(att_ltlv, file_ca_bltlv[file_ca_num++]))
							goto ReadFailEnd;
				}
				else if(bret == 2)
					goto check_head;
	    	}

		}

		if(!memcmp(att_type, ATT_TEST, sizeof(ATT_TEST) - 1))
		{
		    while(fgets(att_type, 800, fp))
	    	{

				if(file_test_num < 255)
				{
    				bret = ParseTest(att_type, "=;", (char[5][512])file_test_bltlvs[file_test_num]);
					if(!bret)
					{
						file_test_num++;
					}
					else if(bret == 2)
						goto check_head;
				}
	    	}

		}

		if(!memcmp(att_type, ATT_DICT, sizeof(ATT_DICT) - 1))
		{
		    char tag_num[20];
			char tag_name[20];
			
		    while(fscanf(fp, "%x", &file_dict[file_dict_num].tag))
	    	{		    
	    		uint8_t ucs2[800];
			    if(fscanf(fp, "%s", att_ltlv))
		    	{			    
			        UTF82UCS2((uint8_t*)att_ltlv, ucs2, strlen(att_ltlv));
					file_dict[file_dict_num].name.Format(TEXT("%s"), ucs2);//ucs2;
					file_dict_num++;
		    	}
	    	}

		}

	}




	//fclose(fp);
	//return TRUE;
ReadFailEnd:
	fclose(fp);
	return FALSE;
FileEnd:
	fclose(fp);
///merge special to aid, because new-land's arch.
for(int i = 0; i < file_aid_num ;i++)
{
    uint32_t len, slen;
    len = (file_aid_bltlv[i][0] << 8) + file_aid_bltlv[i][1];

	for(int j = 0; j < file_term_special_num; j++)
	{
	    slen = (file_term_special_bltlv[j][0] << 8) + file_term_special_bltlv[j][1];

		memcpy(&file_aid_bltlv[i][len+2], &file_term_special_bltlv[j][2], slen);
		len += slen;
	}
	file_aid_bltlv[i][0] = (len >> 8) & 0xff;
	file_aid_bltlv[i][1] = len & 0xff;
}	
	return TRUE;
}


DWORD CPBOC_card_testDlg::GetEditValue(CEdit &edit, BYTE *buf)
{
	TCHAR tmp_t[1024];
	int len;
	
	edit.GetWindowText(tmp_t, 1024);
	len = edit.GetWindowTextLength();		
	if(buf != 0)
		Text2Bin(tmp_t, len, (char*)buf);
	return len/2;
}

void CPBOC_card_testDlg::AssmTLV(BYTE *tlvalue, BYTE tlvaluelen, BYTE tlv[1024], DWORD &tlvidx)
{
	memcpy(tlv+tlvidx, tlvalue, tlvaluelen);
	tlvidx += tlvaluelen;
}

void CPBOC_card_testDlg::AssmTLV(BYTE *value, BYTE vallen, DWORD tag, BYTE taglen, BYTE tlv[1024], DWORD &tlvidx)
{
    DWORD len;
	
    //T
    if(taglen == 2)
	{
		tlv[tlvidx++] = (tag >> 8) & 0xff;
		tlv[tlvidx++] = tag & 0xff;
	}
	else
		tlv[tlvidx++] = tag & 0xff;

    //L
	len = vallen;
	
	if(len > 0x80)
	{
		tlv[tlvidx++] = 0x81;
	}

    tlv[tlvidx++] = len;

	//V, haha
	memcpy(tlv+tlvidx, value, len);
	tlvidx += len;
}

void CPBOC_card_testDlg::AssmTLV(CEdit &edit, DWORD tag, BYTE taglen, BYTE tlv[1024], DWORD &tlvidx)
{
    DWORD len;
	
    //T
    if(taglen == 2)
	{
		tlv[tlvidx++] = (tag >> 8) & 0xff;
		tlv[tlvidx++] = tag & 0xff;
	}
	else
		tlv[tlvidx++] = tag & 0xff;

    //L
	len = GetEditValue(edit, (BYTE*)0);
	
	if(len > 0x80)
	{
		tlv[tlvidx++] = 0x81;
	}

    tlv[tlvidx++] = len;

	//V, haha
	tlvidx += GetEditValue(edit, (BYTE*)tlv+tlvidx);

}

void CPBOC_card_testDlg::InitPBOC()
{
	uint8_t *tlv;
	uint32_t tlvlen;
	uint8_t *lv;
	uint32_t tag, i;
    if(mode == MODE_PC) //PC
	{
	    /*Prepare transport*/
		PBOC_InitVar(DoAPDU_p);    	

	    /*Prepare terminal data*/
		PBOC_InitTermData(&pboc_term_data);
		PBOC_InitCardData(&pboc_card_files);

        for(i = 0; i < file_term_num; i++)
    	{
	        tlvlen = (file_term_bltlv[i][0] << 8) + file_term_bltlv[i][1];
			tlv = &file_term_bltlv[i][2];
			
	        tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv);

    		PBOC_AddTermFixData(&pboc_term_data, &lv[1], lv[0], tag, (tag & 0xff00) ? 2 : 1);
    	}
		
	}		
	else //Q5
	{

	    uint16_t resp;
		uint8_t result[1024];
		uint32_t len;
	    uint8_t parm[50];

        AdjustTime();
		InitRandom(); //because we-are only init internally on bt_connected;
		
		parm[0] = 0; //delete fix term
	    MposSendCmd(parm, 1, CMD_OP_VENDOR_PBOC_TERM, 0, resp, result, 1024, 0, 0);

        for(i = 0; i < file_term_num; i++)
    	{			
	        tlvlen = (file_term_bltlv[i][0] << 8) + file_term_bltlv[i][1];
			tlv = &file_term_bltlv[i][2];
			
	        tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv);

            //workaround for Reversal report
			if(tag == TTAG_TERMINAL_TYPE)
			{
				if(lv && lv[0]==1)
				{
				   terminal_type = lv[1];
				}
			}

			parm[0] = 1; //add fix term
			parm[1] = (tag & 0xff00) ? 2 : 1;
			parm[2] = (tag & 0xff00) >> 8;
			parm[3] = tag & 0xff;
			parm[4] = lv[0];
			memcpy(parm+5, lv+1, lv[0]);
		    MposSendCmd(parm, 5 + lv[0], CMD_OP_VENDOR_PBOC_TERM, 0, resp, result, 1024, 0, 0);
    	}


	}
}


void Hex2BCD4byte(uint32_t len, uint8_t *bcd)
{
    bcd[3] = (len % 10);
    len = len / 10;
    bcd[3] |= ((len % 10) << 4);
    len = len / 10;

    bcd[2] = (len % 10);
    len = len / 10;
    bcd[2] |= ((len % 10) << 4);
    len = len / 10;

    bcd[1] = (len % 10);
    len = len / 10;
    bcd[1] |= ((len % 10) << 4);
    len = len / 10;

    bcd[0] = (len % 10);
    len = len / 10;
    bcd[0] |= ((len % 10) << 4);
    len = len / 10;
}

//fake command input
BYTE transaction_data[640];
BYTE issuer_data[640];

void FlowInd(uint8_t ind)
{
    switch(ind)
	{
		case 0:
			ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T("Selecting APP..."));
			break;
		case 1:
			ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T("Initialing APP..."));
			break;
		case 2:
			ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T("Read APP data..."));
			break;
		case 3:
			ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T("Data Autheticating..."));
			break;
		case 4:
			ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T("Process Restriction..."));
			break;
		case 5:
			ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T("CVM..."));
			break;
		case 6:
			ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T("TRM..."));
			break;
		case 7:
			ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T("Terminal Behavior Analysis..."));
			break;
		case 8:
			ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(_T("Card Behavior Analysis..."));
			break;
	}
}

void InfoRecv(uint16_t len, uint8_t *dat)
{
   FlowInd(dat[0]);
}

void FlowFinish(uint8_t* resp, uint32_t resp_len)
{
    uint8_t *df75, *df37, *a9f37;
	uint8_t unpredict[8];

	ThisIsMe->KillTimer(SECOND_TIMER);
    PBOC_TLVFindTag(resp+2, resp_len-2, 0xDF75, &df75);
    PBOC_TLVFindTag(resp+2, resp_len-2, 0x9F37, &a9f37);

	if(a9f37)
	{
	    memcpy(unpredict, a9f37+1, 4);
        memcpy(unpredict+4, a9f37+1, 4);
	}
	else
	{
	    memset(unpredict, 0, 8);
	}

    if(df75) //連機 || 收受
	{
	    if((df75[1] == 0x03 || df75[1] == 0x01))
    	{			
		    PBOC_TLVFindTag(resp+2, resp_len-2, 0xDF37, &df37);

            if(df37)
        	{
				ThisIsMe->Pin.ResetPin(ThisIsMe->online_pin+1);
				ThisIsMe->Pin.DoModal();

				if(ThisIsMe->online_pin[1])
				{
					Mpos_3DESEnc(unpredict, &ThisIsMe->online_pin[1], &ThisIsMe->online_pin[1]);
					ThisIsMe->online_pin[0] = 8;
				}
				else
					ThisIsMe->online_pin[0] = 0;
        	}
			else
				ThisIsMe->online_pin[0] = 0;

			ThisIsMe->network_ok = ThisIsMe->Issuer->IsAble2Online();
			
			if(ThisIsMe->network_ok)
			{
    		    ThisIsMe->Issuer->TransactionRequest(resp, resp_len, ThisIsMe->online_pin);

    			ThisIsMe->Issuer->TransactionResponse(ThisIsMe->arclv, ThisIsMe->authcode, ThisIsMe->issauthdata, &ThisIsMe->issauthlen, ThisIsMe->script, &ThisIsMe->script_len);

			    if(ThisIsMe->arclv[0] != 2)
        		    ThisIsMe->Issuer->TransactionRequest(resp, resp_len, ThisIsMe->online_pin, TRUE); //retry

	    	    ThisIsMe->PBOC2nd(ThisIsMe->arclv, ThisIsMe->authcode, ThisIsMe->issauthdata, ThisIsMe->issauthlen, ThisIsMe->script, ThisIsMe->script_len);
			}
			else
			{
    		    ThisIsMe->Issuer->TransactionRequest(resp, resp_len, ThisIsMe->online_pin);
    		    ThisIsMe->Issuer->TransactionRequest(resp, resp_len, ThisIsMe->online_pin, TRUE);//retry

				ThisIsMe->PBOC2nd(0, 0, 0, 0, 0, 0);
			}

			return;
		}
		else
		{						
		    CString str;
			Beep( 750*2, 300/2 );
			Beep( 750*2, 300/2 );
			Beep( 750*2, 300/2 );
			Beep( 750*2, 300/2 );
			ThisIsMe->Go2State(PBOC_EXECUTE_END);
			ThisIsMe->m_pboc_result.SetTextColor(LIGHTRED);

			if(df75[1] == 0x02) //AAC, 拒絕
			{
				str.LoadStringW(IDS_STRING_TRANSACTION_DECLINE);
				str += TEXT("!!");

//				ThisIsMe->m_pboc_result.SetWindowTextW(_T("Transaction DECLINE!!"));
				ThisIsMe->m_pboc_result.SetWindowTextW(str);
			}
			else if(df75[1] == 0xFE) //FALLBACK, TERMINATE
			{
				str.LoadStringW(IDS_STRING_TRANSACTION_TERMINATE);
				str += TEXT("!!");
//		    	ThisIsMe->m_pboc_result.SetWindowTextW(_T("Transaction TERMINATE!!"));
				ThisIsMe->m_pboc_result.SetWindowTextW(str);
			}
			else if(df75[1] == 0xFF)
			{
				str.LoadStringW(IDS_STRING_TRANSACTION_TERMINATE_NOT_ACCEPT);
				str += TEXT("!!");
//		    	ThisIsMe->m_pboc_result.SetWindowTextW(_T("Transaction TERMINATE, Not Accepted!!"));
				ThisIsMe->m_pboc_result.SetWindowTextW(str);
			}
		}

	}
	else
	{
		Beep( 750*2, 300/2 );
		Beep( 750*2, 300/2 );
		Beep( 750*2, 300/2 );
		Beep( 750*2, 300/2 );

		ThisIsMe->Go2State(PBOC_EXECUTE_END);
		ThisIsMe->m_pboc_result.SetTextColor(LIGHTRED);
    	ThisIsMe->m_pboc_result.SetWindowTextW(_T("PBOC, Abort!"));

	}
end:
	ThisIsMe->PBOCEnd();

}


void Flow2ndFinish(uint8_t *resp, uint32_t resp_len)
{
    uint8_t *df75;
	int i;
	BYTE len;
	CString status_str;

    PBOC_TLVFindTag(resp+2, resp_len-2, 0xDF75, &df75);

    if(ThisIsMe->network_ok)
	{	
	    status_str += TEXT("================================================\r\n");

		len = ThisIsMe->arclv[0];
		status_str.Format(status_str+TEXT("Authentication Response Code(%X) :"), TTAG_ARC);
	    if(len != 2)
			status_str.Format(status_str+TEXT("(%02X)"), len);
		for(i = 0; i < len; i++)
		    if(isdigit(ThisIsMe->arclv[i+1]))
    	    	status_str.Format(status_str+TEXT("%02X"), ThisIsMe->arclv[i+1]);
	    if(len != 2)
			status_str.Format(status_str+TEXT(" (Invalid length)"));
		status_str.Format(status_str+TEXT("\r\n"));

		status_str.Format(status_str+TEXT("Authentication Code(%X) :"), TTAG_AUTH_CODE);
		for(i = 0; i < 6; i++)
	    	status_str.Format(status_str+TEXT("%02X"), ThisIsMe->authcode[i]);
		status_str.Format(status_str+TEXT("\r\n"));


		status_str.Format(status_str+TEXT("Issuer Authentication Data(%X) :"), TTAG_ISSUER_AUTH_DATA);
		for(i = 0; i < ThisIsMe->issauthlen; i++)
	    	status_str.Format(status_str+TEXT("%02X"), ThisIsMe->issauthdata[i]);
		status_str.Format(status_str+TEXT("\r\n"));

		status_str += TEXT("================================================\r\n");
	}
	
	Beep( 750*2, 300/2 );
	Beep( 750*2, 300/2 );
    if(df75 && (df75[1] == 0x01)) //連機 || 收受
	{
	    CString str;
		ThisIsMe->Go2State(PBOC_EXECUTE_END);
    	ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(status_str);
		ThisIsMe->m_pboc_result.SetTextColor(LIGHTGREEN);

		str.LoadStringW(IDS_STRING_TRANSACTION_SUCCESS);
		str += TEXT("!!");
		
    	//ThisIsMe->m_pboc_result.SetWindowTextW(_T("Transaction SUCCESS!!"));		
		ThisIsMe->m_pboc_result.SetWindowTextW(str);		
	    if(ThisIsMe->network_ok)
	     	ThisIsMe->Issuer->TransactionConfirm(resp, resp_len);

	}
	else if(df75 && (df75[1] == 0x04)) //2nd AAC, DECLINE
	{
	    CString str;
		Beep( 750*2, 300/2 );
		Beep( 750*2, 300/2 );

    	ThisIsMe->Go2State(PBOC_EXECUTE_END);
		ThisIsMe->GetDlgItem(IDC_STATIC_PBOC_RESULT)->SetWindowTextW(status_str);
		ThisIsMe->m_pboc_result.SetTextColor(LIGHTRED);

		str.LoadStringW(IDS_STRING_TRANSACTION_DECLINE);
		str += TEXT("!!");
//    	ThisIsMe->m_pboc_result.SetWindowTextW(_T("Transaction DECLINE!!"));		
		ThisIsMe->m_pboc_result.SetWindowTextW(str);
		
	    //if(ThisIsMe->network_ok) for PBOC lvl 2 data capture
    	{
    	    if((ThisIsMe->arclv[0] != 2) || !memcmp(&ThisIsMe->arclv[1], "00", 2) || !memcmp(&ThisIsMe->arclv[1], "10", 2) || !memcmp(&ThisIsMe->arclv[1], "11", 2)) //online approved   	       
	    	{
	    	    //workaround for reversal report
	    	    
		     	ThisIsMe->Issuer->TransactionReversal(resp, resp_len, terminal_type);
	    	}
    	}

	}
	else if(df75 && (df75[1] == 0x02))
	{
	    CString str;
		Beep( 750*2, 300/2 );
		Beep( 750*2, 300/2 );

    	ThisIsMe->Go2State(PBOC_EXECUTE_END);
		ThisIsMe->m_pboc_result.SetTextColor(LIGHTRED);
		str.LoadStringW(IDS_STRING_TRANSACTION_TERMINATE_NOT_ACCEPT);
		str += TEXT("!!");
		
//    	ThisIsMe->m_pboc_result.SetWindowTextW(_T("Transaction TERMINATE, NOT ACCEPTED!!"));		
		ThisIsMe->m_pboc_result.SetWindowTextW(str);
	}
	else
	{
	    CString str;
		Beep( 750*2, 300/2 );
		Beep( 750*2, 300/2 );

    	ThisIsMe->Go2State(PBOC_EXECUTE_END);
		ThisIsMe->m_pboc_result.SetTextColor(LIGHTRED);
		str.LoadStringW(IDS_STRING_TRANSACTION_TERMINATE);
		str += TEXT("!!");
		
//    	ThisIsMe->m_pboc_result.SetWindowTextW(_T("Transaction TERMINATE!!"));		
		ThisIsMe->m_pboc_result.SetWindowTextW(str);
	}

	
    ThisIsMe->PBOCEnd();

}

void CPBOC_card_testDlg::FlowSwipe()
{
    if(mode == MODE_PC)
	{
	    //nothing
	}
	else
	{
		try
		{
			CString tracks;
		    uint16_t resp;
			uint8_t parm[30];
			uint8_t resp_dat[1024];
			uint8_t session_key[8];
			uint8_t is_valid = 1;
			uint8_t sc[3];
			CString str;

	        tracks = TEXT("swipe fail, Again!!\n");
			do {
				// TODO: Add your control notification handler code here
				parm[0] = 0x01;
				parm[1] = 0x0a;
				MposSendCmd(parm, 2, CMD_OP_OPEN_DEVICE, 0, resp, resp_dat, 1024, 0);
				is_valid = 0;

				if(resp == RES_OP_OK)
			    {
			        uint16_t len;
		    		len = MposSendCmd(0, 0, CMD_OP_VENDOR_READ_TRACKS, 0, resp, resp_dat, 1024);
					for(int i = 0 ; i < len ; i++)
					{
					    if(resp_dat[i] == '=') //tracks 2 =
						{
							if(i + 5 >= len)
								break; //fail
							memcpy(sc, &resp_dat[i+5], 3);
							is_valid = 1;
							break;
						}
						
					}

					if(is_valid)            
					{
		    			tracks.Format(TEXT("%c%c%c\r\n"), sc[0], sc[1], sc[2]);
					}
					
		        }

	            if(is_valid)
	        	{
	        	    CString str1;
					ThisIsMe->m_pboc_result.SetTextColor(LIGHTBLUE);
					str.LoadStringW(IDS_STRING_SC_CODE);
					str.Format(str+TEXT("=%c%c%c\r\n"), sc[0], sc[1], sc[2]);
					
					if(sc[0] == 0x32 || sc[0] == 0x36)
					{
						str1.LoadStringW(IDS_STRING_PLEASE_INSERT_IC);
						str += str1;
						ThisIsMe->m_pboc_result.SetWindowTextW(str);
					}
					else
					{
						str1.LoadStringW(IDS_STRING_SWIPE_END);
						str += str1;
						ThisIsMe->m_pboc_result.SetWindowTextW(str);
					}
	        	}
				else
				{
					ThisIsMe->m_pboc_result.SetTextColor(LIGHTBLUE);
					str.LoadStringW(IDS_STRING_SWIPE_AGAIN);
					ThisIsMe->m_pboc_result.SetWindowTextW(str);
				}
			
			}while(!is_valid);

		}
		catch(CSerialException *e)
		{
        	ThisIsMe->KillTimer(SECOND_TIMER);
 			Go2State(PBOC_WAIT_READER_IN);
			return;
		}
		
	}

	ThisIsMe->KillTimer(SECOND_TIMER);
	Go2State(PBOC_WAIT_CARD_IN);
}

void CPBOC_card_testDlg::FlowPBOC()
{
	DWORD term_totallen;
	BYTE tmp[4];
    uint16_t len;
	
	/*transaction related*/
	term_totallen = 0;

    Hex2BCD4byte(tsc_9f41, tsc_9f41_bcd);
	tmp[0] = tsc_9f41_bcd[0] & 0xff;
	tmp[1] = tsc_9f41_bcd[1] & 0xff;
	tmp[2] = tsc_9f41_bcd[2] & 0xff;
	tmp[3] = tsc_9f41_bcd[3] & 0xff;

    //2CN.004.00.00 begin
    uint32_t amount_binary;
	uint8_t amount_binary_array[5];
    amount_binary = BCD6byte_2Number(amount_9f02);
    amount_binary_array[0] = amount_binary >> 24;
    amount_binary_array[1] = amount_binary >> 16;
    amount_binary_array[2] = amount_binary >> 8;
    amount_binary_array[3] = amount_binary >> 0;

	AssmTLV(amount_binary_array, 4, TTAG_AMOUNT_REF, 2, transaction_data+2, term_totallen); //Amount, Referency Currency
	AssmTLV((BYTE*)"\x00\x00\x00\x00\x00\x01", 6, TTAG_ACQUIRER_ID, 2, transaction_data+2, term_totallen);//Aquirer Identifier
	AssmTLV((BYTE*)"\x30\x31\x32\x33\x34\x35\x30\x31\x32\x33\x34\x35\x30\x31\x32\x33\x34\x35", 15, TTAG_MERCHANT_ID, 2, transaction_data+2, term_totallen);//Aquirer Identifier
	AssmTLV((BYTE*)"\x87\x34", 2, TTAG_MERCHANT_CATEGORY_CODE, 2, transaction_data+2, term_totallen);
	AssmTLV((BYTE*)"PBOC LVL2 Testing  ", 20, TTAG_MERCHANT_NAME, 2, transaction_data+2, term_totallen);
	AssmTLV((BYTE*)"\x21", 1, TTAG_POS_ENTRY, 2, transaction_data+2, term_totallen);
	AssmTLV((BYTE*)"\x30\x31\x32\x33\x34\x35\x36\x37\x38", 8, TTAG_TERM_ID, 2, transaction_data+2, term_totallen);
	
	//2CN.004.00.00 end
	
	AssmTLV(amount_9f02, 6, TTAG_AUTH_QUAN, 2, transaction_data+2, term_totallen);
	AssmTLV(m_edit_9c, TTAG_TRANACTION_TYPE, 1, transaction_data+2, term_totallen);
	AssmTLV((BYTE*)"\x00\x00\x00\x00\x00\x00", 6, TTAG_AUTH_OTHER_QUAN, 2, transaction_data+2, term_totallen);
	if(m_check_isforce_online.GetCheck())
    	AssmTLV((BYTE*)"\x01", 1, TTAG_FORCE_CONNECT, 2, transaction_data+2, term_totallen);
	AssmTLV((BYTE*)"\x19", 1, TTAG_VENDOR_TRANSACTION_TYPE, 2, transaction_data+2, term_totallen);
	AssmTLV((BYTE*)"\x06", 1, TTAG_VENDOR_FLOW, 2, transaction_data+2, term_totallen);
	AssmTLV((BYTE*)"\x00", 1, TTAG_VENDOR_SEL_ACCOUNT, 2, transaction_data+2, term_totallen);
	AssmTLV(tmp, 4, TTAG_TEST_SEQ_COUNTER, 2, transaction_data+2, term_totallen);

    /*Server related*/
    Issuer->Clear();

    if(mode == MODE_PC)
        PBOCFF_Execute(&pboc_card_files, &pboc_term_data, transaction_data+2, term_totallen, FlowFinish, FlowInd);
	else
	{
	    uint16_t resp;
		uint8_t result[1024];
		uint32_t len;
		//make LL		
	    Hex2BCD(term_totallen, transaction_data); 
		Sleep(100);
	    len = MposSendCmd(transaction_data, term_totallen+2, CMD_OP_PBOC_EXECUTE, 0, resp, result, 1024, 0, InfoRecv);

		if(resp == RES_OP_OK)
		{
		    FlowFinish(result, len);
		}
		else
			TRACE(TEXT("SHIT!!!! resp = %d\n"), resp);

	}
}

CString CPBOC_card_testDlg::GenerateTLVString(DWORD tag, BYTE *lv)
{
	CString str;

    str.Format(TEXT("%s [%X]:"), SearchDict(tag), tag);
	
	for(int i =0; i < lv[0] ; i++)
	{
	    str.Format(str+TEXT("%02X"), lv[i+1]);
	}

	return str;
}

void CPBOC_card_testDlg::UpdateEditValue(int id, uint8_t *lv)
{	
	CString str;
	
	for(int i =0; i < lv[0] ; i++)
	{
	    str.Format(str+TEXT("%02X"), lv[i+1]);
	}
	this->GetDlgItem(id)->SetWindowText(str);
}

void CPBOC_card_testDlg::UpdateEditValue(int id, BYTE *lv, BOOL isAppend)
{
	CString str;

    if(isAppend)
	{
	    this->GetDlgItem(id)->GetWindowText(str);
	}
	
	for(int i =0; i < lv[0] ; i++)
	{
	    str.Format(str+TEXT("%02X"), lv[i+1]);
	}

	str.Format(str+TEXT("\r\n"));
	this->GetDlgItem(id)->SetWindowText(str);
}


CString CPBOC_card_testDlg::SearchDict(uint32_t tag, CString default_str)
{
    CString no_name;
    for(int i = 0; i < file_dict_num; i++)
	{
	    if(tag == file_dict[i].tag)
		    return file_dict[i].name;
	}

    no_name = default_str;
	return no_name;
}

void CPBOC_card_testDlg::UpdateTermUI()
{
	uint8_t *tlv;
	uint32_t tlvlen;
	uint8_t *lv;
	uint32_t tag, i;

	((CEdit*)m_TermFix.GetWindow(GW_CHILD))->SetReadOnly();
    m_TermFix.ResetContent();	

	for(i = 0; i < file_term_num; i++)
	{
        tlvlen = (file_term_bltlv[i][0] << 8) + file_term_bltlv[i][1];
		tlv = &file_term_bltlv[i][2];
		
        tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv);

		m_TermFix.AddString(GenerateTLVString(tag, lv));
	}


	for(i = 0; i < file_term_special_num; i++)
	{
        tlvlen = (file_term_special_bltlv[i][0] << 8) + file_term_special_bltlv[i][1];
		tlv = &file_term_special_bltlv[i][2];
		
        tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv);

		m_TermFix.AddString(GenerateTLVString(tag, lv));
	}

	m_TermFix.SetCurSel(0);	

}

BOOL CPBOC_card_testDlg::CheckReader()
{
	unsigned short vid_to_find, pid_to_find;
	int com_found;

    if(mode == MODE_PC) //PC
	{
	    LONG num = m_SCReader.ListReaders();
	    if(num > READER_IDX)
    	{
			m_SCReader.SetCurReader(READER_IDX);
			return TRUE;
    	}
	    
        return FALSE;
	}
	else 
	{
	
	    //Q5
		vid_to_find = 0x19c6;
	    pid_to_find = 0x83a6;

		com_found = FindComPort("Taxeia Virtual Com Port", &vid_to_find, &pid_to_find);  

		if(com_found > 0)
		{
		    if(serial_port.IsOpen())
				serial_port.Close();
	        serial_port.Open(com_found, 115200, CSerialPort::NoParity, 8, CSerialPort::OneStopBit, CSerialPort::NoFlowControl, false);
		   	return TRUE;
		}
        return FALSE;
	}
	
}

void CPBOC_card_testDlg::OnBnClickedAtr()
{
	CString cstr;
    RECT rect;
	GetDlgItem(ID_ATR)->EnableWindow(0);
    GetDlgItem(IDC_RADIO_PC)->EnableWindow(0);
    GetDlgItem(IDC_RADIO_Q5)->EnableWindow(0);

    //detect hardware
    if(!CheckReader())
	{
		Helper::ShowUserMsg(_T("Error"), _T("Please plug card reader !!"), MB_OK);
		return;
	}

    //detect USER input
	if(!this->GetDlgItem(IDC_EDIT_9C_TYPE)->GetWindowTextLengthW())
	{
		Helper::ShowUserMsg(_T("Error"), _T("Please fill the Amount and Type !!"), MB_OK);
		//return;
	}

	//Init Storage
recheck:
	if(!CheckStorageSetting())
	{
	    cstr.Format(_T("Storage Error!"));		
		Helper::ShowUserMsg(_T("Error"), cstr, MB_OK);
		exit(0);
	}

	if(!ReadStorageSetting())
	{
	    cstr.Format(_T("Can't Access Storage!"));		
		Helper::ShowUserMsg(_T("Error"), cstr, MB_OK);
		exit(0);
	}

	if(!CheckConsistence())
	{
    	int yesno = 0;
		
	    cstr.Format(_T("File Setting is different from Storage, ReSync?"));		
		
		yesno = Helper::ShowUserMsg(_T("Warning"), cstr, MB_YESNO);

		if(yesno == IDYES)
		{
		    OverWriteStorageSetting();

			goto recheck;
		}

	}


    //Init PBOC
	InitPBOC();
	
    //Init UI
    UpdateTermUI();
	UpdateAIDUI();
    UpdateCAUI();


    //Init Server
	Issuer = new CIssuerDlg(file_dict, file_dict_num,  &file_test_bltlvs[0], file_test_num);
	
	Issuer->Create(CIssuerDlg::IDD, GetDesktopWindow());
	Issuer->ShowWindow(TRUE);
	Issuer->GetClientRect(&rect);
	
	Issuer->SetWindowPos(0, 0, 0, rect.right - rect.left, rect.bottom - rect.top, 0);
    
	RECT rect1;
	this->GetClientRect(&rect1);
	this->SetWindowPos(0, rect.right - rect.left, 0, rect1.right - rect1.left, rect1.bottom - rect1.top, 0);
		
}

void CPBOC_card_testDlg::PBOCEnd()
{
    if(mode == MODE_PC) 
	    PBOCFF_End(&pboc_card_files, &pboc_term_data);
	else
	{//Q5
	    uint16_t resp;
		uint8_t result[1024];
		uint32_t len;
	    len = MposSendCmd(0, 0, CMD_OP_PBOC_END, 0, resp, result, 1024, 0);

		if(resp == RES_OP_OK)
		{
		}
		else
			TRACE(TEXT("SHIT!!!!\n"));
	}

	tsc_9f41++;
	m_check_isforce_online.SetCheck(1);
	Go2State(PBOC_WAIT_CARD_OUT);
}

void CPBOC_card_testDlg::PBOC2nd(BYTE *ARCLV/*lv*/, BYTE *AUTH_CODE/*6bytes*/, BYTE *ISSUER_AUTH_DATA,  BYTE IAD_LEN, BYTE *SCRIPT, DWORD SCRIPT_LEN)
{
	DWORD term_totallen;

	term_totallen = 0;

	if(ARCLV) //online success
	{
		AssmTLV(ARCLV+1, ARCLV[0], TTAG_ARC, 1, issuer_data+2, term_totallen);

		if(AUTH_CODE)
			AssmTLV(AUTH_CODE, 6, TTAG_AUTH_CODE, 1, issuer_data+2, term_totallen);
		if(IAD_LEN)
		{
			AssmTLV(ISSUER_AUTH_DATA, IAD_LEN, TTAG_ISSUER_AUTH_DATA, 1, issuer_data+2, term_totallen);
		}

		if(SCRIPT_LEN)
		{//tag len including
			AssmTLV(SCRIPT, SCRIPT_LEN, issuer_data+2, term_totallen);
		}
	}


    if(mode == MODE_PC)
		PBOCFF_2ndAuth(&pboc_card_files, &pboc_term_data, issuer_data+2, term_totallen, Flow2ndFinish, 0);
	else //Q5
	{
	    uint16_t resp;
		uint8_t result[1024];
		uint32_t len;
        //make LL
	    Hex2BCD(term_totallen, issuer_data);
	    len = MposSendCmd(issuer_data, term_totallen+2, CMD_OP_PBOC_2ND, 0, resp, result, 1024, 0);

		if(resp == RES_OP_OK)
		{
		
		    Flow2ndFinish(result, len);
		}
		else
		{
		    TRACE(TEXT("SHIT!!\n"));
		}
	}
}

//0:start 1:stop else: thread self
BYTE CPBOC_card_testDlg::RunPBOC(BYTE start_nstop)
{
    static HANDLE OSThread = (HANDLE)-1;

    if(start_nstop == 1) //Start Thread
    {
        if(OSThread != (HANDLE) -1) //already runing
            return 2;

        DWORD dwThreadID;

        OSThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
                                0,
                                (LPTHREAD_START_ROUTINE)CPBOC_card_testDlg::RunPBOC,
                                (LPVOID)3,
                                0, &dwThreadID);

        return 0;
    }
    else if(start_nstop == 0) //Stop Thread
    {
        if(OSThread != (HANDLE)-1)
        {
            TerminateThread(OSThread, 0);
            CloseHandle(OSThread);
            OSThread = (HANDLE)-1;
            return 0;
        }
        return 1;
    }

    /////Thread Body/////////////////////
    ThisIsMe->FlowPBOC();
	CloseHandle(OSThread);
    OSThread = (HANDLE)-1;
    return 0;
}

//0:start 1:stop else: thread self
BYTE CPBOC_card_testDlg::RunSwipe(BYTE start_nstop)
{
    static HANDLE OSThread = (HANDLE)-1;

    if(start_nstop == 1) //Start Thread
    {
        if(OSThread != (HANDLE) -1) //already runing
            return 2;

        DWORD dwThreadID;

        OSThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
                                0,
                                (LPTHREAD_START_ROUTINE)CPBOC_card_testDlg::RunSwipe,
                                (LPVOID)3,
                                0, &dwThreadID);

        return 0;
    }
    else if(start_nstop == 0) //Stop Thread
    {
        if(OSThread != (HANDLE)-1)
        {
            TerminateThread(OSThread, 0);
            CloseHandle(OSThread);
            OSThread = (HANDLE)-1;
            return 0;
        }
        return 1;
    }

    /////Thread Body/////////////////////
    ThisIsMe->FlowSwipe();
	CloseHandle(OSThread);
    OSThread = (HANDLE)-1;
    return 0;
}


void CPBOC_card_testDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnTimer(nIDEvent);

    if(nIDEvent == PBOC_STATE_PROCESS)
	{
	    KillTimer(PBOC_STATE_PROCESS);
	    StateProcess();
		SetTimer(PBOC_STATE_PROCESS, 250, 0);
	}
	else if(nIDEvent == SECOND_TIMER)
	{
	    CString str;
		second ++;
		str.Format(TEXT("%d"), second);
	    this->GetDlgItem(IDC_STATIC_SECOND)->SetWindowTextW(str);
	}
	else if(nIDEvent == PBOC_AUTO_OUT_IN)
	{
    	int yesno = 0;
		CString cstr;

	    KillTimer(PBOC_AUTO_OUT_IN);
		
	    cstr.Format(_T("Testing Next ?"));		
		
    	yesno = Helper::ShowUserMsg(_T("Confirm"), cstr, MB_YESNO);

		if(yesno == IDYES)
		{
    		OnBnClickedButtonEditWaitin();
	    }
		else
		    Go2State(PBOC_WAIT_CARD_OUT);

	}
}


void CPBOC_card_testDlg::InitAIDUI()
{
	myListAID.SetExtendedStyle(LVS_EX_GRIDLINES);

    CString str;


	myListAID.InsertColumn(0, SearchDict(TTAG_RID, TEXT("AID")) , LVCFMT_RIGHT);
	myListAID.InsertColumn(1, SearchDict(TTAG_ASI,TEXT("ASI")), LVCFMT_RIGHT);
	myListAID.InsertColumn(2, SearchDict(TTAG_APP_VER,TEXT("APP Ver")), LVCFMT_RIGHT);
	myListAID.InsertColumn(3, SearchDict(TTAG_TAC_DEFAULT,TEXT("TAC Default")), LVCFMT_RIGHT);
	myListAID.InsertColumn(4, SearchDict(TTAG_TAC_ONLINE,TEXT("TAC Online")), LVCFMT_RIGHT);
	myListAID.InsertColumn(5, SearchDict(TTAG_TAC_DENIAL,TEXT("TAC Denial")), LVCFMT_RIGHT);
	myListAID.InsertColumn(6, SearchDict(TTAG_FLOOR_LMT,TEXT("Floor Limit")), LVCFMT_RIGHT);
	myListAID.InsertColumn(7, SearchDict(TTAG_BIAS_RANDOM_SEL_THRESHOLD,TEXT("BRS Ths Value")), LVCFMT_RIGHT);
	myListAID.InsertColumn(8, SearchDict(TTAG_BIAS_RANDOM_SEL_MAX_PERCENTAGE,TEXT("BRS Maximal %")), LVCFMT_RIGHT);
	myListAID.InsertColumn(9, SearchDict(TTAG_RANDOM_SEL_PERCENTAGE,TEXT("RS Target %")), LVCFMT_RIGHT);

	myListAID.AdjustColumnWidth();

}

void CPBOC_card_testDlg::InitUI()
{
    CString str_9f02, str_9c, str;

	str_9f02 = SearchDict(TTAG_AUTH_QUAN, TEXT("Amount"));
	str_9f02+=TEXT("(9F02)");
	
    GetDlgItem(IDC_STATIC_9F02)->SetWindowTextW(str_9f02);

	str_9c = SearchDict(TTAG_TRANACTION_TYPE, TEXT("Type"));
	str_9c+=TEXT("(9C)");
	
    GetDlgItem(IDC_STATIC_9C)->SetWindowTextW(str_9c);

	CEdit *m_edit;
	
	m_edit_9f02.SetWindowTextW(TEXT("$1"));

	m_edit = (CEdit*) GetDlgItem(IDC_EDIT_9C_TYPE);
	m_edit->SetWindowTextW(TEXT("00"));


    str.LoadStringW(IDS_STRING_BUTTON_OUT_IN);
	GetDlgItem(IDC_BUTTON_EDIT_WAITIN)->SetWindowTextW(str);
    str.LoadStringW(IDS_STRING_BUTTON_SWIPE);
	GetDlgItem(IDC_BUTTON_Swipe)->SetWindowTextW(str);
    str.LoadStringW(IDS_STRING_BUTTON_SET_TIME);
	GetDlgItem(IDC_BUTTON_LOCK_TIME)->SetWindowTextW(str);

}

void CPBOC_card_testDlg::InitCAUI()
{
	myListCA.SetExtendedStyle(LVS_EX_GRIDLINES);

	myListCA.InsertColumn(0, SearchDict(TTAG_RID,TEXT("AID")), LVCFMT_RIGHT);
	myListCA.InsertColumn(1, SearchDict(TTAG_CA_IDX,TEXT("IDX")), LVCFMT_RIGHT);
	myListCA.InsertColumn(2, SearchDict(TTAG_CA_EXP,TEXT("EXP")), LVCFMT_RIGHT);
	myListCA.InsertColumn(3, SearchDict(TTAG_CA_MOD,TEXT("MOD")), LVCFMT_LEFT);

	myListCA.AdjustColumnWidth();

}

CString ltlv2str(BYTE *ltlv)
{
    CString str;

    if(ltlv)
	{
	    str = TEXT("");
	    for(int i = 0; i < ltlv[0]; i++)
		{
		    str.Format(str+TEXT("%02X"), ltlv[i+1]);
		}
	}
	else
		str = TEXT("None");
	return str;
}

void CPBOC_card_testDlg::UpdateCAUI()
{
    BYTE *rid, ridx;
	BYTE *lv_idx;
	BYTE *lv_exp;
	BYTE *lv_mod;
	
	for(ridx = 0; ridx < ca_num; ridx++)
	{

		PBOC_TLVFindTag(&ca_bltlv[ridx][2], (ca_bltlv[ridx][0] << 8) + ca_bltlv[ridx][1], TTAG_RID, &rid);
		
		lv_idx = lv_exp = lv_mod = 0;

		PBOC_TLVFindTag(&ca_bltlv[ridx][2], (ca_bltlv[ridx][0] << 8) + ca_bltlv[ridx][1], TTAG_CA_IDX, &lv_idx);
		PBOC_TLVFindTag(&ca_bltlv[ridx][2], (ca_bltlv[ridx][0] << 8) + ca_bltlv[ridx][1], TTAG_CA_EXP, &lv_exp);
		PBOC_TLVFindTag(&ca_bltlv[ridx][2], (ca_bltlv[ridx][0] << 8) + ca_bltlv[ridx][1], TTAG_CA_MOD, &lv_mod);
		
		myListCA.InsertItem((int)ridx, ltlv2str(rid));		
		myListCA.SetItemText((int)ridx, 1, ltlv2str(lv_idx)); 
		myListCA.SetItemText((int)ridx, 2, ltlv2str(lv_exp)); 
		myListCA.SetItemText((int)ridx, 3, ltlv2str(lv_mod)); 
       
	}
	myListCA.AdjustColumnWidth();
}

void CPBOC_card_testDlg::UpdateAIDUI()
{
	BYTE *aid, aidx;
	DWORD tag;
	BYTE *lv_aid;
	BYTE *lv_asi;
	BYTE *lv_app_ver;
	BYTE *lv_tac_defaul;
	BYTE *lv_tac_online;
	BYTE *lv_tac_denial;
	BYTE *lv_tac_floor_limit;
	BYTE *lv_brs_ths;
	BYTE *lv_brs_maximal;
	BYTE *lv_rs_target;
	BYTE temp[10];
	CString tmp_str;

	for(aidx = 0; aidx < aid_num; aidx++)
	{   
	    BYTE *ltlv;
    	//Find tags
		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_RID, &lv_aid);
		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_ASI, &lv_asi);
//		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_APP_VER, &lv_app_ver);
		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_TAC_DEFAULT, &lv_tac_defaul);
		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_TAC_ONLINE, &lv_tac_online);
		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_TAC_DENIAL, &lv_tac_denial);
		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_FLOOR_LMT, &lv_tac_floor_limit);
		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_BIAS_RANDOM_SEL_THRESHOLD, &lv_brs_ths);
		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_BIAS_RANDOM_SEL_MAX_PERCENTAGE, &lv_brs_maximal);
		PBOC_TLVFindTag(&aid_bltlv[aidx][2], (aid_bltlv[aidx][0] << 8) + aid_bltlv[aidx][1], TTAG_RANDOM_SEL_PERCENTAGE, &lv_rs_target);

		PBOC_AutoTermAppVer(lv_aid, temp);
		lv_app_ver = temp;
        
		myListAID.InsertItem((int)aidx, ltlv2str(lv_aid));		
		myListAID.SetItemText((int)aidx, 1, ltlv2str(lv_asi)); 
		myListAID.SetItemText((int)aidx, 2, ltlv2str(lv_app_ver)); 
		myListAID.SetItemText((int)aidx, 3, ltlv2str(lv_tac_defaul)); 
		myListAID.SetItemText((int)aidx, 4, ltlv2str(lv_tac_online)); 
		myListAID.SetItemText((int)aidx, 5, ltlv2str(lv_tac_denial)); 
		myListAID.SetItemText((int)aidx, 6, ltlv2str(lv_tac_floor_limit)); 
		myListAID.SetItemText((int)aidx, 7, ltlv2str(lv_brs_ths)); 
		myListAID.SetItemText((int)aidx, 8, ltlv2str(lv_brs_maximal)); 
		myListAID.SetItemText((int)aidx, 9, ltlv2str(lv_rs_target)); 

	}

	myListAID.AdjustColumnWidth();

}

void CPBOC_card_testDlg::OnBnClickedButtonEditAid()
{
	// TODO: Add your control notification handler code her
   
}

void CPBOC_card_testDlg::OnBnClickedRadioS()
{
	// TODO: Add your control notification handler code here
	CButton *but = (CButton*)GetDlgItem(IDC_RADIO_PC);
	if(but->GetCheck())
		mode = MODE_PC; //PC
	else
		mode = MODE_Q5; //Q5
}

void CPBOC_card_testDlg::OnEnUpdateEdit9f02Amount()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function to send the EM_SETEVENTMASK message to the control
	// with the ENM_UPDATE flag ORed into the lParam mask.

	// TODO:  Add your control notification handler code here
//	m_edit->GetWindowText(str);
}

void CPBOC_card_testDlg::OnEnChangeEdit9f02Amount()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
#if 1
	CString str;
	m_edit_9f02.GetWindowText(str);

	if(str.GetAt(0) != '$')
    	m_edit_9f02.SetWindowTextW(TEXT("$"));
	else if(str.GetLength() > 1) //exclude $
	{
		int point;

		point = 0;
		for(int i = 1; i < str.GetLength(); i++)
		{
    	    if(str.GetAt(i) == '.')
			{
				if(!point)
				    point = i;
				else
				{
        		    str.SetAt(i, 0);
                    m_edit_9f02.SetWindowTextW(str);
		            m_edit_9f02.SetFocus();
		            m_edit_9f02.SetSel(-1);
					return;
				}
			}
			else if(!isdigit(str.GetAt(i)))
			{
        		    str.SetAt(i, 0);
                    m_edit_9f02.SetWindowTextW(str);
		            m_edit_9f02.SetFocus();
		            m_edit_9f02.SetSel(-1);
					return;
			}
			else if(point)
			{
				if(i > point+2)
				{
        		    str.SetAt(i, 0);
                    m_edit_9f02.SetWindowTextW(str);
		            m_edit_9f02.SetFocus();
		            m_edit_9f02.SetSel(-1);
					return;
				}
			}

		}

        uint8_t amount[12];
		if(point == 0)
			point = str.GetLength();

		for(int i = 0; i < 10; i++)
		{
		    if(i < (10 - (point - 1)))
				amount[i] = '0';
			else
				amount[i] = str.GetAt(i - (10 - point));
		}

		for(int i = 10; i < 12; i++)
		{
		    if((i - 10)+ point + 1 < str.GetLength())
			    amount[i] = str.GetAt((i - 10)+ point + 1);
			else
				amount[i] = '0';
		}


	    CString str_9f02;
		
		str_9f02 = SearchDict(TTAG_AUTH_QUAN, TEXT("Amount"));
		str_9f02+=TEXT("(9F02)\n");

        for(int i = 0; i < 6; i++)
		{
		    amount_9f02[i] = ((amount[2*i] - '0') << 4) + (amount[2*i+1] - '0');
			str_9f02.Format(str_9f02+TEXT("%02X"), amount_9f02[i]);
		}

	    GetDlgItem(IDC_STATIC_9F02)->SetWindowTextW(str_9f02);		

	}
#endif
}

void CPBOC_card_testDlg::OnBnClickedButtonEditWaitin()
{
	// TODO: Add your control notification handler code here
	TRACE(TEXT("card out!!"));
    KillTimer(PBOC_AUTO_OUT_IN);
    Go2State(PBOC_WAIT_CARD_IN);
	
}

void CPBOC_card_testDlg::OnBnClickedButtonLockTime()
{
    if(!isTimeLocked)
	{
	    GetTimes(&year_idx, &month_idx, &day_idx, &hour_idx, &min_idx, &sec_idx);

		// TODO: Add your control notification handler code here
		CSetTimeDlg dlg;

		dlg.SetTimes(year_idx, month_idx, day_idx, hour_idx, min_idx, sec_idx);
		dlg.DoModal();

		if((isTimeLocked = dlg.GetTimes(year_idx,month_idx,day_idx,hour_idx,min_idx,sec_idx)))
		{//time lock
		    uint8_t time_str[15];
			CString str;
			GetTime(time_str);
			str.Format(TEXT("Time From %S"), time_str);
		    this->GetDlgItem(IDC_STATIC_TIME)->SetWindowTextW(str);
			Go2State(PBOC_UPDATE_TIME);
		}

		this->GetDlgItem(IDC_BUTTON_LOCK_TIME)->SetWindowTextW(TEXT("Release Time"));
	}
	else
	{
	    isTimeLocked = FALSE;
		this->GetDlgItem(IDC_STATIC_TIME)->SetWindowTextW(NULL);
		this->GetDlgItem(IDC_BUTTON_LOCK_TIME)->SetWindowTextW(TEXT("Set Time"));
    }
}

void CPBOC_card_testDlg::OnBnClickedCheckForceOnline()
{
    uint8_t *tlv;
	uint32_t tlvlen;
	uint8_t *lv;
	uint32_t tag;
	int i;

	for(i = 0; i < file_term_num; i++)
	{
        tlvlen = (file_term_bltlv[i][0] << 8) + file_term_bltlv[i][1];
		tlv = &file_term_bltlv[i][2];
		
        tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv);

		if(tag == TTAG_TERMINAL_TYPE)
		{
		    if(lv && lv[0]==1 && ((lv[1] & 0xf) == 1 || (lv[1] & 0xf) == 4))
	    	{
	    	    CString str;
				str.Format(TEXT("Terminal Type is %02x, please set to non-online only!!"), lv[1]);
				m_check_isforce_online.SetCheck(1);
	    		MessageBox(str);
	    	}
		}
			
	}

}

void CPBOC_card_testDlg::OnBnClickedCheckNoAmount()
{
	// TODO: Add your control notification handler code here
	int a = m_check_no_amount.GetCheck(); //current
	m_edit_9f02.EnableWindow(!a);
	GetDlgItem(IDC_STATIC_9F02)->EnableWindow(!a);	
}

void CPBOC_card_testDlg::OnBnClickedButtonSwipe()
{
	// TODO: Add your control notification handler code here
    KillTimer(PBOC_AUTO_OUT_IN);
    Go2State(PBOC_WAIT_SWIPE);

}
