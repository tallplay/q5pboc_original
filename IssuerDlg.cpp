// IssuerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PBOC_card_test.h"
#include "IssuerDlg.h"
#include "pboc\pboc.h"


void OpenUnpredictNumber();

// CIssuerDlg dialog

IMPLEMENT_DYNAMIC(CIssuerDlg, CDialog)

CIssuerDlg::CIssuerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIssuerDlg::IDD, pParent)
{
    
	request_save_len = 0;
	
}

CString CIssuerDlg::SearchDict(DWORD tag)
{
    CString no_name;
    for(int i = 0; i < dict_num; i++)
	{
	    if(tag == dict[i].tag)
		    return dict[i].name;
	}

    no_name = TEXT("");
	return no_name;
}

CIssuerDlg::CIssuerDlg(PBOC_DICTIONARY *dict, DWORD dict_num, char test[255][5][512], BYTE testnum)
{
	this->dict = dict;
	this->dict_num = dict_num;
	this->test_setting  = &test[0];
	this->test_num = testnum;
}

CIssuerDlg::~CIssuerDlg()
{
}

void CIssuerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_VECTORS, m_combo_vector);
	DDX_Control(pDX, IDC_COMBO_ARC, m_combo_arc);
}

void CIssuerDlg::Clear()
{
    this->GetDlgItem(IDC_EDIT_TRADE)->SetWindowTextW(TEXT(""));
}

BOOL CIssuerDlg::IsAble2Online()
{
    CEdit *arc, *iad, *authcode;

    arc = (CEdit*) this->GetDlgItem(IDC_EDIT_ARC);
    authcode = (CEdit*) this->GetDlgItem(IDC_EDIT_AUTH_CODE);
    iad = (CEdit*) this->GetDlgItem(IDC_EDIT_IAD);

	if(!arc->GetWindowTextLength() || !authcode->GetWindowTextLength() || !authcode->GetWindowTextLength())
		return FALSE;
	return TRUE;
}

void CIssuerDlg::StrUtil_AddTagValue(CString &str, DWORD tag, DWORD len, BYTE *dat)
{
    if(tag == TTAG_ARC)
	{
		str.Format(str+TEXT("%s [%X]:"), SearchDict(tag), tag);

	    if(len != 2)
			str.Format(str+TEXT("(%02X)"), len);

		for(int i = 0; i < len; i++)
		{
		    if(isdigit(dat[i]))
		    	str.Format(str+TEXT("%02X"), dat[i]);
		}

	    if(len != 2)
			str.Format(str+TEXT(" (Invalid length)"));

		str.Format(str+TEXT("\r\n"));
	}
	else
	{
		str.Format(str+TEXT("%s [%X]:"), SearchDict(tag), tag);
		for(int i = 0; i < len; i++)
	    	str.Format(str+TEXT("%02X"), dat[i]);
		str.Format(str+TEXT("\r\n"));
	}
}

void CIssuerDlg::TransactionResponse(BYTE *arclv, BYTE authcode[6], BYTE issauthdata[16], BYTE *issuedatalen, BYTE script[512], DWORD *script_len)
{
    CString report_str;
    int i;
	this->GetDlgItem(IDC_EDIT_TRADE)->GetWindowText(report_str);
    report_str += TEXT("\r\n============[Financial Transaction Response Messages]============\r\n");

	
	this->SendData(TTAG_ARC, arclv);
	this->SendData(TTAG_AUTH_CODE, authcode);	
	*issuedatalen = this->SendData(TTAG_ISSUER_AUTH_DATA, issauthdata);
	*script_len = this->SendData(TTAG_ISSUER_SCRIPT1, script);

    StrUtil_AddTagValue(report_str, TTAG_ARC, arclv[0], arclv+1);
    StrUtil_AddTagValue(report_str, TTAG_AUTH_CODE, 6, authcode);
    StrUtil_AddTagValue(report_str, TTAG_ISSUER_AUTH_DATA, *issuedatalen, issauthdata);
    StrUtil_AddTagValue(report_str, TTAG_ISSUER_SCRIPT1, *script_len, script);


    this->GetDlgItem(IDC_EDIT_TRADE)->SetWindowTextW(report_str);
   ((CEdit*)this->GetDlgItem(IDC_EDIT_TRADE))->LineScroll( ((CEdit*)this->GetDlgItem(IDC_EDIT_TRADE))->GetLineCount());
}

#define C2B(x) (x > '9' ? x-'A'+10 : x - '0')

UINT CIssuerDlg::SendData(UINT tag, BYTE *dat)
{
    CEdit *arc, *iad, *authcode;
	CEdit *script;
	CString str_arc, str_iad, str_authcode, str_script;
	
	
    arc = (CEdit*) this->GetDlgItem(IDC_EDIT_ARC);
    authcode = (CEdit*) this->GetDlgItem(IDC_EDIT_AUTH_CODE);
    iad = (CEdit*) this->GetDlgItem(IDC_EDIT_IAD);
    script = (CEdit*) this->GetDlgItem(IDC_EDIT_SCRIPT);
	
	arc->GetWindowText(str_arc);
	authcode->GetWindowText(str_authcode);
	iad->GetWindowText(str_iad);
	script->GetWindowText(str_script);
	
    if(tag == TTAG_AUTH_CODE)
	{
		for(int i = 0; i < 6; i++)
			dat[i] = (C2B(str_authcode.GetAt(i*2)) << 4) + (C2B(str_authcode.GetAt(i*2 + 1)));

	  //  memcpy(dat, (BYTE*)"\x00\x00\x00\x00\x00\x00", 6); 

	    return 6;
	}
	else if(tag == TTAG_ARC)
	{
		for(int i = 0; i < str_arc.GetLength()/2; i++)
			dat[i+1] = (C2B(str_arc.GetAt(i*2)) << 4) + (C2B(str_arc.GetAt(i*2 + 1)));

		if(m_combo_arc.GetCurSel() == 4)
		{//2CO.023.00, its tricks, and tester was misunderstanding the test item. We don't want to argu with him.
			dat[0] = 1 + (str_arc.GetLength()/2);
			dat[dat[0]] = 0xff;
		}
		else
			dat[0] = str_arc.GetLength()/2;
		
		    return dat[0];
	}
	else if(tag == TTAG_ISSUER_AUTH_DATA)
	{

		for(int i = 0; i < str_iad.GetLength()/2; i++)
			dat[i] = (C2B(str_iad.GetAt(i*2)) << 4) + (C2B(str_iad.GetAt(i*2 + 1)));

	    return str_iad.GetLength()/2;
	}
	else if(tag == TTAG_ISSUER_SCRIPT1)
	{
		for(int i = 0; i < str_script.GetLength()/2; i++)
			dat[i] = (C2B(str_script.GetAt(i*2)) << 4) + (C2B(str_script.GetAt(i*2 + 1)));

	    return str_script.GetLength()/2;	
	}

	return 0;
}

BYTE * CIssuerDlg::TransactionReversal(BYTE *report, DWORD report_len, BYTE terminal_type)
{
    CString report_str;
	UINT tag;
	BYTE *tlv;
	uint32_t tlvlen;
	BYTE *lv;
	BYTE arclv[10], authcode[6];

    int i;

	this->GetDlgItem(IDC_EDIT_TRADE)->GetWindowText(report_str);
    report_str += TEXT("\r\n============[Financial Transaction Reversal Messages]============\r\n");
	tlv = report + 2;
	tlvlen = report_len -2;
    while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv)) != 0)
	{
	    if(tag == TTAG_AUTH_OTHER_QUAN || tag == CTAG_AC || tag == TTAG_UNPREDICT|| (tag == 0xDF37) || (tag == 0xDF75))//skip
		    continue;
		report_str.Format(report_str+TEXT("%s [%X]:"), SearchDict(tag), tag);
        for(i = 0; i < lv[0]; i++)
    		report_str.Format(report_str+TEXT("%02X"), lv[1+i]);
		report_str.Format(report_str+TEXT("\r\n"));
	}


    //POS entry 
	report_str.Format(report_str+TEXT("%s [%X]:"), SearchDict(TTAG_POS_ENTRY), TTAG_POS_ENTRY);
	report_str.Format(report_str+TEXT("21"));
	report_str.Format(report_str+TEXT("\r\n"));

	//Terminal ID
	report_str.Format(report_str+TEXT("%s [%X]:"), SearchDict(TTAG_TERM_ID), TTAG_TERM_ID);
	report_str.Format(report_str+TEXT("303132333435363738"));
	report_str.Format(report_str+TEXT("\r\n"));

    //From Financial Response 
	this->SendData(TTAG_ARC, arclv);
	this->SendData(TTAG_AUTH_CODE, authcode);	

    StrUtil_AddTagValue(report_str, TTAG_ARC, arclv[0], arclv+1);
    StrUtil_AddTagValue(report_str, TTAG_AUTH_CODE, 6, authcode);

	//From Financial Request 
	tlv = request_save + 2;
	tlvlen = request_save_len -2;
    while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv)) != 0)
	{
	    if(tag == TTAG_TRANACTION_TYPE || tag == CTAG_ATC || tag == TTAG_AUTH_CODE || tag == TTAG_AUTH_OTHER_QUAN || tag == TTAG_DATE || tag == TTAG_TVR || tag == CTAG_AC || tag == TTAG_UNPREDICT || (tag == 0xDF37) || (tag == 0xDF75))//skip
		    continue;
		report_str.Format(report_str+TEXT("%s [%X]:"), SearchDict(tag), tag);
        for(i = 0; i < lv[0]; i++)
    		report_str.Format(report_str+TEXT("%02X"), lv[1+i]);
		report_str.Format(report_str+TEXT("\r\n"));
	}

	report_str.Format(report_str+TEXT("%s [%X]:"), SearchDict(TTAG_TERMINAL_TYPE), TTAG_TERMINAL_TYPE);
	report_str.Format(report_str+TEXT("%02X"), terminal_type);
	report_str.Format(report_str+TEXT("\r\n"));

	report_str.Format(report_str+TEXT("\r\n"));


    this->GetDlgItem(IDC_EDIT_TRADE)->SetWindowTextW(report_str);
   ((CEdit*)this->GetDlgItem(IDC_EDIT_TRADE))->LineScroll( ((CEdit*)this->GetDlgItem(IDC_EDIT_TRADE))->GetLineCount());

	return 0;
}

BYTE unpredict[2000][4];
DWORD unpredict_idx = 0;

void CheckUnpredictNumber()
{
    //judge if reduncy
	for(int i = 0; i < unpredict_idx; i++)
	{
		for(int j = i+1; j < unpredict_idx; j++)
		{
			    if(!memcmp(&unpredict[i], &unpredict[j], 4)) 
				{
					CString str;
					str.Format(TEXT("Random number error in %d and %d!!"), i, j);
					MessageBoxW(0, str, str, 0);
				}
		}
	}

}

void OpenUnpredictNumber()
{
	FILE *fp;
	char str[20];
	unpredict_idx = 0;
	fp = fopen("UnpredictNumber.txt", "a+");
	
	while(fscanf(fp, "%02X%02X%02X%02X\n", &unpredict[unpredict_idx][0],  &unpredict[unpredict_idx][1], &unpredict[unpredict_idx][2], &unpredict[unpredict_idx][3]) != EOF)
	{
       unpredict_idx++;
	}
	fclose(fp);
}

void SaveUnpredictNumber(BYTE *number4digit)
{
	FILE *fp;
	char str[20];
	fp = fopen("UnpredictNumber.txt", "a+");
    sprintf(str, "%02X%02X%02X%02X\n", number4digit[0],  number4digit[1], number4digit[2], number4digit[3]);
	fwrite(str, 1, strlen(str), fp);
	fclose(fp);

	memcpy(&unpredict[unpredict_idx++], number4digit, 4);
	CheckUnpredictNumber();
}

BYTE * CIssuerDlg::TransactionConfirm(BYTE *report, DWORD report_len)
{
    CString report_str;
	UINT tag;
	BYTE *tlv;
	uint32_t tlvlen;
	BYTE *lv;

    int i;

	this->GetDlgItem(IDC_EDIT_TRADE)->GetWindowText(report_str);
    report_str += TEXT("\r\n============[Financial Transaction Confirmation Messages]============\r\n");

	//Terminal ID
	report_str.Format(report_str+TEXT("%s [%X]:"), SearchDict(TTAG_TERM_ID), TTAG_TERM_ID);
	report_str.Format(report_str+TEXT("303132333435363738"));
	report_str.Format(report_str+TEXT("\r\n"));

	tlv = report + 2;
	tlvlen = report_len -2;

    while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv)) != 0)
	{
	    if(tag == TTAG_UNPREDICT)
	    {
	        SaveUnpredictNumber(&lv[1]);
			continue;
	    }
	    if(tag == 0xDF75)
    	{
    	    if(lv && lv[0] == 1)
	    	{
                if(lv[1] == 0x01)//TC
					report_str.Format(report_str+TEXT("TC\r\n"));
				else if(lv[1] == 0x04) //AAC
					report_str.Format(report_str+TEXT("AAC\r\n"));
	    	}
    	}
		else
		{
			report_str.Format(report_str+TEXT("%s [%X]:"), SearchDict(tag), tag);
	        for(i = 0; i < lv[0]; i++)
	    		report_str.Format(report_str+TEXT("%02X"), lv[1+i]);
			report_str.Format(report_str+TEXT("\r\n"));
		}
	}



    this->GetDlgItem(IDC_EDIT_TRADE)->SetWindowTextW(report_str);
   ((CEdit*)this->GetDlgItem(IDC_EDIT_TRADE))->LineScroll( ((CEdit*)this->GetDlgItem(IDC_EDIT_TRADE))->GetLineCount());

	return 0;
}



BYTE * CIssuerDlg::TransactionRequest(BYTE *report, DWORD report_len, BYTE *online_pin, BOOL is_retry)
{
    CString report_str;
	UINT tag;
	BYTE *tlv;
	uint32_t tlvlen;
	BYTE *lv;
	BYTE cnt;
	BYTE *a9f37;
	BYTE unpredict[8];

    int i;

    memcpy(request_save, report, report_len);
	request_save_len = report_len;

    if(is_retry)
	{
		this->GetDlgItem(IDC_EDIT_TRADE)->GetWindowText(report_str);
		report_str += TEXT("\r\n");
	}
	else
		report_str = TEXT("");

    report_str += TEXT("============[Financial Transaction Request Messages]============\r\n");

	/////////////////////////////////online pin//////////////////////////////////////////////////
    report_str.Format(report_str+TEXT("[Enciphered Online PIN]:"));
    cnt = online_pin[0];
	for(i = 0; i < cnt; i++)
	{
		report_str.Format(report_str+TEXT("%02X"),online_pin[i+1]);	    
	}
	
	if(cnt)
	{
    	report_str.Format(report_str+TEXT(" (==> "));

	    PBOC_TLVFindTag(report+2, report_len-2, 0x9F37, &a9f37);

		if(a9f37)
		{
		    memcpy(unpredict, a9f37+1, 4);
	        memcpy(unpredict+4, a9f37+1, 4);
		}
		else
		{
		    memset(unpredict, 0, 8);
		}

		Mpos_3DESDec(unpredict, &online_pin[1], &online_pin[1]);
	    cnt = online_pin[1] & 0xf;
		for(i = 0; i < cnt; i++)
		{
			report_str.Format(report_str+TEXT("%1X"),i & 1 ? (online_pin[i/2+2] >> 4) & 0xf : online_pin[i/2+2] & 0xf );	    
		}
		report_str.Format(report_str+TEXT(")"));
	}
	report_str.Format(report_str+TEXT("\r\n"));


	/////////////////////////////////////////////////////////////////////////////////////////
	tlv = report + 2;
	tlvlen = report_len -2;
    while((tag = PBOC_TLVEnum(&tlv, &tlvlen, &lv)) != 0)
	{
	    if(tag == 0xDF37)
			continue;
	    if(tag == 0xDF75)
    	{
    	    if(lv && lv[0] == 1 && lv[1] == 0x03)
				report_str.Format(report_str+TEXT("ARQC\r\n"));
    	}
	    else
    	{
			report_str.Format(report_str+TEXT("%s [%X]:"), SearchDict(tag), tag);
	        for(i = 0; i < lv[0]; i++)
	    		report_str.Format(report_str+TEXT("%02X"), lv[1+i]);
			report_str.Format(report_str+TEXT("\r\n"));
    	}
	}

    this->GetDlgItem(IDC_EDIT_TRADE)->SetWindowTextW(report_str);
	return 0;
}


BEGIN_MESSAGE_MAP(CIssuerDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_VECTORS, &CIssuerDlg::OnCbnSelchangeComboVectors)
	ON_CBN_SELCHANGE(IDC_COMBO_ARC, &CIssuerDlg::OnCbnSelchangeComboArc)
END_MESSAGE_MAP()


// CIssuerDlg message handlers

BOOL CIssuerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	this->GetDlgItem(IDC_EDIT_ARC)->SetWindowTextW(TEXT("3030"));
	this->GetDlgItem(IDC_EDIT_IAD)->SetWindowTextW(TEXT("01020102010201023030"));
	this->GetDlgItem(IDC_EDIT_AUTH_CODE)->SetWindowTextW(TEXT("000000000000"));

	m_combo_vector.AddString(TEXT("Default"));

    for(int i = 0; i < test_num; i++)
	{
		CString str;
		str.Format(TEXT("%S"), test_setting[i][0]);
		m_combo_vector.AddString(str);
	}

	m_combo_vector.SetCurSel(0);


    m_combo_arc.AddString(TEXT("Approve-00")); 
    m_combo_arc.AddString(TEXT("Decline-05")); 
    m_combo_arc.AddString(TEXT("Referral-01"));
    m_combo_arc.AddString(TEXT("Decline-51"));
	m_combo_arc.AddString(TEXT("Invalid Length-1234"));
    m_combo_arc.SetCurSel(0);

	OpenUnpredictNumber();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CIssuerDlg::OnCbnSelchangeComboVectors()
{
	// TODO: Add your control notification handler code here
	int sel = m_combo_vector.GetCurSel();

	CString str;
	CString str_arc, str_iad, str_ac, str_script;

    if(sel)
	{
		str.Format(TEXT("%S"), test_setting[sel-1][0]);
		str_arc.Format(TEXT("%S"), test_setting[sel-1][1]);
		str_iad.Format(TEXT("%S"), test_setting[sel-1][2]);
		str_ac.Format(TEXT("%S"), test_setting[sel-1][3]);
		str_script.Format(TEXT("%S"), test_setting[sel-1][4]);

		this->GetDlgItem(IDC_STATIC_VECTOR)->SetWindowTextW(str);
		this->GetDlgItem(IDC_EDIT_ARC)->SetWindowTextW(str_arc);
		this->GetDlgItem(IDC_EDIT_IAD)->SetWindowTextW(str_iad);
		this->GetDlgItem(IDC_EDIT_AUTH_CODE)->SetWindowTextW(str_ac);
		this->GetDlgItem(IDC_EDIT_SCRIPT)->SetWindowTextW(str_script);
	}
	else
	{
		this->GetDlgItem(IDC_STATIC_VECTOR)->SetWindowTextW(TEXT("Default"));
		this->GetDlgItem(IDC_EDIT_ARC)->SetWindowTextW(TEXT("3030"));
		this->GetDlgItem(IDC_EDIT_IAD)->SetWindowTextW(TEXT("01020102010201023030"));
		this->GetDlgItem(IDC_EDIT_AUTH_CODE)->SetWindowTextW(TEXT("000000000000"));
		this->GetDlgItem(IDC_EDIT_SCRIPT)->SetWindowTextW(TEXT(""));
	}

}

void CIssuerDlg::OnCbnSelchangeComboArc()
{
	// TODO: Add your control notification handler code here
	switch(m_combo_arc.GetCurSel())
	{
	    case 0:
			this->GetDlgItem(IDC_EDIT_ARC)->SetWindowTextW(TEXT("3030"));
			break;
		case 1:
			this->GetDlgItem(IDC_EDIT_ARC)->SetWindowTextW(TEXT("3035"));
			break;
		case 2:
			this->GetDlgItem(IDC_EDIT_ARC)->SetWindowTextW(TEXT("3031"));
			break;
	    case 3:
			this->GetDlgItem(IDC_EDIT_ARC)->SetWindowTextW(TEXT("3531"));
			break;
	    case 4:
			this->GetDlgItem(IDC_EDIT_ARC)->SetWindowTextW(TEXT("31323334"));
			break;
	}
}
