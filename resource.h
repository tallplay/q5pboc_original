//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PBOC_card_test.rc
//
#define IDD_PBOC_CARD_TEST_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_ISSUER               129
#define IDD_DIALOG_EDIT_AID             130
#define IDD_DIALOG_PIN_KAD              130
#define IDD_DIALOG1                     131
#define IDS_STRING_TRANSACTION_SUCCESS  132
#define IDS_STRING_TRANSACTION_DECLINE  133
#define IDS_STRING_TRANSACTION_TERMINATE 134
#define IDS_STRING_TRANSACTION_TERMINATE_NOT_ACCEPT 135
#define IDS_STRING_PLEASE_SWIPE         136
#define IDS_STRING_SWIPE_AGAIN          137
#define IDS_STRING_SC_CODE              138
#define IDS_STRING_PLEASE_INSERT_IC     139
#define IDS_STRING_SWIPE_END            140
#define IDS_STRING_BUTTON_SET_TIME      141
#define IDS_STRING_BUTTON_OUT_IN        142
#define IDS_STRING_BUTTON_SWIPE         143
#define IDS_STRING_TRACE_DATA           144
#define ID_ATR                          1007
#define IDC_EDIT_9F1A                   1010
#define IDC_EDIT_5F2A                   1011
#define IDC_EDIT_9F33                   1012
#define IDC_EDIT_9F4E                   1013
#define IDC_STATIC_AUTOMODE             1015
#define IDC_STATIC_PBOC_RESULT          1016
#define IDC_LIST_CA                     1018
#define IDC_LIST_AID                    1019
#define IDC_EDIT_TRADE                  1019
#define IDC_EDIT_ARC                    1020
#define IDC_EDIT_IAD                    1021
#define IDC_EDIT_AUTH_CODE              1022
#define IDC_EDIT_SCRIPT                 1023
#define IDC_EDIT_9F02_AMOUNT            1024
#define IDC_EDIT_9C_TYPE                1025
#define IDC_BUTTON_EDIT_AID             1026
#define IDC_BUTTON_EDIT_WAITIN          1026
#define IDC_BUTTON_Swipe                1027
#define IDC_COMBO_AID                   1030
#define IDC_RADIO_PC                    1031
#define IDC_RADIO2                      1032
#define IDC_RADIO_Q5                    1032
#define IDC_COMBO_TERMFIX               1034
#define IDC_STATIC_9F02                 1035
#define IDC_STATIC_9C                   1036
#define IDC_STATIC_SECOND               1037
#define IDC_EDIT_PIN                    1038
#define IDC_BUTTON_1                    1039
#define IDC_BUTTON_2                    1040
#define IDC_BUTTON_3                    1041
#define IDC_BUTTON_4                    1042
#define IDC_BUTTON_5                    1043
#define IDC_BUTTON_6                    1044
#define IDC_BUTTON_7                    1045
#define IDC_BUTTON_8                    1046
#define IDC_BUTTON_9                    1047
#define IDC_BUTTON_OK                   1048
#define IDC_STATIC_PBOC_END_RESULT      1049
#define IDC_COMBO_VECTORS               1050
#define IDC_STATIC_VECTOR               1051
#define IDC_BUTTON_CANCEL               1052
#define IDC_BUTTON_0                    1053
#define IDC_COMBO_ARC                   1053
#define IDC_BUTTON_LOCK_TIME            1054
#define IDC_STATIC_TIME                 1055
#define IDC_COMBO_YEAR                  1056
#define IDC_COMBO_MONTH                 1057
#define IDC_CHECK1                      1058
#define IDC_CHECK_FORCE_ONLINE          1058
#define IDC_COMBO_DAY                   1059
#define IDC_CHECK_NO_AMOUNT             1059
#define IDC_COMBO_HOUR                  1060
#define IDC_COMBO_MINUTE                1061
#define IDC_COMBO_SECOND                1062

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1060
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
