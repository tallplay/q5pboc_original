// PinDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PBOC_card_test.h"
#include "PinDlg.h"


// CPinDlg dialog

IMPLEMENT_DYNAMIC(CPinDlg, CDialog)

CPinDlg::CPinDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPinDlg::IDD, pParent)
{
}

CPinDlg::~CPinDlg()
{
}

void CPinDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

int CPinDlg::UpdateUI()
{
    int cnt;
    CString str;
	str.Format(TEXT("[Time Left %02d seconds]\r\n\r\n"), second);

    cnt = your_pin[0] & 0xf;

	for(int i = 0; i < cnt; i++)
		str.Format(str+TEXT("*"));

	this->GetDlgItem(IDC_EDIT_PIN)->SetWindowTextW(str);
    return 0;
}


BEGIN_MESSAGE_MAP(CPinDlg, CDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_1, &CPinDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON_2, &CPinDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON_3, &CPinDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON_4, &CPinDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON_5, &CPinDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON_6, &CPinDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON_7, &CPinDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON_8, &CPinDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON_9, &CPinDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON_OK, &CPinDlg::OnBnClickedButtonOk)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CPinDlg::OnBnClickedButtonCancel)
	ON_BN_CLICKED(IDC_BUTTON_0, &CPinDlg::OnBnClickedButton0)
END_MESSAGE_MAP()


// CPinDlg message handlers

void CPinDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == SECOND_DOWN_COUNT)
	{
	    second--;
		UpdateUI();

		Beep( 750, 300/2 );

		if(second == 0)
		{
		    memset(your_pin, 0, 8);
			this->EndDialog(1);
		}
	}
}

BOOL CPinDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
    SetTimer(SECOND_DOWN_COUNT, 1000, 0);
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPinDlg::ResetPin(unsigned char pin[8])
{
    second = 30;
	your_pin = pin;
	pin[0] = 0; //format = 0; len = 0;
    memset(pin, 0, 8);
}

void AddDigit(unsigned char *pin, unsigned char digit)
{
    int cnt;
	// TODO: Add your control notification handler code here
	cnt = pin[0] & 0xf;

	if(cnt == 12)
	    return;
	

	pin[1+cnt/2] |= (cnt & 1) ? (digit << 4) : digit;
	cnt++;
	pin[0] &= ~0xf;
	pin[0] |= (cnt & 0xf);
}

void CPinDlg::OnBnClickedButton1()
{
    AddDigit(your_pin,1);
	UpdateUI();
}

void CPinDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
    AddDigit(your_pin,2);
	UpdateUI();
}

void CPinDlg::OnBnClickedButton3()
{
	// TODO: Add your control notification handler code here
    AddDigit(your_pin,3);
	UpdateUI();
}

void CPinDlg::OnBnClickedButton4()
{
	// TODO: Add your control notification handler code here
    AddDigit(your_pin,4);
	UpdateUI();
}

void CPinDlg::OnBnClickedButton5()
{
	// TODO: Add your control notification handler code here
    AddDigit(your_pin,5);
	UpdateUI();
}

void CPinDlg::OnBnClickedButton6()
{
	// TODO: Add your control notification handler code here
    AddDigit(your_pin,6);
	UpdateUI();
}

void CPinDlg::OnBnClickedButton7()
{
	// TODO: Add your control notification handler code here
    AddDigit(your_pin,7);
	UpdateUI();
}

void CPinDlg::OnBnClickedButton8()
{
	// TODO: Add your control notification handler code here
    AddDigit(your_pin,8);
	UpdateUI();
}

void CPinDlg::OnBnClickedButton9()
{
	// TODO: Add your control notification handler code here
    AddDigit(your_pin,9);
	UpdateUI();
}

void CPinDlg::OnBnClickedButtonOk()
{
    int cnt;
    cnt = your_pin[0] & 0xf;
	// TODO: Add your control notification handler code here
	if(cnt >= 4 && cnt <= 12)
        this->EndDialog(0);

}

void CPinDlg::OnBnClickedButtonCancel()
{
	// TODO: Add your control notification handler code here
    memset(your_pin, 8, 0);
	UpdateUI();
    this->EndDialog(1);
}

void CPinDlg::OnBnClickedButton0()
{
	// TODO: Add your control notification handler code here
    AddDigit(your_pin,0);
	UpdateUI();
}
