#pragma once
#include "afxwin.h"


// CSetTimeDlg dialog

class CSetTimeDlg : public CDialog
{
    DECLARE_DYNAMIC(CSetTimeDlg)

public:
	CSetTimeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSetTimeDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

    int year_idx,month_idx,day_idx,hour_idx,min_idx,sec_idx;

	BOOL isTimeSet;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    void SetTimes(int year_iidx, int month_iidx, int day_iidx, int hour_iidx, int min_iidx, int sec_iidx);
    BOOL GetTimes(int &year_iidx, int &month_iidx, int &day_iidx, int &hour_iidx, int &min_iidx, int &sec_iidx);
    virtual BOOL OnInitDialog();
    CComboBox m_combo_year;
    CComboBox m_combo_month;
    CComboBox m_combo_day;
    CComboBox m_combo_hour;
    CComboBox m_combo_minute;
    CComboBox m_combo_seconds;
	afx_msg void OnBnClickedOk();
};
