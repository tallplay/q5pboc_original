// AidEditDialog.cpp : implementation file
//

#include "stdafx.h"
#include "PBOC_card_test.h"
#include "AidEditDialog.h"


// CAidEditDialog dialog

IMPLEMENT_DYNAMIC(CAidEditDialog, CDialog)

CAidEditDialog::CAidEditDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CAidEditDialog::IDD, pParent)
{
 
}

CAidEditDialog::~CAidEditDialog()
{
}

void CAidEditDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAidEditDialog, CDialog)
END_MESSAGE_MAP()


// CAidEditDialog message handlers

BOOL CAidEditDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CAidEditDialog::SetData(BYTE aid[16][256], BYTE &aid_num)
{
    aid_num++;
}

