// SetTimeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PBOC_card_test.h"
#include "SetTimeDlg.h"


// CSetTimeDlg dialog

IMPLEMENT_DYNAMIC(CSetTimeDlg, CDialog)

CSetTimeDlg::CSetTimeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSetTimeDlg::IDD, pParent)
{
    year_idx=month_idx=day_idx=hour_idx=min_idx=sec_idx = 0;
	isTimeSet = FALSE;

}

void CSetTimeDlg::SetTimes(int year_iidx, int month_iidx, int day_iidx, int hour_iidx, int min_iidx, int sec_iidx)
{
    year_idx = year_iidx - 1950;
	month_idx = month_iidx - 1;
	day_idx = day_iidx - 1;
	hour_idx = hour_iidx;
	min_idx = min_iidx;
	sec_idx = sec_iidx;
}

BOOL CSetTimeDlg::GetTimes(int &year_iidx, int &month_iidx, int &day_iidx, int &hour_iidx, int &min_iidx, int &sec_iidx)
{


	year_iidx = year_idx + 1950;
	month_iidx = month_idx + 1;
	day_iidx = day_idx+1;
	hour_iidx = hour_idx;
	min_iidx = min_idx;
	sec_iidx = sec_idx;

	
	return isTimeSet;
}

CSetTimeDlg::~CSetTimeDlg()
{
}

void CSetTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_YEAR, m_combo_year);
	DDX_Control(pDX, IDC_COMBO_MONTH, m_combo_month);
	DDX_Control(pDX, IDC_COMBO_DAY, m_combo_day);
	DDX_Control(pDX, IDC_COMBO_HOUR, m_combo_hour);
	DDX_Control(pDX, IDC_COMBO_MINUTE, m_combo_minute);
	DDX_Control(pDX, IDC_COMBO_SECOND, m_combo_seconds);
}


BEGIN_MESSAGE_MAP(CSetTimeDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSetTimeDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CSetTimeDlg message handlers

BOOL CSetTimeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	for(int i = 1950; i <= 2049; i++)
	{
	    CString year_str;
		year_str.Format(TEXT("%04d"), i);
	    m_combo_year.AddString(year_str);
	}

	m_combo_year.SetCurSel(year_idx);
	for(int i = 1; i <= 12; i++)
	{
	    CString mon_str;
		mon_str.Format(TEXT("%02d"), i);
	    m_combo_month.AddString(mon_str);
	}

	m_combo_month.SetCurSel(month_idx);

	for(int i = 1; i <= 31; i++)
	{
	    CString day_str;
		day_str.Format(TEXT("%02d"), i);
	    m_combo_day.AddString(day_str);
	}
	m_combo_day.SetCurSel(day_idx);

	for(int i = 0; i <= 23; i++)
	{
	    CString hour_str;
		hour_str.Format(TEXT("%02d"), i);
	    m_combo_hour.AddString(hour_str);
	}
	m_combo_hour.SetCurSel(hour_idx);

	for(int i = 0; i <= 59; i++)
	{
	    CString min_str;
		min_str.Format(TEXT("%02d"), i);
	    m_combo_minute.AddString(min_str);
	}
	m_combo_minute.SetCurSel(min_idx);

	for(int i = 0; i <= 59; i++)
	{
	    CString sec_str;
		sec_str.Format(TEXT("%02d"), i);
	    m_combo_seconds.AddString(sec_str);
	}
	m_combo_seconds.SetCurSel(sec_idx);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSetTimeDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	year_idx = m_combo_year.GetCurSel();
	month_idx = m_combo_month.GetCurSel();
	day_idx = m_combo_day.GetCurSel();
	hour_idx = m_combo_hour.GetCurSel();
	min_idx = m_combo_minute.GetCurSel();
	sec_idx = m_combo_seconds.GetCurSel();	
	OnOK();
	isTimeSet = TRUE;
}
