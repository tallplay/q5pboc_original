#pragma once
#include "afxwin.h"
#include "reader/q5/Mpos_command.h"

typedef struct
{
    CString name;
    DWORD tag;
}PBOC_DICTIONARY;


// CIssuerDlg dialog

class CIssuerDlg : public CDialog
{
    DECLARE_DYNAMIC(CIssuerDlg)

public:
    PBOC_DICTIONARY *dict;
    DWORD dict_num;

    BYTE test_num;
    char (*test_setting)[5][512] ; //NAME, ARC, IAD, AC, SCRIPT

    BYTE request_save[512];
    DWORD request_save_len;
    
    CIssuerDlg(CWnd* pParent = NULL);   // standard constructor
    CIssuerDlg(PBOC_DICTIONARY *dict, DWORD dict_num, char test[255][5][512], BYTE testnum);

	virtual ~CIssuerDlg();

// Dialog Data
    enum { IDD = IDD_DIALOG_ISSUER };


    CString SearchDict(DWORD tag);

    BYTE * TransactionRequest(BYTE *, DWORD, BYTE *, BOOL is_retry = 0);
    void TransactionResponse(BYTE arc[2], BYTE authcode[6], BYTE issauthdata[16], BYTE *issuedatalen, BYTE script[512], DWORD *script_len);
    BOOL IsAble2Online();
    void Clear();
    BYTE * TransactionConfirm(BYTE *, DWORD);
    BYTE * TransactionReversal(BYTE *, DWORD, BYTE/*bad*/);
    
    UINT SendData(UINT tag, BYTE *dat);

	void StrUtil_AddTagValue(CString &str, DWORD tag, DWORD len, BYTE *dat);
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CComboBox m_combo_vector;
	afx_msg void OnCbnSelchangeComboVectors();
	CComboBox m_combo_arc;
	afx_msg void OnCbnSelchangeComboArc();
};
