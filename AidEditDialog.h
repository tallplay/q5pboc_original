#pragma once
#include "mylistctrl.h"


// CAidEditDialog dialog

class CAidEditDialog : public CDialog
{
	DECLARE_DYNAMIC(CAidEditDialog)

public:
	CAidEditDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAidEditDialog();

// Dialog Data
	enum { IDD = IDD_DIALOG_EDIT_AID };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

    void SetData(BYTE aid[16][256], BYTE &aid_num);
};
