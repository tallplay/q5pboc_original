// PBOC_card_test.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "MyListCtrl.h"
#include "ColorStatic.h"

// CPBOC_card_testApp:
// See PBOC_card_test.cpp for the implementation of this class
//

class CPBOC_card_testApp : public CWinApp
{
public:
	CPBOC_card_testApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CPBOC_card_testApp theApp;